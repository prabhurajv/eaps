
CREATE TABLE  eaps.tabagnt_fc_hk
(
  agntnum character(10),
  agnt_name character(100),
  agntsup character(10),
  agntbdm character(10),
  agntrbdm character(10),
  mgbanca character(10),
  hdbanca character(10),
  agnt_branch character(10) -- agent sale branch
)
WITH (
  OIDS=FALSE
);

ALTER TABLE  eaps.tabagnt_fc_hk
  OWNER TO eaps;
GRANT ALL ON TABLE  eaps.tabagnt_fc_hk TO eaps;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE  eaps.tabagnt_fc_hk TO eaps_svc;
GRANT SELECT ON TABLE  eaps.tabagnt_fc_hk TO eaps_supp;

  CREATE TABLE  eaps.tabagnt_mgt_infor
(
  user_id character(10),
  fullname character(100),
  email character(100),
  phone character(20),
  "position" character(100),
  lascode character(10)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE  eaps.tabagnt_mgt_infor
  OWNER TO eaps;
GRANT ALL ON TABLE  eaps.tabagnt_mgt_infor TO eaps;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE  eaps.tabagnt_mgt_infor TO eaps_svc;
GRANT SELECT ON TABLE  eaps.tabagnt_mgt_infor TO eaps_supp;

create table eaps.prem_tax_chg_policy
(
 	id serial NOT NULL,
  policy_num character varying(8),
  app_num character varying(10),
  la1_name character varying(100),
  la2_name character varying(100),
  po_clntnum character varying(8),
  po_name character varying(100),
  po_salutation character varying(10),
  po_dob date,
  agnt_num character varying(8),
  agnt_name character varying(100),
  agnt_mobl_num character varying(30),
  agnt_office_num character varying(30),
  agnt_home_num character varying(30),
  premium_mode smallint,
  payment_method character varying(2),
  sum_assured_basic numeric(18,0),
  inst_prem_basic numeric(18,2),
  la1_sum_assured_pcb numeric(18,0),
  la1_sum_assured_fib numeric(18,0),
  la1_sum_assured_rtry numeric(18,0),
  la1_inst_prem_pcb numeric(18,2),
  la1_inst_prem_fib numeric(18,2),
  la1_inst_prem_rtry numeric(18,2),
  la2_sum_assured_pcb numeric(18,0),
  la2_sum_assured_fib numeric(18,0),
  la2_sum_assured_rtry numeric(18,0),
  la2_inst_prem_pcb numeric(18,2),
  la2_inst_prem_fib numeric(18,2),
  la2_inst_prem_rtry numeric(18,2),
  protection_upgr_eligible character varying(1),
  sms_notice_date timestamp without time zone,
  eaps_short_url character varying(250),
  customer_decision character varying(1),
  customer_decision_date timestamp without time zone,
  is_upsell_interested character varying(1),
  created_date timestamp without time zone,
  created_user character varying(50),
  modified_date timestamp without time zone,
  modified_user character varying(50),
  is_deleted smallint,
  sync_status character varying(1),
  sync_date timestamp without time zone,
  sync_user character varying(50),
  CONSTRAINT prem_tax_chg_policy_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE eaps.prem_tax_chg_policy
  OWNER TO eaps;
GRANT ALL ON TABLE  eaps.prem_tax_chg_policy TO eaps;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE  eaps.prem_tax_chg_policy TO eaps_svc;
GRANT SELECT ON TABLE  eaps.prem_tax_chg_policy TO eaps_supp;

ALTER TABLE eaps.prem_tax_chg_policy_id_seq
  OWNER TO eaps;
GRANT ALL ON SEQUENCE eaps.prem_tax_chg_policy_id_seq TO eaps;
GRANT ALL ON SEQUENCE eaps.prem_tax_chg_policy_id_seq TO eaps_svc;

-- Function: eaps.rpttaxchange(character, character)

-- Function: eaps.splist_heirarchies(character)

DROP FUNCTION eaps.splist_heirarchies(character);
CREATE FUNCTION eaps.splist_heirarchies(userid character)
  RETURNS TABLE(agntnum character varying) AS
$BODY$
		declare plascode varchar(10);
		pposition varchar(50);
	BEGIN	
		pposition=ltrim((select "position" FROM eaps.tabagnt_mgt_infor a where a.user_id=userid limit 1));
		plascode = (select lascode from eaps.tabagnt_mgt_infor where user_id = userid limit 1);
		if userid IN('CS326625', 'CS340830') then 
			plascode = 'all';
		end if;
		RAISE NOTICE '%',plascode;

	RETURN QUERY	
		SELECT a.agntnum::character varying(10) AS agntnum FROM eaps.tabagnt_fc_hk a WHERE (1=1)
			AND(
				CASE WHEN userid in('CS326625','CS340830') THEN 1=1
					 WHEN lower(pposition)=lower('EXCO') THEN 1=1
					 WHEN lower(pposition)=lower('Head of Banca') then ltrim(rtrim(lower(hdbanca)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('Manager Banca') then ltrim(rtrim(lower(mgbanca)))=ltrim(rtrim(lower(plascode)))
					 WHEN ltrim(rtrim(lower(pposition)))=ltrim(rtrim(lower('RBDM'))) then ltrim(rtrim(lower(agntrbdm)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('SBDM') then ltrim(rtrim(lower(agntbdm)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('BDM') then ltrim(rtrim(lower(agntbdm)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('Sale Supervisor') then ltrim(rtrim(lower(agntsup)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('UM') then ltrim(rtrim(lower(agnt_branch)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('LD') then ltrim(rtrim(lower(agntsup)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('Head of LIC') then ltrim(rtrim(lower(hdbanca)))=ltrim(rtrim(lower(plascode)))
					 WHEN left(userid, 2) IN ('60', '69') then ltrim(rtrim(lower(a.agntnum)))=ltrim(rtrim(lower(userid)))
				ELSE 1=2
				END
			);
	END;
	$BODY$

  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION eaps.splist_heirarchies(character)
  OWNER TO eaps;
GRANT EXECUTE ON FUNCTION eaps.splist_heirarchies(character) TO public;
GRANT EXECUTE ON FUNCTION eaps.splist_heirarchies(character) TO eaps;
GRANT EXECUTE ON FUNCTION eaps.splist_heirarchies(character) TO eaps_svc;
GRANT EXECUTE ON FUNCTION eaps.splist_heirarchies(character) TO eaps_supp;



-- DROP FUNCTION eaps.rpttaxchange(character, character);

-- Function: eaps.rpttaxchange(character, character)

-- DROP FUNCTION eaps.rpttaxchange(character, character);

CREATE OR REPLACE FUNCTION eaps.rpttaxchange(
    IN user_id character,
    IN keyword character)
  RETURNS TABLE(no integer, chdr_num character varying, app_num character varying, po_name character varying, po_dob date, sa_before_taxchange numeric, sa_after_taxchange numeric, prem_installment_before_taxchange numeric, prem_installment_after_taxchange numeric) AS
$BODY$
BEGIN
	return query
	WITH tmpagnts as (select * from eaps.splist_heirarchies(user_id)) 
	SELECT 
		CAST(0+row_number() OVER (ORDER BY A.policy_num DESC) AS INTEGER)  AS NO,
		A.policy_num as chdr_num,
		A.app_num,
		A.po_name, 
		A.po_dob,
		(A.sum_assured_basic + A.la1_sum_assured_pcb + A.la2_sum_assured_pcb) AS sa_before_taxchange, 
		(A.sum_assured_basic + A.la1_sum_assured_pcb + A.la2_sum_assured_pcb + A.la1_sum_assured_rtry + A.la2_inst_prem_rtry) AS sa_after_taxchange ,
		(A.inst_prem_basic + A.la1_inst_prem_pcb + A.la1_inst_prem_fib + A.la2_inst_prem_pcb + A.la2_inst_prem_fib) AS prem_installment_before_taxchange, 
		(A.inst_prem_basic + A.la1_inst_prem_pcb + A.la1_inst_prem_fib + A.la2_inst_prem_pcb + A.la2_inst_prem_fib - A.la1_inst_prem_rtry - A.la2_inst_prem_rtry) AS prem_installment_after_taxchange 
		FROM eaps.prem_tax_chg_policy A 
		INNER JOIN tmpagnts b
		on A.agnt_num = b.agntnum
		where A.policy_num LIKE '%'||$2||'%'
			OR A.app_num LIKE '%'||$2||'%'
			OR lower(A.po_name) LIKE '%'||lower($2)||'%'
			OR A.po_clntnum LIKE '%'||$2||'%'
			OR A.agnt_num LIKE '%'||$2||'%'
			OR A.po_dob = (CASE WHEN eaps.is_valid_date($2) THEN $2::date ELSE '19000101'::date END);

	-- How to run script
	/* 
	SELECT * FROM eaps.rpttaxchange(
 	    'CS340830',
 	    '70000048'
 	);
	*/
END 
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION eaps.rpttaxchange(character, character)
  OWNER TO eaps;
GRANT EXECUTE ON FUNCTION eaps.rpttaxchange(character, character) TO public;
GRANT EXECUTE ON FUNCTION eaps.rpttaxchange(character, character) TO eaps;
GRANT EXECUTE ON FUNCTION eaps.rpttaxchange(character, character) TO eaps_svc;
GRANT EXECUTE ON FUNCTION eaps.rpttaxchange(character, character) TO eaps_supp;

-- Table: eaps.premtaxchtemplate

-- DROP TABLE eaps.premtaxchtemplate;

CREATE TABLE eaps.premtaxchtemplate
(
    code character varying(20) NOT NULL,
  query character varying NOT NULL,
  templatelatin character varying NOT NULL,
  templatekhmer character varying NOT NULL,
  validflag numeric(11,0) DEFAULT NULL::numeric,
  couser character varying(45) DEFAULT NULL::character varying,
  syndate timestamp with time zone,
  CONSTRAINT premtaxchtemplate_pkey PRIMARY KEY (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE eaps.premtaxchtemplate
  OWNER TO eaps;
GRANT ALL ON TABLE  eaps.premtaxchtemplate TO eaps;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE  eaps.premtaxchtemplate TO eaps_svc;
GRANT SELECT ON TABLE eaps.premtaxchtemplate TO eaps_supp;


-- setup language using for message
delete from eaps.tab_lang;
Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','customerDecisionQuestion', 'Upgrade My Protection Benefits');
Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','customerDecisionQuestion', 'តម្លើង អត្ថប្រយោជន៍ការពាររបស់ខ្ញុំ');

-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','customerDecisionBox', 'MY DECISION BOX');
-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','customerDecisionBox', 'ប្រអប់ការសម្រេចចិត្តខ្ញុំ');

Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','acceptNotify', 'Thank you! Your protection benefits are upgraded as per Terms and Conditions');
Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','acceptNotify', 'សូមអរគុណ! អត្ថប្រយោជន៍ការពាររបស់លោកអ្នក ត្រូវបានតម្លើង យោងតាម ខ និង លក្ខខណ្ឌ');

Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','acceptUpsell', 'Yes');
Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','acceptUpsell', 'ព្រម');
	 
-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','rejectNotify', 'Thank you! Your premium are reducing  as per Terms and Conditions');
-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','rejectNotify', 'សូមអរគុណ! បុព្វលាភរបស់លោកអ្នក ត្រូវបានកាត់បន្តយយោងតាម ខ និង លក្ខណ្ឌ');

Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','upsellQuestion', 'WOULD YOU LIKE YOUR SERVICING AGENT TO CONTACT YOU TO REVIEW YOUR PROTECTION NEED?');
Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','upsellQuestion', 'តើលោកយល់ព្រមឲ្យភ្នាក់ងារលក់ ទាក់ទងលោកអ្នកដើម្បីពិនិត្យមើលតម្រូវការធានារ៉ាប់រងរបស់លោកអ្នកឡើងវិញដែរឬទេ?');





Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','rejectUpsell', 'No');
Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','rejectUpsell', 'មិនព្រម');

-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','acceptMessage', 'OPTION 1: I WANT TO UPDGRADE MY PROTECTION AT THE SAME TOTAL PREMIUM');
-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','acceptMessage', 'ជម្រើសទី‌1៖ ខ្ញុំចង់តម្លើងអត្ថប្រយោជន៍នៃការការពារ ដោយរក្សាការបង់បុព្វលាភធានារ៉ាប់រងដដែល');

-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','rejectMessage', ' OPTION 2: I DON’T WANT TO UPGRADE AS MY CURRENT LEVEL OF PROTECTION IS ADEQUATE ');
-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','rejectMessage', ' ជម្រើសទី‌2៖ ខ្ញុំមិនចង់តម្លើងអត្ថប្រយោជន៍នៃការការពារទេ ព្រោះវាមានកម្រិតគ្រប់គ្រាន់ហើយ');


Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','submit', 'Submit');
Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','submit', 'បញ្ចូន');


-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('en','alertChooseDecision', 'PLEASE CHOOSE ONE OF DECISION BOX');
-- Insert into eaps.tab_lang(lang, tag_id, "desc") values('kh','alertChooseDecision', 'សូមជ្រើសរើសជម្រើសក្នុង្រអប់ការសម្រេចចិត្តខ្ញុំ');

-- SELECT 
-- 	*,
-- 	case when premium_mode = 1 then 'ឆ្នាំ' 
-- 		when premium_mode = 12 then 'ខែ' 
-- 		else  'ឆមាស' end as bill_freq_kh,
-- 	case when premium_mode = 1 then 'year' 
-- 		when premium_mode = 12 then 'month' 
-- 		else  'half year' end as bill_freq_en,
-- 	CAST('2019-04-01' as date) as effective_date
--  FROM eaps.prem_tax_chg_policy
--  where policy_num=:policy_num