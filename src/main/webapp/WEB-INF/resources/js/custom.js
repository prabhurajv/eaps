/**
 * 
 */
// Set timeout variables.
var timoutWarning = 840000; // Display warning in 14 Mins.
var timoutNow = 55000; // Warning has been shown, give the user 1 minute to interact
var logoutUrl = 'logout.php'; // URL to logout page.

var warningTimer;
var timeoutTimer;

// Start warning timer.
function StartWarningTimer() {
    warningTimer = setTimeout("IdleWarning()", timoutWarning);
}

// Reset timers.
function ResetTimeOutTimer() {
    clearTimeout(timeoutTimer);
    StartWarningTimer();
    $("#timeout").modal('toggle');
}

// Show idle timeout warning dialog.
function IdleWarning() {
    clearTimeout(warningTimer);
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
    $("#timeout").modal('toggle');
    // Add code in the #timeout element to call ResetTimeOutTimer() if
    // the "Stay Logged In" button is clicked
}

// Logout the user.
function IdleTimeout() {
	document.getElementById("logoutForm").submit();
}

var showTandC = showTandC || (function ($) {
	var $dialog = $('<div id="modalTemplate" class="modal fade" role="dialog">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content" style="font-family: Kh-Metal-Chrieng;">' +
					'<div class="modal-header">' +
						'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
						'<h4 class="modal-title"><b>Set Image Url</b></h4>' +
					'</div>' +
					'<div class="modal-body">' +
						'<form id="frmModalTemplate" method="post">' +
							'<ul>' +
								'<li>Some term and condition text test.</li>' +
								'<li>Some term and condition text test.</li>' +
								'<li>Some term and condition text test.</li>' +
								'<li>Some term and condition text test.</li>' +
								'<li>Some term and condition text test.</li>' +
							'</ul>' +
						'</form>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="submit" class="btn btn-success" id="btnModalSubmit">Submit</button>' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param view Custom view
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, lang, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}

			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			
			if (typeof lang === 'undefined'){
				lang = 'en';
			}
			var settings = $.extend({
				dialogSize: 'lg',
				//progressType: '',
				onHide: clearModalInput // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);

			$dialog.find('h4').text(message);

			$dialog.find('#btnModalSubmit').unbind('click').on('click', function (e) {
				

//				$dialog.modal('hide');
//				$dialog.modal('destroy').remove();

				e.preventDefault();
				
				$('form')[0].submit();
			});

			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			
			$dialog.find('ul').html('');
			if(lang == 'en'){
				for(var i=0; i<tandc_en.length; i++){
//		  			$('#' + data[i].tagId).html(data[i].desc);
		  			$dialog.find('ul').append('<li>' + tandc_en[i].desc + '</li>');
		  		}
			}else{
				for(var i=0; i<tandc_kh.length; i++){
		  			$dialog.find('ul').append('<li>' + tandc_kh[i].desc + '</li>');
		  		}
			}

			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},

		/**
		 * Destroy dialog
		 */
		destroy: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},
		setType: function (value) {
			type = value;
		}
	};

	function clearModalInput() {
	    $dialog.find('input[name=modal_template_name]').val('');
	    $dialog.find('input[name=modal_template_desc]').val('');
	}
})(jQuery);

var showFailed = showFailed || (function ($) {
	var $dialog = $('<div id="modalTemplate" class="modal fade" role="dialog">' +
			'<div class="modal-dialog">' +
			'<div class="modal-content" style="font-family: Kh-Metal-Chrieng;">' +
				'<div class="modal-header">' +
					'<button type="button" class="btn btn-danger close" data-dismiss="modal">&times;</button>' +
					'<h4 class="modal-title"><b></b></h4>' +
				'</div>' +
				'<div class="modal-body">' +
					'<center>' +
						'<h4 id="successMsg" style="font-family: Khmer OS;">' +
						'</h4>' +
					'</center>' +
				'</div>' +
				'<div class="modal-footer">' +
					'<center><button type="submit" class="btn btn-default" id="btnModalSubmit">OK</button></center>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>');
	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param view Custom view
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, lang, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}

			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			
			if (typeof lang === 'undefined'){
				lang = 'en';
			}
			var settings = $.extend({
				dialogSize: 'md',
				//progressType: '',
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);

//			$dialog.find('#successMsg').html(message);

			$dialog.find('#btnModalSubmit').unbind('click').on('click', function (e) {
				$dialog.modal('hide');
				$dialog.modal('destroy').remove();
			});

			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			
			$dialog.find('#successMsg').html(message);
		

			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},

		/**
		 * Destroy dialog
		 */
		destroy: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},
		setType: function (value) {
			type = value;
		}
	};
})(jQuery);


var showSuccess = showSuccess || (function ($) {
	var $dialog = $($("#TaxChangeConfirmMessageModal")[0]).html();
	$dialog = $($dialog);
	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param view Custom view
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, lang, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}

			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			
			if (typeof lang === 'undefined'){
				lang = 'en';
			}
			var settings = $.extend({
				dialogSize: 'md',
				//progressType: '',
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);

//			$dialog.find('#successMsg').html(message);

			$dialog.find('#btnModalSubmit').unbind('click').on('click', function (e) {
				$dialog.modal('hide');
				$dialog.modal('destroy').remove();
			});

			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			
			$dialog.find('#successMsg').html(message);
		

			// Opening dialog
			$dialog.modal();
			return $dialog;
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},

		/**
		 * Destroy dialog
		 */
		destroy: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},
		setType: function (value) {
			type = value;
		}
	};
})(jQuery);

function setLanguage(lang){
	if(typeof(Storage) !== 'undefined'){
		if(localStorage.lang){
			localStorage.removeItem('lang');
		}
		
		localStorage.lang = lang;
	}
}

function getLanguage(){
	if(typeof(Storage) !== 'undefined'){
		if(localStorage.lang){
			return localStorage.lang;
		}
	}
	
	return 'kh';
}