            var app = angular.module('myApp', []);
            app.controller('myCtrl', function ($scope, $filter) {
                  $scope.init = function()
                  {
                	  
                      $scope.model=JSON.parse(window.TaxChange);
                      $scope.messages = JSON.parse(window.Messages);
                      $scope.IsAnonymous = window.IsAnonymous;
                      $scope.language = getLanguage();
                                          
                      if ($scope.model.protection_upgr_eligible == 'Y' && $scope.IsAnonymous == 'true' && $scope.model.customer_decision == null)
                      {
                    	  $("#protectionUpgradeBox").show();
                      }

                      if($scope.language=='kh') // get from localstorage
                      {
                        $scope.PrintPreview = $("#TemplateKH").html()
                        $scope.khLanguage = true;
                      }
                      else
                      {
                        $scope.PrintPreview = $("#TemplateEN").html();
                        $scope.khLanguage = false;
                      }
                      if(window.submit=='true')
                      {
                          /// return submit error message
                    	  showFailed.show(window.error, 'en');
                          return;
                      }

                      if($scope.model && $scope.model.customer_decision == 'Y' && $scope.model.protection_upgr_eligible == 'Y'){ // accept the condition

                    	  var dialog = showSuccess.show($scope.findMessage('acceptNotify'), 'en', dialog);
                    	  if($scope.model.is_upsell_interested == null && window.firstLoadAfterSubmit == 'true') // only first loading after submit and not yet decide
                          {
                    		  $(dialog).find("form#interestedUpsell").show();
                    		  $(dialog).find("#upsellQuestion").html($scope.findMessage("upsellQuestion"));
                    		  $(dialog).find("#is_upsell_interested_y").html($scope.findMessage("acceptUpsell"));
                    		  $(dialog).find("#is_upsell_interested_n").html($scope.findMessage("rejectUpsell"));
                          }
                       }
                        
                  }

                  $scope.switchLanguage = function()
                  {
                      if($scope.khLanguage == true)
                      {
                          setLanguage('kh');
                          $scope.PrintPreview = $("#TemplateKH").html();
                          $scope.language = 'kh';
                      }
                      else
                      {
                          setLanguage('en');
                          $scope.PrintPreview = $("#TemplateEN").html();
                          $scope.language = 'en';
                      }
                  }
                  
                  $scope.findMessage = function(key)
                  {
                	  var value =  $.grep($scope.messages, function(e){ return e.tag_id.trim() == key && e.lang.trim()== $scope.language; });
                	  return  value[0] == null ? "": value[0].description;
                  }

                 $scope.submit = function()
                 {
                		 $('form#customerdecision')[0].submit();
                 }

                  $scope.init();
            }).directive('ngPrintPreview', function ($compile) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs, ctrls) {
                        // trigger changes of the ngModel
                        scope.$watch(attrs.ngPrintPreview, function (value) {
                            if (value) {
                                var tableElement = angular.element(value);
                                $compile(tableElement)(scope);
                                element.html(tableElement);
                            }
                        });
                    }
                };
            }).directive('toggleCheckbox', function() {
            	  
            	  return {
            	    restrict: 'A',
            	    require: 'ngModel',
            	    link: function (scope, element, attributes, ngModelController) {
            	      element.on('change.toggle', function(event) { // note that ".toogle" is our namespace, used further down to remove the handler again
            	        var checked = element.prop('checked');
            	        ngModelController.$setViewValue(checked);
            	      });
            	                
            	      ngModelController.$render = function() {
            	        element.bootstrapToggle(ngModelController.$viewValue ? 'ខ្មែរ' : 'EN');
            	      };
            	      
            	      scope.$on('$destroy', function() {
            	        // clean up
            	        element.off('change.toggle');
            	        element.bootstrapToggle('destroy');
            	      });
            	              
            	      // we set the 'checked' property once so the Bootstrap toggle is initialized to the correct value, i.e.,  without flashing the 'off' state and then switch to the 'on' state in case of an initial value of 'true';
            	      // this is not needed if your markup already contains the correct 'checked' property;
            	      // note that we can't use ngModelController.$viewValue since at this stage, it's still uninitialized as NaN
            	      var initialValue = scope.$eval(attributes.ngModel);
            	      element.prop('checked', initialValue);
            	    }
            	  };
            	});