<%@ include file="shared/header.jspf" %>
<style>	
	#login-style{
		background-image: -webkit-linear-gradient(90deg, #8C0000, #DE1818);
		box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.2) inset, 0px 0px 2px rgba(0, 0, 0, 0.5);
	}
</style>
<body>
	<div id="login-page">
	  	<div class="container" style="margin-top:50px;">
	  		  <fmt:setBundle basename="version" />  
		      <fmt:message var="version" key="version" /> 
			  <center><img src="resources/images/logo_prudential_notagline.jpg" width=300 height=130/></center>
			  <form:form class="form-login" name='loginForm'   action="login" method='POST' autocomplete="off" commandName="user">
			        <h2 class="form-login-heading" style="background:#fff2f3;">Sign in to <SPAN style="color:#EE1B2E;font-size:20px;"><b><span style="text-transform: lowercase;">e</span>APS</b></SPAN><i style="text-transform: lowercase; font-size:25px;">Online</i> V1.0</h2>
			      
			        <div class="login-wrap">
			            <form:input type="text" path='username' class="form-control" placeholder="User ID"/>
			            <br>
			            <form:password path='pwd' class="form-control" placeholder="Password" />
			            <label class="checkbox" style="height: 20px">
			            	<span class="pull-left">
			            		<c:if test="${not empty error}">
								  <div class="text-danger">${error}</div>
								  </c:if>
								  <c:if test="${not empty msg}">
								  <div class="text-success">${msg}</div>
								  </c:if>
								  <c:if test="${not empty expire}">
								 <div class="text-danger">${expire}</div>
								 </c:if>
			            	</span>
			            </label>
			           
			            <button class="btn btn-danger btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
			            <br/>
			             
			            <p class="text-muted">� 2016 Prudential Cambodia All Rights Reserved. </p>
			         </div>
					 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			          <!-- Modal -->
			          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
			              <div class="modal-dialog">
			                  <div class="modal-content">
			                      <div class="modal-header">
			                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                          <h4 class="modal-title">Forgot Password ?</h4>
			                      </div>
			                      <div class="modal-body">
			                          <p>Enter your e-mail address below to reset your password.</p>
			                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
			
			                      </div>
			                      <div class="modal-footer">
			                          <button data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
			                          <button class="btn btn-theme" type="button">Submit</button>
			                      </div>
			                  </div>
			              </div>
			          </div>
		      </form:form>
	  	</div>
	  	 
	  </div>
<%@ include file="shared/footer.jspf" %>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[name=username]').focus();
	});
</script>