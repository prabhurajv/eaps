﻿<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
	
	@media(max-width: 768px){
		#title{
			font-size: 16px;
		}
	}
	
	.view-style{
		font-size: 12px;
	}
	
	.view-style-kh{ 
		font-size: 12px;
	}
	
	.option-1-intro{
		font-size: 15px;
	}
	
	.tandc-title{
		font-size: 14px;
	}
	
	p{
		line-height: 1.7em;
	}
	
	@media print{
		.panel{
			border: none;
		}
		
		.panel-heading{
			border-color: transparent;
		}
		
		.btn{
			border: none;
		}
		
		.toggle{
			display: none;
		}
		
/* 		p{ */
/* 			margin: 0px; */
/* 		} */
		
		legend{
			margin-bottom: 0px;
		}
		
		#tandc-box{
 			margin-bottom: 150px; 
		}
		
		.view-style{
			font-size: 11px;
		}
		
 		.view-style-kh{ 
 			font-size: 10px;
 		}
		
 		.option-1-intro-kh{
 			font-size: 11px;
 		}
		
 		.tandc-title-kh{
 			font-size: 11px;
  		}
  		
  		th{
  			padding-top: 0px;
  		}
  		
  		td{
  			padding-top: 0px;
  			padding-bottom: 0px;
  		}
  		
  		#tandc-list{
  			font-size: 10px;
  		}
  		
  		a[href]:after {
		    content: none !important;
	    }
	}
</style>
<body style="height: 100%">
	<div class="panel panel-danger" style="margin: 0 20px; margin-top: 10px;">
		<div class="panel-heading" style="background-color: #EE1B2E; border-color: #d43f3a;">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px;">
				<div class="col-xs-12" style="padding: 0px;">
					<h3 class="pull-left" id="title" style="font-family: Khmer OS; color: white; line-height: 0; margin-top: 15px;">
						Prudential (Cambodia) Life Assurance PLC
					</h3>
					<input type="checkbox" checked data-toggle="toggle" data-size="small"
					 data-on="ខ្មែរ" data-off="En" data-onstyle="danger" id="toggle-lang" />
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px; font-family: Khmer OS;">
				<form method="post">
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.getSendDateFormated(\"dd/MM/yyyy\") }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">Policy Number: <c:out value="${ log.chdrNum }" /></span><br/>
<%-- 					<span class="text-danger" style="font-size: 12px;">Date of Birth: <fmt:formatDate type="date" pattern="dd MMMM yyyy" value="${ log.taxChange.get(0).poDob }" /></span><br/> --%>
					<span class="text-danger" style="font-size: 12px;">FC/LIC: <c:out value="${ log.taxChange.get(0).agntName }" /></span><br/><br/>
<!-- 					<p style="margin-top: 5px;"><b id="subject" class="view-style">Subject: Upgraded protection due to tax change</b></p> -->
					<p class="view-style"><span id="dear" class="view-style">ជូនចំពោះ</span> <span class="view-style"><c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName.trim() }" />,</span></p>
					<p id="greeting" class="view-style">
						<span style="text-align: justify;">
							<br/>យើងខ្ញុំសូមថ្លែងអំណរគុណចំពោះការជឿទុកចិត្ដនិងការជ្រើសរើសយកក្រុមហ៊ុនព្រូដិនសល (ខេមបូឌា) ឡាយហ្វ៍ អឹសួរ៉ិនស៍ ម.ក (ក្រុមហ៊ុន) ធ្វើជាក្រុមហ៊ុន​ផ្តល់សេវាធានារ៉ាប់រងអាយុជីវិតជូនលោកអ្នក។
							<br/><br/>យើងខ្ញុំមានសេចក្តីសោមនស្សរីករាយសូមជម្រាបជូនលោកអ្នកថា ពន្ធដែលមានអត្រាស្មើនឹង 5% លើបុព្វលាភរ៉ាប់រងនៃសេវាធានារ៉ាប់រងអាយុជីវិតត្រូវបាន​ផ្លាស់ប្តូរ។ អាស្រ័យដោយការផ្លាស់ប្ដូរនេះ  ក្រុមហ៊ុននឹងផ្ដល់ជូនលោកអ្នកនូវជម្រើសមួយក្នុងចំណោមជម្រើសទាំងពីរខាងក្រោម ដោយយោងទៅតាម​លទ្ធផល​នៃការវាយតម្លៃជាក់ស្ដែងលើសំណើសុំបន្តសុពលភាពនៃបណ្ណសន្យារ៉ាប់រងប៉ុណ្ណោះ។ ទន្ទឹមនឹងនេះ សូមលោកអ្នកផ្ដិតស្នាមមេដៃ ឬចុះហត្ថលេខា​នៅពីខាង​ក្រោមនៃជម្រើសណាមួយដើម្បីបញ្ជាក់អំពីបំណងលោកអ្នក។
						</span>
					</p>
<!-- 					<p id="option-1-intro" class="option-1-intro" style="color: red; font-weight: 600;"> -->
<!-- 						Providing greater peace of mind for you and your family at the same premium -->
<!-- 					</p> -->
<!-- 					<span id="option-1-intro-2" class="view-style"> -->
<!-- 						To enjoy this upgrade, all you need to do is to confirm your acceptance by: -->
<!-- 					</span> -->
					<ul id="ul-main" class="view-style">
						<li>
							<span id="ul-1" class="view-style"><span style="color: red;"><u>ជម្រើសទី 1</u> ៖</span> យើងខ្ញុំអាចនឹងតម្លើងអត្ថប្រយោជន៍ការពាររបស់លោកអ្នកពី US$<span id="sa-before"><fmt:formatNumber type="number" pattern="###,###,##0.00" value="${log.getTotalSABeforeTaxChange()}" /></span> (ដែលជាគម្រោងមូលដ្ឋាន + អត្ថប្រយោជន៍នៃ​លក្ខខណ្ឌ​បន្ថែម) ទៅ US$<span id="sa-after"><fmt:formatNumber type="number" pattern="###,###,##0.00" value="${log.getTotalSAAfterTaxChange()}" /></span> ជាមួយនឹងបុព្វលាភរ៉ាប់រងដដែល ។</span>
							<br/><br/>
							<fieldset class="scheduler-border" style="margin-top: 10px;">
								<legend class="scheduler-border" style="margin-bottom: 0px;">
									<span id="choose-option" class="btn btn-sm btn-danger view-style">
										សេចក្តីសង្ខេបនៃអត្ថប្រយោជន៍៖
									</span>
								</legend>
								<div class="row table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th style="max-width: 200px; vertical-align: text-top;">
													<span id="th-desc" class="view-style">ព័ត៌មានអំពីការធានារ៉ាប់រង</span>
												</th>
												<th style="vertical-align: text-top;">
													<span id="th-current" class="view-style">អត្ថប្រយោជន៍និងបុព្វលាភរ៉ាប់រង     បច្ចុប្បន្នជាដំណាក់កាល/ប្រចាំ</span>
													<c:choose>
														<c:when test="${ log.taxChange.get(0).freq == \"1\" }">
															<span id="freq">ឆ្នាំ</span>
														</c:when>
														<c:when test="${ log.taxChange.get(0).freq == \"2\" }">
															<span id="freq">ឆមាស</span>
														</c:when>
														<c:when test="${ log.taxChange.get(0).freq == \"12\" }">
															<span id="freq">ខែ</span>
														</c:when>
													</c:choose>
												</th>
												<th style="vertical-align: text-top;">
													<c:choose>
														<c:when test="${ !log.approved && !log.rejected }">
															<input type="radio" name="rdoOption" id="rdoIncrease" class="hidden-print" value="increase" checked />
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${ not empty log.approvedDate }">
																	<input type="radio" name="rdoOption" id="rdoIncrease" class="hidden-print" value="increase" 
																		checked disabled />
																</c:when>
																<c:otherwise>
																	<input type="radio" name="rdoOption" id="rdoIncrease" class="hidden-print" value="increase"
																	 	disabled />
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
													<span id="th-extra" class="view-style">អត្ថប្រយោជន៍ និងបុព្វលាភរ៉ាប់រងថ្មី​ជាដំណាក់កាល/ប្រចាំ 
													<c:choose>
														<c:when test="${ log.taxChange.get(0).freq == \"1\" }">
															<span class="freq">ឆ្នាំ</span>
														</c:when>
														<c:when test="${ log.taxChange.get(0).freq == \"2\" }">
															<span class="freq">ឆមាស</span>
														</c:when>
														<c:when test="${ log.taxChange.get(0).freq == \"12\" }">
															<span class="freq">ខែ</span>
														</c:when>
													</c:choose>
													 បន្ទាប់ពី​បានតម្លើង</span>
												</th>
		<!-- 										<th style="word-wrap: break-word;"> -->
		<%-- 											<c:choose> --%>
		<%-- 												<c:when test="${ !log.approved && !log.rejected }"> --%>
		<!-- 													<input type="radio" name="rdoOption" id="rdoRefund" value="refund" /> -->
		<%-- 												</c:when> --%>
		<%-- 												<c:otherwise> --%>
		<%-- 													<c:choose> --%>
		<%-- 														<c:when test="${ not empty log.rejectedDate }"> --%>
		<!-- 															<input type="radio" name="rdoOption" id="rdoRefund" value="refund"  -->
		<!-- 																checked disabled /> -->
		<%-- 														</c:when> --%>
		<%-- 														<c:otherwise> --%>
		<!-- 															<input type="radio" name="rdoOption" id="rdoRefund" value="refund" -->
		<!-- 															 	disabled /> -->
		<%-- 														</c:otherwise> --%>
		<%-- 													</c:choose> --%>
		<%-- 												</c:otherwise> --%>
		<%-- 											</c:choose> --%>
		<!-- 											Same benefits and premium  -->
		<!-- 											installment/year -->
		<!-- 										</th> -->
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="3" style="border: none; max-height: 200px;">
													ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានា 1 ៖ <c:out value="${ log.lifePFs.get(0).laName }" />
												</td>
											</tr>
											<tr>
												<td style="padding-left: 20px; max-width: 200px;">
													<span id="desc-basic-life1">ទឹកប្រាក់ធានារ៉ាប់រងនៃគម្រោងមូលដ្ឋាន</span>
												</td>
												<td style="max-width: 200px">
													US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(0).basicSA }"/>
												</td>
												<td style="max-width: 200px">
<%-- 													<c:out value="${ log.lifePFs.get(0).basicSA }" /> --%>
													<span class="same">ដដែល</span>
												</td>
		<!-- 										<td style="max-width: 200px"> -->
		<%-- 											<c:out value="${ log.lifePFs.get(0).basicSA }" /> --%>
		<!-- 										</td> -->
											</tr>
											<c:if test="${ log.lifePFs.get(0).hasRtr1.trim().toLowerCase().equals('yes') }">
												<tr>
													<td style="padding-left: 20px; max-width: 200px;">
														<span id="desc-pcb-life1">អត្ថប្រយោជន៍ធានារ៉ាប់រងមាតាបិតា</span>
													</td>
													<td style="max-width: 200px">
														US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(0).pcb }"/>
													</td>
													<td style="color: red; max-width: 200px; font-weight: 600;">
														US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(0).pcb + log.taxChange.get(0).extraSA }"/>
													</td>
		<!-- 											<td style="max-width: 200px"> -->
		<%-- 												<c:out value="${ log.lifePFs.get(0).pcb }" /> --%>
		<!-- 											</td> -->
												</tr>
											</c:if>
											<c:if test="${ log.lifePFs.get(0).hasRsr1.trim().toLowerCase().equals('yes') }">
												<tr>
													<td style="padding-left: 20px; max-width: 200px;">
														<span id="desc-fib-life1">អត្ថប្រយោជន៍ចំណូលគ្រួសារ</span>
													</td>
													<td style="max-width: 200px">
														US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(0).fib }"/>
													</td>
													<td style="max-width: 200px">
<%-- 														<c:out value="${ log.lifePFs.get(0).fib }" /> --%>
														<span class="same">ដដែល</span>
													</td>
		<!-- 											<td style="max-width: 200px"> -->
		<%-- 												<c:out value="${ log.lifePFs.get(0).fib }" /> --%>
		<!-- 											</td> -->
												</tr>
											</c:if>
<!-- 											<tr style="border: none;"> -->
<!-- 												<td colspan="3" style="border: none;"></td> -->
<!-- 											</tr> -->
											<c:if test="${ log.lifePFs.size() > 1 }">
												<tr>
													<td colspan="3" style="border: none;">
														ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានា 2 ៖ <c:out value="${ log.lifePFs.get(1).laName }" />
													</td>
												</tr>
<!-- 												<tr> -->
<!-- 													<td style="padding-left: 20px; max-width: 200px;"> -->
<!-- 														<span id="desc-basic-life2">Basic Sum Assured</span> -->
<!-- 													</td> -->
<!-- 													<td style="max-width: 200px"> -->
<%-- 														<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 													</td> -->
<!-- 													<td style="max-width: 200px"> -->
<%-- 														<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 													</td> -->
<!-- 													<td style="max-width: 200px"> -->
<%-- 														<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 													</td> -->
<!-- 												</tr> -->
<%-- 												<c:if test="${ log.lifePFs.get(1).hasRtr1.trim().toLowerCase().equals('yes') }"> --%>
													<tr>
														<td style="padding-left: 20px; max-width: 200px;">
															<span id="desc-pcb-life2">អត្ថប្រយោជន៍ធានារ៉ាប់រងមាតាបិតា</span>
														</td>
														<td style="max-width: 200px">
															US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(1).basicSA }"/>
														</td>
														<td style="max-width: 200px;">
															<span class="same">ដដែល</span>
<%-- 															US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(1).basicSA + log.taxChange.get(1).extraSA }"/> --%>
														</td>
		<!-- 												<td style="max-width: 200px"> -->
		<%-- 													<c:out value="${ log.lifePFs.get(1).pcb }" /> --%>
		<!-- 												</td> -->
													</tr>
<%-- 												</c:if> --%>
												<c:if test="${ log.lifePFs.get(1).hasRsr1.trim().toLowerCase().equals('yes') }">
													<tr>
														<td style="padding-left: 20px; max-width: 200px;">
															<span id="desc-fib-life2">អត្ថប្រយោជន៍ចំណូលគ្រួសារ</span>
														</td>
														<td style="max-width: 200px">
															US$<fmt:formatNumber type="number" pattern="###,###,##0.00" value="${ log.lifePFs.get(1).fib }"/>
														</td>
														<td style="max-width: 200px">
<%-- 															<c:out value="${ log.lifePFs.get(1).fib }" /> --%>
															<span class="same">ដដែល</span>
														</td>
		<!-- 												<td style="max-width: 200px"> -->
		<%-- 													<c:out value="${ log.lifePFs.get(1).fib }" /> --%>
		<!-- 												</td> -->
													</tr>
												</c:if>
												
<!-- 												<tr style="border: none;"> -->
<!-- 													<td colspan="3" style="border: none;"></td> -->
<!-- 												</tr> -->
											</c:if>
<!-- 											<tr> -->
<!-- 												<td style="border: none; max-width: 200px;"> -->
<!-- 													<span id="p-installment">Premium Installment /</span> -->
<%-- 													<c:choose> --%>
<%-- 														<c:when test="${ log.taxChange.get(0).freq == \"1\" }"> --%>
<!-- 															<span class="freq">year</span> -->
<%-- 														</c:when> --%>
<%-- 														<c:when test="${ log.taxChange.get(0).freq == \"2\" }"> --%>
<!-- 															<span class="freq">half-year</span> -->
<%-- 														</c:when> --%>
<%-- 														<c:when test="${ log.taxChange.get(0).freq == \"12\" }"> --%>
<!-- 															<span class="freq">month</span> -->
<%-- 														</c:when> --%>
<%-- 													</c:choose> --%>
<!-- 												</td> -->
<!-- 												<td style="border: none;"> -->
<%-- 													US$<fmt:formatNumber type="number" pattern="###,##0.00" value="${ log.taxChange.get(0).installmentBeforeTaxchange }" /> --%>
<!-- 												</td> -->
<!-- 												<td style="border: none;"> -->
<!-- 													<span class="same">Same</span> -->
<!-- 												</td> -->
<!-- 											</tr> -->
										</tbody>
									</table>
								</div>
							</fieldset>
							<br/>
							<div id="tandc-box" class="row" style="border: 1px solid grey; padding: 5px; margin-left: 0px; margin-right: 0px;">
								<div class="col-xs-12">
									<span id="tandc-title" class="tandc-title">ខ និង លក្ខខណ្ឌ</span>
									<br/>
									<ul id="tandc-list">
										<li>ខ្ញុំបាទ/នាងខ្ញុំ យល់ព្រមទទួលអត្ថប្រយោជន៍ការពារដែលបានតម្លើងសម្រាប់បណ្ណសន្យារ៉ាប់រងរបស់ខ្ញុំបាទ/នាងខ្ញុំចាប់ ពីកាលបរិច្ឆេទបន្តសុពលភាពនៃបណ្ណសន្យារ៉ាប់រងតទៅ។</li>
										<li>ខ្ញុំបាទ/នាងខ្ញុំ យល់និងទទួលស្គាល់ថា ការតម្លើងអត្ថប្រយោជន៍ការពារបន្ថែម មានសុពលភាព ដរាបណាសំណើ​សុំបន្ត​សុពលភាពនៃបណ្ណសន្យារ៉ាប់រងរបស់ខ្ញុំបាទ/នាងខ្ញុំ ត្រូវបានទទួលយកនិងយល់ព្រមផ្តល់ជូនអត្ថប្រយោជន៍ ដោយ​ក្រុមហ៊ុនព្រូដិនសលកម្ពុជាតែប៉ុណ្ណោះ។</li>
										<li>ខ្ញុំបាទ/នាងខ្ញុំ យល់ និងទទួលស្គាល់ថា ក្រុមហ៊ុននឹងអនុវត្តតាមលក្ខខណ្ឌនេះ រហូតដល់កាលបរិច្ឆេទផុតកំណត់នៃបណ្ណ សន្យារ៉ាប់រងរបស់ខ្ញុំបាទ/នាងខ្ញុំ។</li>
									</ul>
									<br/>
									<div class="row">
										<c:if test="${ not empty log && log.approved == true }">
											<fmt:formatDate var="date" value="${log.approvedDate}" pattern="dd/MM/yyyy" />
										</c:if>
										<c:if test="${ not empty log && log.rejected == true }">
											<fmt:formatDate var="date" value="${log.rejectedDate}" pattern="dd/MM/yyyy" />
										</c:if>
										
										<sec:authorize access="isAnonymous()">
											<c:if test="${ empty log || (log.approved == false && log.rejected == false)}">
												<div class="col-xs-6">
													<button type="button" id="btnAgreed" class="btn btn-danger btn-sm hidden-print">Upgrade my protection</button>
												</div>
											</c:if>
											<c:if test="${ empty log || (log.approved == true || log.rejected == true)}">
												<div class="col-xs-6">
													<button type="button" class="btn btn-danger btn-sm" style="visibility: hidden;">Just a button</button>
												</div>
											</c:if>
											<div class="col-xs-6">
												<div class="pull-right" style="margin-right: 40px;">
													<span><span class="agreed-text">យល់ព្រមនៅថ្ងៃទី ___ / ___ / ______</span>
													<br/><span style="color: lightgray;">ហត្ថលេខា ឬស្នាមមេដៃ</span>
													<br/><br/><br/><br/><br/>
													<span><span id="name">ឈ្មោះ </span><u><c:out value="${ log.taxChange.get(0).poName }" /></u></span>
												</div>
											</div>
										</sec:authorize>
										<sec:authorize access="isAuthenticated()">
											<div class="col-xs-12">
												<div class="pull-right" style="margin-right: 40px;">
													<span><span class="agreed-text">យល់ព្រមនៅថ្ងៃទី ___ / ___ / ______</span>
													<br/><span style="color: lightgray;">ហត្ថលេខា ឬស្នាមមេដៃ</span>
													<br/><br/><br/><br/><br/>
													<span><span id="name">ឈ្មោះ </span><u><c:out value="${ log.taxChange.get(0).poName }" /></u></span>
												</div>
											</div>
										</sec:authorize>
									</div>
								</div>
							</div>
							<br/>
							<br/>
							<br/>
						</li>
						<li id="ul-2" class="view-style" style="padding-top: 70px">
							<span style="color: red;"><u>ជម្រើសទី 2 ៖</u></span> បើលោកអ្នកយល់ឃើញថា អត្ថប្រយោជន៍ការពារបច្ចុប្បន្នរបស់លោកអ្នកមានកម្រិតគ្រប់គ្រាន់ សមរម្យល្មមហើយ លោកអ្នកអាច​ជ្រើសរើសរក្សាអត្ថប្រយោជន៍ការពារនោះឲ្យស្ថិតនៅក្នុងកម្រិតដដែលក៏បាន ដោយបុព្វលាភរ៉ាប់រងសរុបត្រូវទូទាត់របស់លោកអ្នក នឹងត្រូវបាន​កាត់បន្ថយពី US$<span id="current-p"><fmt:formatNumber type="number" pattern="###,##0.00" value="${ log.taxChange.get(0).installmentBeforeTaxchange }" /></span> ទៅ US$<span id="new-p"><fmt:formatNumber type="number" pattern="###,##0.00" value="${ log.getTotalInstallmentAfterTaxChange() }" /></span> ។
							<br/>
							<div class="col-xs-12" style="border: 1px solid black; margin-top: 20px;">
								<div class="pull-right" style="margin-right: 40px;">
									<span class="agreed-text">យល់ព្រមនៅថ្ងៃទី ___ / ___ / ______</span>
									<br/><span style="color: lightgray;">ហត្ថលេខា ឬស្នាមមេដៃ</span>
									<br/><br/><br/><br/><br/>
									<span><span id="name">ឈ្មោះ </span><u><c:out value="${ log.taxChange.get(0).poName }" /></u></span>
									<br/>
								</div>
							</div>
						</li>
					</ul>
					
					<br/><br/>
					<p id="option-2-intro" class="view-style" style="padding-top: 100px;">
						<span style="text-align: justify;">
							ប្រសិនបើលោកអ្នកជ្រើសរើសយកជម្រើសទីពីរនេះ សូមលោកអ្នក ៖
						</span>
					</p>
					<ol>
						<li id="ol-1" class="view-style">
							កែប្រែបញ្ជាអចិន្ត្រៃយ៍ថេរបច្ចុប្បន្នរបស់លោកអ្នក ឲ្យបានត្រឹមត្រូវទៅតាមចំនួនបុព្វលាភរ៉ាប់រងថ្មីឲ្យបានឆាប់រហ័សតាមការគួរ។ ការកែប្រែនេះ​គឺពាក់ព័ន្ធនឹងការបំពេញសំណុំបែបបទស្នើសុំថ្មីដើម្បីដាក់ឲ្យដំណើរការជាថ្មី។
						</li>
						<li id="ol-2" class="view-style">
							<span style="text-align: justify;">
								រួចផ្ញើច្បាប់ចម្លងនៃការកែប្រែបញ្ជាអចិន្ត្រៃយ៍ថេរមកកាន់ក្រុមហ៊ុនតាមរយៈភ្នាក់ងារលក់ ឬមជ្ឈមណ្ឌលផ្ដល់សេវាអតិថិជនយើងខ្ញុំ តាមរយៈ​អាសយដ្ឋាន អគារភ្នំពេញថោវើរ៍ ជាន់ទី20  ផ្ទះលេខ 445 មហាវិថីព្រះមុនីវង្ស សង្កាត់បឹងព្រលិត ខណ្ឌ 7 មករា រាជធានី ភ្នំពេញ។
								<br/><br/>
								បុព្វលាភរ៉ាប់រងដែលត្រូវកាត់បន្ថយ នឹងចាប់មានសុពលភាពបន្ទាប់ពីក្រុមហ៊ុនទទួលបានច្បាប់ចម្លងនៃការកែប្រែបញ្ជាអចិន្ត្រៃយ៍ថេរពីលោកអ្នក។
							</span>
						</li>
					</ol>
					<p id="note" class="view-style">
						<span style="text-align: justify;">
							យើងខ្ញុំសូមលើកទឹកចិត្តឲ្យលោកអ្នកពិគ្រោះយោបល់ជាមួយនឹងភ្នាក់ងារលក់របស់លោកអ្នក ដើម្បីពិនិត្យមើលតម្រូវការធានារ៉ាប់រងឡើងវិញ និងដើម្បី​ជាជំនួយ​ដល់សេចក្ដីសម្រចចិត្តរបស់លោកអ្នក ។
						</span>
					</p>
					<p id="contact" class="view-style">
						<span style="text-align: justify;">
							<br/>សូមលោកអ្នកទាក់ទងភ្នាក់ងារលក់របស់លោកអ្នកឈ្មោះ <span id="agent-name"><c:out value="${ log.taxChange.get(0).agntName }"/></span> ទូរស័ព្ទលេខ <span id="agent-phone"><c:out value="${ log.taxChange.get(0).agntPhone }"/></span> ឬបុគ្គលិកផ្នែកទំនាក់ទំនងអតិថិជន​តាមរយៈលេខទូរស័ព្ទ តាមរយៈលេខ 1800-212223 (ឥតគិតថ្លៃ) ឬ 023 964 222 ប្រសិនបើលោកអ្នកមានសំណួរផ្សេងៗ។
						</span>
					</p>
		
						<br/><br/><span id="sign" class="view-style">ដោយក្តីគោរពដ៏ខ្ពង់ខ្ពស់</span>
						<br/><img src="<c:url value="/resources/images/CEO Signature.JPG"/>" />
						<br/><span class="view-style">David Nutman</span>
						<br/><span id="pos" class="view-style">អគ្គនាយក</span>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  />
				</form>
				
			</div>
		</div>
	</div>
	<div class="row" style="margin: 0px; height: 100px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	var subject = '';
	var dear = '';
	var greeting = '';
	var choose_option = '';
	var option_1_intro = '';
	var option_1_intro_2 = '';
	var ul_1 = '';
	var ul_2 = '';
	var ul_3 = '';
	var ul_4 = '';
	var option_2_intro = '';
	var ol_1 = '';
	var ol_2 = '';
	var note = '';
	var sign = '';
	var pos = '';
	var tandc = '';
	var tandc2 = '';
	var data = [];
	var sa_before = '';
	var sa_after = '';
	var tandc_en = [];
	var tandc_kh = [];
	var contact = '';
	var agent_name = '';
	var agent_phone = '';
	var current_p = '';
	var new_p = '';
	var th_desc = '';
	var th_current = '';
	var th_extra = '';
	var desc_basic_life1 = '';
	var desc_pcb_life1 = '';
	var desc_fib_life1 = '';
	var desc_basic_life2 = '';
	var desc_pcb_life2 = '';
	var desc_fib_life2 = '';
	var p_installment = '';
	var freq = '';
	var tandc_title = '';
// 	var refund = '';
// 	var refund_amount = '0';
	var successEn = '${successEn}';
	var successKh = '${successKh}';
	var isSubmitted = '';
	
	$(document).ready(function(){
		togglePrintStyle();
		
		$('#btnAgreed').on('click', function(e){
			e.preventDefault();
			
			$('form')[0].submit();
		})
		
		$('#agreed').on('change', function(){
			if($(this).is(':checked')){
				$('#btnSubmit').removeAttr('disabled');
			}else{
				$('#btnSubmit').attr('disabled', 'disabled');
			}
		});
		
		
		isSubmitted = '<c:out value="${log.getIsSubmitted()}" />';
		
		if(isSubmitted){
			if($('#toggle-lang').is(':checked')){
				showSuccess.show(successEn, 'en');
			}else{
				showSuccess.show(successKh, 'kh');
			}
		}
	});
	
	function replaceFreqKh(){
		if(freq === 'year'){
			$('#freq').html('ប្រចាំឆ្នាំ');
			$('.freq').html('ប្រចាំឆ្នាំ');
		}else if(freq === 'half-year'){
			$('#freq').html('ប្រចាំឆមាស');
			$('.freq').html('ប្រចាំឆមាស');
		}else if(freq === 'month'){
			$('#freq').html('ប្រចាំខែ');
			$('.freq').html('ប្រចាំខែ');
		}else{
			$('#freq').html('');
			$('.freq').html('');
		}
	}
	
	function togglePrintStyle(){
		$('#subject').toggleClass('view-style view-style-kh');
  		$('#dear').toggleClass('view-style view-style-kh');
  		$('#greeting').toggleClass('view-style view-style-kh');
  		$('#choose-option').toggleClass('view-style view-style-kh');
  		$('#option-1-intro').toggleClass('option-1-intro option-1-intro-kh');
  		$('#option-1-intro-2').toggleClass('view-style view-style-kh');
  		$('#ul-1').toggleClass('view-style view-style-kh');
  		$('#ul-2').toggleClass('view-style view-style-kh');
  		$('#ul-3').toggleClass('view-style view-style-kh');
  		$('#ul-4').toggleClass('view-style view-style-kh');
  		$('#option-2-intro').toggleClass('view-style view-style-kh');
  		$('#ol-1').toggleClass('view-style view-style-kh');
  		$('#ol-2').toggleClass('view-style view-style-kh');
  		$('#note').toggleClass('view-style view-style-kh');
  		$('#contact').toggleClass('view-style view-style-kh');
  		$('#th-desc').toggleClass('view-style view-style-kh');
  		$('#th-current').toggleClass('view-style view-style-kh');
  		$('#th-extra').toggleClass('view-style view-style-kh');
  		$('#desc-basic-life1').toggleClass('view-style view-style-kh');
  		$('#desc-pcb-life1').toggleClass('view-style view-style-kh');
  		$('#desc-fib-life1').toggleClass('view-style view-style-kh');
  		$('#desc-basic-life2').toggleClass('view-style view-style-kh');
  		$('#desc-pcb-life2').toggleClass('view-style view-style-kh');
  		$('#desc-fib-life2').toggleClass('view-style view-style-kh');
  		$('#p-installment').toggleClass('view-style view-style-kh');
  		$('#tandc-title').toggleClass('tandc-title tandc-title-kh');
  		$('#ul-main').toggleClass('view-style view-style-kh');
  		$('#refund').toggleClass('view-style view-style-kh');
  		
//   		if($('.same').text().indexOf('Same') !== -1){
//   			$('.same').text('ដដែល');
//   		}else{
//   			$('.same').text('Same');
//   		}
  		
//   		if($('#name').text().indexOf('Name') !== -1){
//   			$('#name').text('ឈ្មោះ ');
//   		}else{
//   			$('#name').text('Name ');
//   		}
  		
//   		if($('.agreed-text').text().indexOf('Agreed on') !== -1){
//   			$('.agreed-text').text('យល់ព្រមនៅថ្ងៃទី ___ ខែ ___ ឆ្នាំ ______');
//   		}else{
//   			$('.agreed-text').text('Agreed on ___ / ___ / ______');
//   		}
  		
//   		if($('#btnAgreed').text() === 'Upgrade my protection'){
//   			$('#btnAgreed').text('តម្លើងអត្ថប្រយោជន៍ការពាររបស់ខ្ញុំ');
//   		}else{
//   			$('#btnAgreed').text('Upgrade my protection');
//   		}
	}
</script>
</body>
</html>