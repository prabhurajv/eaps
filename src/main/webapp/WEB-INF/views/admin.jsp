<%@ include file="shared/header.jspf" %>
<body>
<%@ include file="shared/navbar.jspf" %>

<div class="container" style="margin-top: 60px;">
	<div class="row" style="margin: 0px; margin-top: 10px;border:1px solid #ccc; border-top-right-radius:10px;border-top-left-radius:10px;margin-bottom:10px;">
		<div class="col-lg-12">
			<div class="row" style="background:#EEEEEE;border-bottom:1px solid #ccc; border-top-right-radius:10px;border-top-left-radius:10px;">
				<div style="padding:10px">
					<span style="font-size: 16px; color: #D9534F; font-weight: 500;">Change Front Page Banner</span>
				</div>
			</div>
			<div class="row" style="padding-top: 10px; padding-bottom: 10px;">
				<div class="col-lg-12">
					<form:form method="post" enctype="multipart/form-data" id="frmAdmin" commandName="data" action="admin?${_csrf.parameterName}=${_csrf.token}">
						<div class="row">
							<div class="col-lg-3">
								<label class="form-label">Path to Image</label>
							</div>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-lg-9">
										<div class="row">
											<div class="col-lg-12">
												<form:input type="file" path="image" class="form-control" accept=".JPG" />
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<c:if test="${error != null }">
													<span class="text-danger">${error }</span>
												</c:if>
												<c:if test="${msg != null }">
													<span class="text-success">${msg }</span>
												</c:if>
											</div>
										</div>
									</div>
									<div class="col-lg-3">
										<input type="submit" name="btnUpload" value="Upload" class="btn btn-danger" />
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
</body>
</html>

<script>
	$(document).ready(function(){
		$('#frmAdmin').validate({
			rules:{
				image: {
					required: true,
					extension: 'jpg'
				}
			},
			messages:{
				image: {
					extension: 'Image file must be of type JPEG'
				}
			},
			highlight: function(element) {
				$(element).parent().removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).parent().removeClass('has-error').addClass('has-success');
			}
		})
		
		StartWarningTimer();
	})
	
	$('#btnStayLoggedIn').click(function(){
		ResetTimeOutTimer();
	})
</script>