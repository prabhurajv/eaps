<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
	
	.login-style{
		background-image: -webkit-linear-gradient(90deg, #FFFFFF, #EEEEEE);
		box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.2) inset, 0px 0px 2px rgba(0, 0, 0, 0.5);
		padding: 10px;
	}
</style>
<body style="height: 100%">
	<%@ include file="shared/navbar.jspf" %>
	<div class="row" style="margin: 0px; height: 60px"></div>
	<br /><br /><br /><br /><br /><br /><br />
	<div class="container" >
		<div class="login-style" align="center">
			<h2 style="color: #EE1B2E; font-weight:bold;">Application Encountered Error</h2><br/><br />
			<h4 style="color: black;">The application has encountered error while processing your request.</h4>
			<h4 style="color: black;">Please try again. If the problem still persists please contact IT Support.</h4>
		</div>
	</div>
<div class="row" style="margin: 0px; height: 30px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	$('#btnStayLoggedIn').click(function(){
		ResetTimeOutTimer();
	})
</script>
</body>
</html>