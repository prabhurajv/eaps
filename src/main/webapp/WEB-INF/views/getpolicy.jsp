﻿<%@ include file="shared/header.jspf" %>
<style>
	html, body{
		height: 100%;
		width: 100%;
	}
	
	html {
		display: table;
	}
	
	body{
		display: table-cell;
		vertical-align: middle;
	}
	
	.bg-error{
		background-color: #d9534f;
		color: white;
		border: 1px solid yellow;
	}
	
	.bg-error:focus{
		border: 1px solid yellow;
	}
	
	.bg-success{
		background-color: #fff;
		color: black;
		border: 1px solid white;
	}
	
	.error{
		font-size: 13px;
	}
	
	.note{
		padding-bottom: 5px;
		margin-bottom: 5px;
	}
	
	.contact{
		color: white;
	}
	
	.contact:hover{
		color: yellow;
	}
</style>
<body>
<div class="container" style="border:1px solid #eee;padding:5px;max-width:75%;-webkit-box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.30);
-moz-box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.30);
box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.30);">
	<div class="row">
		<div class="col-lg-12">
			<img src="resources/images/ads.jpg" style="width: 100%" />
		</div>
	</div>
	<div class="row" style="font-family: Khmer OS">
		<div class="col-lg-12">
			<div class="row" style="margin: 0px; margin-top: 5px; background-color: #ED1B2E;">
				<div class="row" style="margin: 0px; padding-top: 10px;">
					<div class="col-lg-12" style="margin-top: 5px; background-color: #ED1B2E; color: white; font-size: 12px;">
						<form:form method="post" autocomplete="off" commandName="data" id="getpolicy">
							<div class="col-lg-3">
								<label class="form-label" style="font-family: Khmer OS">សូមបញ្ចូលថ្ងៃខែឆ្នាំកំណើត (ថ្ងៃខែឆ្នាំ) <br/> Date of Birth (ddmmyyyy)</label>
							</div>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-lg-9">
										<div class="row">
											<div class="col-lg-12">
												<form:input type="text" name="dob" path="dob" class="form-control" placeholder="Eg: 28072016" style="margin-bottom: 5px;" />
												<form:input type="hidden" name="arg1" path="encryptString" value="${arg1 }" />
												<label class="form-label text-danger" style="color: white; font-size: 13px;">${error}</label>				
											</div>
										</div>
<!-- 										<div class="row"> -->
<!-- 											<div class="col-lg-12"> -->
<!-- 												<div class="g-recaptcha" data-sitekey="6LfxUCcTAAAAAK3VbQvhq-pYwaPYt6l3kgCKKXrj" style="transform:scale(0.70);-webkit-transform:scale(0.70);transform-origin:0 0;-webkit-transform-origin:0 0;"></div> -->
<!-- 											</div> -->
<!-- 										</div> -->
									</div>									
									<div class="col-lg-3">
										<center><input type="submit" value="ទាញយក / Download" id="btnSubmit" class="btn btn-danger" style="font-family: Khmer OS; border: 1px solid #FFF" /></center>
									</div>
								</div>
							</div>
						</form:form></div>
				</div>
				<div class="row" style="margin: 0px; margin-top: 5px; margin-bottom: 5px;">
					<div class="col-lg-12" style="margin-top:0px; background-color: #ED1B2E; color: white; font-size: 10px; padding-bottom: 10px">
						<form:form method="post" autocomplete="off" commandName="data">
							<div class="col-lg-12" style="font-family: Khmer OS">
								<p class="note">សូមវាយថ្ងៃខែឆ្នាំកំណើតរបស់លោកអ្នកតាមទម្រង់ DDMMYYYY ចូលក្នុងប្រអប់ខាងលើ រួចចុចលើពាក្យ ទាញយក។ លេខសម្ងាត់សម្រាប់បើកលិខិតជូនព័ត៌មានអំពីខួបនៃបណ្ណសន្យារ៉ាប់រងគឺ ថ្ងៃខែឆ្នាំកំណើតដដែល។</p>
								<p class="note">Please type your date of birth in the format of DDMMYYYY into the above given box and click Download. The password for openning the PDF file (Annual Policy Statement) is the same date of birth.</p>
								<p class="note">សម្រាប់ព័ត៌មានបន្ថែម សូមទាក់ទងមកកាន់ / For more info, please contact at:  <a class="contact" href="tel:023964222">023 964 222</a> / <a class="contact" href="tel:1800212223">1800 212 223</a> (ឥតគិតថ្លៃ / Free) ឬ  <a class="contact" href="www.prudential.com.kh">www.prudential.com.kh</a></p>							
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<%@ include file="shared/footer.jspf" %>
</body>
</html>
<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
<script>
	$(document).ready(function(){
		$.validator.addMethod("exactlength", function(value, element, param) {
			 return this.optional(element) || value.length == param;
			});
		
		$('#getpolicy').validate({
			rules: {
				dob: {
					required: true,
					number: true,
					exactlength: 8
				}
			},
			messages: {
				dob: {
					required: 'សូមបញ្ចូលថ្ងៃខែឆ្នាំកំនើត <br /> This field is required',
					number: 'អាចបញ្ចូលបានតែលេខប៉ុណ្ណោះ <br /> Number only',
					exactlength: 'ត្រូវមានលេខ៨ខ្ទង់ <br /> Must be 8 digits'
				}
			},
			highlight: function(element) {
// 				$(element).parent().removeClass('has-success').addClass('has-error');
				$(element).removeClass('bg-success').addClass('bg-error');
				$(element).css('border:focus', '1px solid yellow');
			},
			success: function(element) {
// 				$(element).parent().removeClass('has-error').addClass('has-success');
				$(element).prev().removeClass('bg-error').addClass('bg-success');
			}
		});
	});
</script>