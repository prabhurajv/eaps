﻿<%@ include file="shared/header.jspf"%>
<script>
	window.TaxChange = '${TaxChange}';
	window.Messages = '${Messages}';
	window.IsAnonymous = '${IsAnonymous}';
	window.error = '${submitError}';
	window.submit = '${submit}';
	window.firstLoadAfterSubmit = '${firstLoadAfterSubmit}';
</script>
<script id="TemplateKH" type="text/plain">
	${TemplateKH}
</script>
<script id="TemplateEN" type="text/plain">
	${TemplateEN}
</script>
<script id="TaxChangeConfirmMessageModal" type="text/plain">
	<div id="modalTemplate" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="btn btn-danger close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><b></b></h4>
				</div>
				<div class="modal-body">
					<center>
						<h4 id="successMsg" style="font-family: Khmer OS;">
						</h4>
					</center>
					<br>
					<form method="post" id="interestedUpsell" style="display:none" action="<c:url value="/upsellInterested"/>">
						<p class="upsQuest" id="upsellQuestion"></p>
						<div class="btn-footer-container upsQuest">
							<button type="submit" class="btn-decision" id="is_upsell_interested" name="is_upsell_interested" value="Y"><span id="is_upsell_interested_y"></span></button>
							<button type="submit" class="btn-decision" id="is_upsell_interested" name="is_upsell_interested" value="N"><span  id="is_upsell_interested_n"></span></button>
						</div>
						<br>
						<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					</form>
				</div>
				<!-- <div class="modal-footer">
					<center><button type="submit" class="btn btn-default" id="btnModalSubmit">OK</button></center>
				</div> -->
			</div>
		</div>
	</div>
</script>

<style>
	html {
		height: 100%;
	}
	@page {

        size: A4;

        margin: 0.5in 0.4in 0.5in 0.4in;
    }

	.popover {
		background-color: #C9302C;
		color: #FFFFFF;
		border: 1px solid #C9302C;
		padding: 5px;
		font-size: 12px;
		max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom>.arrow:after {
		border-bottom-color: #C9302C;
	}

	@media (max-width : 768px) {
		#title {
			font-size: 16px;
		}
	}

	.option-1-intro {
		font-size: 15px;
	}

	.tandc-title {
		font-size: 14px;
	}

	p {
		line-height: 1.7em;
	}

	.pru-panel {
		margin: 0 20px;
		margin-top: 10px;
		margin-bottom: 155px;
	}

	.print-preview-kh {
		margin-left: 20px;
		margin-right: 20px;
		font-family: PRUCam;
		font-size: 13px;
	}

	.print-preview-en {
		margin-left: 20px;
		margin-right: 20px;
		font-family: Verdana;
		font-size: 11px;
	}

	@media print {

		.print-preview-kh {
			margin-left: 0px;
			margin-right: 10px;
			font-family: PRUCam;
			font-size: 13px;
		}

		.print-preview-en {
			margin-left: 0px;
			margin-right: 10px;
			font-family: Verdana;
			font-size: 11px;
		}

		.pru-panel {
			margin: 0px 1px;
		}

		.panel {
			border: none;
		}

		.panel-heading {
			border-color: transparent;
		}

		.btn {
			border: none;
		}

		.toggle {
			display: none;
		}

		/* 		p{ */
		/* 			margin: 0px; */
		/* 		} */
		legend {
			margin-bottom: 0px;
		}

		th {
			padding-top: 0px;
		}

		td {
			padding-top: 0px;
			padding-bottom: 0px;
		}

		a[href]:after {
			content: none !important;
		}
	}

	/* Todo: Merge with footer decision css*/

	.footer-decision {
		position: fixed;
		left: 25%;
		bottom: 0;
		width: 50%;
		height: 90px;
		margin-left: 0%;
		text-align: center;

	}

	#footer-tit-style {
		margin: 0 auto;
		border: 2px solid gray;
		padding: 14px;
		text-align: center;
		border-radius: 3px;
		margin-bottom: 8px;
	}

	.container-decision {
		padding: 0px 20px;
	}

	.btn-decision {
		background-color: #C9302C;
		border: none;
		color: white;
		padding: 8px 21px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		border-radius: 4px;
	}

	#main-btn-decision {
		padding: 17px;
		text-align: center;
	}

	.upsQuest {
		text-align: center;
	}

	/* End of style for footer decision */
</style>

<body style="height: 100%" ng-app="myApp" ng-controller="myCtrl">
	<div class="panel panel-danger pru-panel">
		<div class="panel-heading" style="background-color: #EE1B2E; border-color: #d43f3a;">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px;">
				<div class="col-xs-12" style="padding: 0px;">
					<h3 class="pull-left" id="title"
						style="font-family: Khmer OS; color: white; line-height: 0; margin-top: 15px;">
						Prudential (Cambodia) Life Assurance PLC</h3>
					<input toggle-checkbox ng-model="khLanguage" type="checkbox" ng-change="switchLanguage()"
						data-size="small" data-on="ខ្មែរ" data-off="En" data-onstyle="danger"></input><br>
				</div>
			</div>
		</div>
		<div style="padding-top: 15px;" ng-print-preview="PrintPreview">
		</div>
	</div>
	<div class="footer-decision hidden-print" id="protectionUpgradeBox" style="display:none">
		<form method="post" id="customerdecision">
			<div class="container-decision">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<button type="button" class="btn btn-danger btn-lg" id="main-btn-decision" ng-click="submit()"
					ng-bind="findMessage('customerDecisionQuestion')"></button>
			</div>
		</form>

	</div>

	<%@ include file="shared/footerinfo.jspf"%>
	<%@ include file="shared/footer.jspf"%>

	<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />">



</script>
<script type="text/javascript" src="<c:url value="/resources/js/printpremtaxch.js" />"></script>
</body>

</html>