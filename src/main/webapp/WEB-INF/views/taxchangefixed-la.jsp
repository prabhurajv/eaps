﻿<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
	
	@media(max-width: 768px){
		#title{
			font-size: 16px;
		}
	}
	
	p{
		line-height: 2em;
	}
	
	@media print{
		.panel{
			border: none;
		}
		
		.panel-heading{
			border-color: transparent;
		}
		
		.btn{
			border: none;
		}
		
		.toggle{
			display: none;
		}
		
		a[href]:after {
		    content: none !important;
	    }
	    
	    .view-style{
	    	font-size: 12px;
	    }
	}
</style>
<body style="height: 100%;">
	<div class="panel panel-danger" style="margin: 0 20px; margin-top: 10px;">
		<div class="panel-heading" style="background-color: #EE1B2E; border-color: #d43f3a;">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px;">
				<div class="col-lg-12" style="padding: 0px;">
					<h3 class="pull-left" id="title" style="font-family: Khmer OS; color: white; line-height: 0; margin-top: 15px;">
						Prudential (Cambodia) Life Assurance PLC
					</h3>
					<input type="checkbox" checked data-toggle="toggle" data-size="small"
					 data-on="ខ្មែរ" data-off="En" data-onstyle="danger" id="toggle-lang" />
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px; font-family: Khmer OS;">
				<form method="post">
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.getSendDateFormated(\"dd/MM/yyyy\") }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">Policy Number: <c:out value="${ log.chdrNum }" /></span><br/>
<%-- 					<span class="text-danger" style="font-size: 12px;">Date of Birth: <fmt:formatDate type="date" pattern="dd MMMM yyyy" value="${ log.taxChange.get(0).poDob }" /></span><br/> --%>
					<span class="text-danger" style="font-size: 12px;">FC/LIC: <c:out value="${ log.taxChange.get(0).agntName }" /></span><br/>
					<p style="font-size: 12px;">
						<br/><b id="subject">កម្មវត្ថុ	៖  ការកាត់បន្ថយបុព្វលាភរ៉ាប់រង អាស្រ័យដោយការផ្លាស់ប្តូរនៃច្បាប់ពន្ធ</b>
						<br/><br/><span id="dear">ជូនចំពោះ</span> <c:out value="${ log.taxChange.get(0).salutl.trim() }. " /> <c:out value="${ log.taxChange.get(0).poName.trim() }" />,<br/>
						<br/><span id="greeting">
							យើងខ្ញុំសូមថ្លែងអំណរគុណចំពោះការជឿទុកចិត្ដនិងការជ្រើសរើសយកក្រុមហ៊ុនព្រូដិនសល (ខេមបូឌា) ឡាយហ្វ៍ អឹសួរ៉ិនស៍ ម.ក (ក្រុមហ៊ុន) ធ្វើជាក្រុមហ៊ុនផ្តល់សេវាធានារ៉ាប់រងអាយុជីវិតជូនលោកអ្នក។
						</span>
						<br/>
					</p>
					<p id="greeting2" style="font-size: 12px;">
						យើងខ្ញុំមានសេចក្តីសោមនស្សរីករាយសូមជម្រាបជូនលោកអ្នកថា ពន្ធដែលមានអត្រា 5% លើបុព្វលាភរ៉ាប់រងនៃសេវាធានារ៉ាប់រង​អាយុជីវិតត្រូវបានផ្លាស់ប្តូរ។ អាស្រ័យដោយការផ្លាស់ប្ដូរនេះ បុព្វលាភរ៉ាប់រងសរុបត្រូវទូទាត់របស់លោកអ្នក ត្រូវបានកាត់បន្ថយពី US$<span id="current-p"><fmt:formatNumber type="number" pattern="###,##0.00" value="${ log.taxChange.get(0).installmentBeforeTaxchange }" /></span> ទៅ US$<span id="new-p"><fmt:formatNumber type="number" pattern="###,##0.00" value="${ log.getTotalInstallmentAfterTaxChange() }" /></span> ដោយយោងទៅតាមលទ្ធផលនៃការវាយតម្លៃជាក់ស្ដែងលើសំណើសុំបន្តសុពលភាពនៃបណ្ណ​សន្យារ៉ាប់រង គិតចាប់ពីកាលបរិច្ឆេទបន្តសុពលភាព​នៃបណ្ណសន្យារ៉ាប់រងតទៅ ដោយមិនប៉ះពាល់ដល់អត្ថប្រយោជន៍ការពារ​បច្ចុប្បន្នរបស់លោកអ្នក ឡើយ។
					</p>
						
					<p id="detail" style="font-size: 12px;">
						សូមលោកអ្នកអញ្ជើញទៅកាន់ធនាគារដើម្បីកែប្រែបញ្ជាអចិន្ត្រៃយ៍ថេរបច្ចុប្បន្នរបស់លោកអ្នកឲ្យបានត្រឹមត្រូវទៅតាមចំនួនបុព្វលាភ រ៉ាប់រងថ្មីខាងលើឲ្យបានឆាប់រហ័សតាមការគួរ។ ការកែប្រែនេះគឺពាក់ព័ន្ធនឹងការបំពេញសំណុំបែបបទស្នើសុំថ្មី ដើម្បីដាក់ឲ្យដំណើរ​ការជាថ្មី។ សូមផ្ញើច្បាប់ចម្លងនៃការកែប្រែបញ្ជាអចិន្ត្រៃយ៍ថេរមកកាន់មជ្ឈមណ្ឌលផ្ដល់សេវាអតិថិជន អាសយដ្ឋាន អគារភ្នំពេញ    ថោវើរ៍ ជាន់ទី 20 លេខ 445 មហាវិថីព្រះមុនីវង្ស សង្កាត់បឹងព្រលិត ខណ្ឌ 7 មករា   រាជធានីភ្នំពេញ។
					</p>
<!-- 					<p style="font-size: 12px;"> -->
					<p id="note" style="font-size: 12px;">
						បុព្វលាភរ៉ាប់រងដែលត្រូវកាត់បន្ថយនឹងចាប់មានសុពលភាពបន្ទាប់ពីក្រុមហ៊ុនទទួលបានច្បាប់ចម្លងនៃការកែប្រែបញ្ជាអចិន្ត្រៃយ៍ថេរ​ពីលោកអ្នក។ រាល់ចំនួនបុព្វលាភរ៉ាប់រង ដែលបានបង់លើស នឹងត្រូវបានបង្វិលទៅក្នុងគណនីធនាគារដែលមានបញ្ជាក់ក្នុងបញ្ជា       អចិន្ត្រៃយ៍ថេរថ្មីរបស់លោកអ្នកវិញ។
					</p>
					
					<p id="note2" style="font-size: 12px;">
						ទន្ទឹមនឹងនេះ លោកអ្នកក៏នៅតែអាចទទួលបានឱកាសតម្លើងអត្ថប្រយោជន៍ការពារដែរ ដើម្បីផ្ដល់ជូនលោកអ្នក ព្រមទាំងគ្រួសារ​នូវភាពស្ងប់ចិត្តមួយកម្រិតថែមទៀតជាមួយនឹងបុព្វលាភរ៉ាប់រងសរុបចំនួនដដែល។ បើលោកអ្នកមានបំណងតម្លើង​អត្ថប្រយោជន៍​ការពារ សូមទាក់ទងភ្នាក់ងារលក់របស់លោកអ្នក។ បន្ទាប់មកទៀតក្រុមហ៊ុននឹងទាក់ទងលោកអ្នកដើម្បីសាកសួរ​បន្ថែមអំពីព័ត៌មាន​ស្ថានភាពសុខភាព ដោយរាប់បញ្ចូលទាំងការធានានូវស្ថានភាពល្អប្រសើរនៃសុខភាពផង ។
					</p>

					<p id="contact" style="font-size: 12px;">
						សូមលោកអ្នកទាក់ទងភ្នាក់ងារលក់របស់លោកអ្នកឈ្មោះ <span id="agent-name"><c:out value="${ log.taxChange.get(0).agntName }"/></span> ទូរស័ព្ទលេខ <span id="agent-phone"><c:out value="${ log.taxChange.get(0).agntPhone }"/></span> ឬ បុគ្គលិកផ្នែក​ទំនាក់​ទំនង អតិថិជនតាមរយៈលេខ 1800-212223 (ឥតគិតថ្លៃ) ឬ 023 964 222 ប្រសិនបើលោកអ្នកមានសំណួរផ្សេងៗ។
					</p>
					
					<br/>
					<div class="row">
						<div class="col-md-6">
							<div class="pull-left">
								<p id="sign" class="view-style">ដោយក្តីគោរពដ៏ខ្ពង់ខ្ពស់</p>
								<img src="<c:url value="/resources/images/CEO Signature.JPG"/>" />
								<br/><span class="view-style">David Nutman</span>
								<br/><span id="pos" class="view-style">អគ្គនាយក</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="pull-right" style="margin-right: 40px; font-size: 12px;">
								<span class="agreed-text">យល់ព្រមនៅថ្ងៃទី ___ / ___ / ______</span>
								<br/><span style="color: lightgray;">ហត្ថលេខា ឬស្នាមមេដៃ</span>
								<br/><br/><br/><br/><br/>
								<span><span id="name">ឈ្មោះ </span><u><c:out value="${ log.taxChange.get(0).poName }" /></u></span>
							</div>
						</div>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
	<div class="row" style="margin: 0px; height: 100px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	var subject = '';
	var dear = '';
	var greeting = '';
	var greeting2 = '';
	var choose_option = '';
	var note = '';
	var note2 = '';
	var sign = '';
	var pos = '';
	var tandc = '';
	var tandc2 = '';
	var data = [];
	var tandc_en = [];
	var tandc_kh = [];
	var contact = '';
	var agent_name = '';
	var agent_phone = '';
	var current_p = '';
	var new_p = '';
	var detail = '';
	var effective_date = '';
	
	$(document).ready(function(){
		
		
		$('#agreed').on('change', function(){
			if($(this).is(':checked')){
				$('#btnSubmit').removeAttr('disabled');
			}else{
				$('#btnSubmit').attr('disabled', 'disabled');
			}
		});
		
		
	});
</script>
</body>
</html>