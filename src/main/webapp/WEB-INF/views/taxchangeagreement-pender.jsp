﻿<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
	
	@media(max-width: 768px){
		#title{
			font-size: 16px;
		}
	}
	
	@media print{
		.panel{
			border: none;
		}
		
		.panel-heading{
			border-color: transparent;
		}
		
		.btn{
			border: none;
		}
		
		.toggle{
			display: none;
		}
		
		p{
			margin: 0px;
		}
		
		legend{
			margin-bottom: 0px;
		}
		
		#tandc-box{
			margin-bottom: 30px;
		}
	}
</style>
<body style="height: 100%">
	<div class="panel panel-danger" style="margin: 0 20px; margin-top: 10px;">
		<div class="panel-heading" style="background-color: #EE1B2E; border-color: #d43f3a;">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px;">
				<div class="col-lg-12" style="padding: 0px;">
					<h3 class="pull-left" id="title" style="font-family: Kh-Metal-Chrieng; color: white; line-height: 0; margin-top: 15px;">
						Prudential Cambodia Life Assurance
					</h3>
					<input type="checkbox" checked data-toggle="toggle" data-size="small"
					 data-on="En" data-off="Kh" data-onstyle="danger" id="toggle-lang" />
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px; font-family: Kh-Metal-Chrieng;">
				<form method="post">
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.getSendDateFormated(\"dd/MM/yyyy\") }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">Policy Number: <c:out value="${ log.chdrNum }" /></span><br/>
<%-- 					<span class="text-danger" style="font-size: 12px;">Date of Birth: <fmt:formatDate type="date" pattern="dd MMMM yyyy" value="${ log.taxChange.get(0).poDob }" /></span><br/> --%>
					<span class="text-danger" style="font-size: 12px;">FC/LIC: <c:out value="${ log.taxChange.get(0).agntName }" /></span><br/>
					<p style="font-size: 12px;">
						<br/><b id="subject">Subject: Upgraded protection due to tax change</b>
						<br/><br/><span id="dear">Dear</span> <c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" />,
						<br/><span id="greeting">
							<br/>We would like to thank you for trusting and choosing Prudential (Cambodia) Life Assurance PLC (the Company) as your life insurance provider. 
							<br/><br/>It gives us great pleasure to inform you that the tax on life insurance based on 5% of premium has been revised.  We have upgraded your protection benefits from (Basic + Parent Coverage rider) US$<span id="sa-before"><c:out value="${log.getTotalSABeforeTaxChange()}"/></span> to US$<span id="sa-after"><c:out value="${log.getTotalSAAfterTaxChange()}"/></span> without any additional evidence of insurability at the same total premium.
						</span>
					</p>
					<p id="option-1-intro" style="font-size: 15px; color: red; font-weight: 600;">
						Providing greater peace of mind for you and your family at the same premium!
					</p>
					<span id="option-1-intro-2" style="font-size: 12px;">
						To enjoy this upgrade, all you need to do is to confirm your acceptance by:
					</span>
					<br/>
					<ul style="font-size: 12px;">
						<li>
							<span id="ul-1">Signing or thumb-printing at the bottom part of the table below after reading the terms and conditions, and sending the signed or thumb-printed copy to us through your servicing agent or at Phnom Penh Tower, Ground Floor, #445, Preah Monivong Blvd, Boeung Prolit, 7 Makara, Phnom Penh.</span>
							<br/><br/>
							<fieldset class="scheduler-border" style="margin-top: 10px;">
								<legend class="scheduler-border" style="margin-bottom: 0px;">
									<span id="choose-option" class="btn btn-sm btn-danger">
										Summary of options
									</span>
								</legend>
								<div class="row table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th style="max-width: 200px;">
													<span id="th-desc">Description</span>
												</th>
												<th style="word-wrap: break-word;">
													<span id="th-current">Current benefits and premium 
													<br />installment/year</span>
												</th>
												<th style="word-wrap: break-word;">
													<c:choose>
														<c:when test="${ !log.approved && !log.rejected }">
															<input type="radio" name="rdoOption" id="rdoIncrease" value="increase" checked />
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${ not empty log.approvedDate }">
																	<input type="radio" name="rdoOption" id="rdoIncrease" value="increase" 
																		checked disabled />
																</c:when>
																<c:otherwise>
																	<input type="radio" name="rdoOption" id="rdoIncrease" value="increase"
																	 	disabled />
																</c:otherwise>
															</c:choose>
														</c:otherwise>
													</c:choose>
													<span id="th-extra">Extra benefits and premium 
													<br />installment/year</span>
												</th>
		<!-- 										<th style="word-wrap: break-word;"> -->
		<%-- 											<c:choose> --%>
		<%-- 												<c:when test="${ !log.approved && !log.rejected }"> --%>
		<!-- 													<input type="radio" name="rdoOption" id="rdoRefund" value="refund" /> -->
		<%-- 												</c:when> --%>
		<%-- 												<c:otherwise> --%>
		<%-- 													<c:choose> --%>
		<%-- 														<c:when test="${ not empty log.rejectedDate }"> --%>
		<!-- 															<input type="radio" name="rdoOption" id="rdoRefund" value="refund"  -->
		<!-- 																checked disabled /> -->
		<%-- 														</c:when> --%>
		<%-- 														<c:otherwise> --%>
		<!-- 															<input type="radio" name="rdoOption" id="rdoRefund" value="refund" -->
		<!-- 															 	disabled /> -->
		<%-- 														</c:otherwise> --%>
		<%-- 													</c:choose> --%>
		<%-- 												</c:otherwise> --%>
		<%-- 											</c:choose> --%>
		<!-- 											Same benefits and premium  -->
		<!-- 											installment/year -->
		<!-- 										</th> -->
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="3" style="border: none; max-height: 200px;">
													<c:out value="${ log.lifePFs.get(0).laName }" />
												</td>
											</tr>
											<tr>
												<td style="padding-left: 20px; max-width: 200px;">
													<span id="desc-basic-life1">Basic Sum Assured</span>
												</td>
												<td style="max-width: 200px">
													<c:out value="${ log.lifePFs.get(0).basicSA }" />
												</td>
												<td style="max-width: 200px">
													Same
												</td>
		<!-- 										<td style="max-width: 200px"> -->
		<%-- 											<c:out value="${ log.lifePFs.get(0).basicSA }" /> --%>
		<!-- 										</td> -->
											</tr>
											<c:if test="${ log.lifePFs.get(0).hasRtr1.trim().toLowerCase().equals('yes') }">
												<tr>
													<td style="padding-left: 20px; max-width: 200px;">
														<span id="desc-pcb-life1">Parent Coverage Benefit</span>
													</td>
													<td style="max-width: 200px">
														<c:out value="${ log.lifePFs.get(0).pcb }" />
													</td>
													<td style="color: red; max-width: 200px;">
														<c:out value="${ log.lifePFs.get(0).pcb + log.taxChange.get(0).extraSA }" />
													</td>
		<!-- 											<td style="max-width: 200px"> -->
		<%-- 												<c:out value="${ log.lifePFs.get(0).pcb }" /> --%>
		<!-- 											</td> -->
												</tr>
											</c:if>
											<c:if test="${ log.lifePFs.get(0).hasRsr1.trim().toLowerCase().equals('yes') }">
												<tr>
													<td style="padding-left: 20px; max-width: 200px;">
														<span id="desc-fib-life1">Family Income Benefit</span>
													</td>
													<td style="max-width: 200px">
														<c:out value="${ log.lifePFs.get(0).fib }" />
													</td>
													<td style="max-width: 200px">
														Same
													</td>
		<!-- 											<td style="max-width: 200px"> -->
		<%-- 												<c:out value="${ log.lifePFs.get(0).fib }" /> --%>
		<!-- 											</td> -->
												</tr>
											</c:if>
											<c:if test="${ log.lifePFs.size() > 1 }">
												<tr>
													<td colspan="3" style="border: none;">
														<c:out value="${ log.lifePFs.get(1).laName }" />
													</td>
												</tr>
<!-- 												<tr> -->
<!-- 													<td style="padding-left: 20px; max-width: 200px;"> -->
<!-- 														<span id="desc-basic-life2">Basic Sum Assured</span> -->
<!-- 													</td> -->
<!-- 													<td style="max-width: 200px"> -->
<%-- 														<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 													</td> -->
<!-- 													<td style="max-width: 200px"> -->
<%-- 														<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 													</td> -->
<!-- 													<td style="max-width: 200px"> -->
<%-- 														<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 													</td> -->
<!-- 												</tr> -->
												<c:if test="${ log.lifePFs.get(1).hasRtr1.trim().toLowerCase().equals('yes') }">
													<tr>
														<td style="padding-left: 20px; max-width: 200px;">
															<span id="desc-pcb-life2">Parent Coverage Benefit</span>
														</td>
														<td style="max-width: 200px">
															<c:out value="${ log.lifePFs.get(1).basicSA }" />
														</td>
														<td style="color: red; max-width: 200px;">
															<c:out value="${ log.lifePFs.get(1).basicSA + log.taxChange.get(1).extraSA }" />
														</td>
		<!-- 												<td style="max-width: 200px"> -->
		<%-- 													<c:out value="${ log.lifePFs.get(1).pcb }" /> --%>
		<!-- 												</td> -->
													</tr>
												</c:if>
												<c:if test="${ log.lifePFs.get(1).hasRsr1.trim().toLowerCase().equals('yes') }">
													<tr>
														<td style="padding-left: 20px; max-width: 200px;">
															<span id="desc-fib-life2">Family Income Benefit</span>
														</td>
														<td style="max-width: 200px">
															<c:out value="${ log.lifePFs.get(1).fib }" />
														</td>
														<td style="max-width: 200px">
															<c:out value="${ log.lifePFs.get(1).fib }" />
														</td>
		<!-- 												<td style="max-width: 200px"> -->
		<%-- 													<c:out value="${ log.lifePFs.get(1).fib }" /> --%>
		<!-- 												</td> -->
													</tr>
												</c:if>
											</c:if>
											<tr>
												<td style="border: none; max-width: 200px;">
													<span id="p-installment">Premium Installment /</span>
													<c:choose>
														<c:when test="${ log.taxChange.get(0).freq.trim() == \"01\" }">
															<span id="freq">year</span>
														</c:when>
														<c:when test="${ log.taxChange.get(0).freq.trim() == \"02\" }">
															<span id="freq">half-year</span>
														</c:when>
														<c:when test="${ log.taxChange.get(0).freq.trim() == \"12\" }">
															<span id="freq">month</span>
														</c:when>
													</c:choose>
												</td>
												<td style="border: none;">
													<c:out value="${ log.taxChange.get(0).installmentBeforeTaxchange }" />
												</td>
		<!-- 										<td style="border: none;"> -->
		<%-- 											<c:out value="${ log.taxChange.get(0).apBeforeTaxChange }" /> --%>
		<!-- 										</td> -->
												<td style="border: none;">
													Same
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</fieldset>
							<div id="tandc-box" class="row" style="border: 1px solid grey; padding: 5px; margin-left: 0px; margin-right: 0px;">
								<div class="col-lg-12" style="font-size: 11px;">
									<span id="tandc-title" style="font-size: 14px;">Terms and Conditions</span>
									<br/><br/>
									<ul id="tandc-list">
										<li>I agree to the protection benefits under my policy to be upgraded from my policy effective date <c:out value="${ log.taxChange.get(0).getNextDueDateFormated(\"dd/MM/yyyy\") }"/>.</li>
										<li>I understand and acknowledge for the Company to implement this until the maturity date of my policy.</li>
									</ul>
									<br/>
									<div class="row">
										<jsp:useBean id="now" class="java.util.Date" />
										<fmt:formatDate var="date" value="${now}" pattern="dd/MM/yyyy" />
										<c:if test="${ not empty log && log.approved == true }">
											<fmt:formatDate var="date" value="${log.approvedDate}" pattern="dd/MM/yyyy" />
										</c:if>
										<c:if test="${ not empty log && log.rejected == true }">
											<fmt:formatDate var="date" value="${log.rejectedDate}" pattern="dd/MM/yyyy" />
										</c:if>
										
										<sec:authorize access="isAnonymous()">
											<c:if test="${ empty log || (log.approved == false && log.rejected == false)}">
												<div class="col-xs-6">
													<button type="button" id="btnAgreed" class="btn btn-danger btn-sm">Upgrade my protection</button>
												</div>
											</c:if>
											<c:if test="${ empty log || (log.approved == true || log.rejected == true)}">
												<div class="col-xs-6">
													<button type="button" class="btn btn-danger btn-sm" style="display: none;">Just a button</button>
												</div>
											</c:if>
											<div class="col-xs-6">
												<div class="pull-right">
													<span>Agreed on <u><c:out value="${ date }"/></u></span><br/>
													<br/><br/><br/>
													<span><u><c:out value="${ log.taxChange.get(0).poName }" /></u></span>
												</div>
											</div>
										</sec:authorize>
										<sec:authorize access="isAuthenticated()">
											<div class="col-xs-6">
												<div class="pull-right">
													<span>Agreed on <u><c:out value="${ date }"/></u></span><br/>
													<br/><br/><br/>
													<span><u><c:out value="${ log.taxChange.get(0).poName }" /></u></span>
												</div>
											</div>
										</sec:authorize>
									</div>
								</div>
							</div>
							<br/>
							<br/>
							<br/>
						</li>
						<li id="ul-2">Alternatively, sending an email to us at <a href="mailto:info@prudential.com.kh">info@prudential.com.kh</a>; or</li>
						<li id="ul-3">Calling our Contact Centre at <a href="tel:1800-212-223">1800-212-223</a> (Toll Free) or at <a href="tel:023-964-222">023-964-222</a> (chargeable); or</li>
						<li id="ul-4">Visiting our Customer Service Centre at the address outlined above to confirm your acceptance.</li>
					</ul>
					
					<p id="option-2-intro" style="font-size: 12px;">
						Should you feel that your proposed level of protection is adequate you could choose to retain your proposed level of benefits and your premium will be reduced from US$<span id="current-p"><c:out value="${ log.taxChange.get(0).installmentBeforeTaxchange }" /></span> to US$<span id="new-p"><c:out value="${ log.getTotalInstallmentAfterTaxChange() }" /></span>. To do this:
					</p>
					<ol style="font-size: 12px;">
						<li id="ol-1">Please contact us by calling the Contact Centre or by email, and state that you would like to retain your proposed benefits at the reduced premium.</li>
						<li id="ol-2">
							In addition, you will also need to go to the bank at your earliest convenience to amend your existing standing instruction with the new premium. This may involve completing and processing a new set of forms. Please then send a copy of the authorized standing instruction to us at the contacts provided above.
							<br/>
							The reduced premium will take effect after we have received the revised authorised standing instruction from you. Any over payment of premium will be refunded to your bank account stated in your standing instruction.
						</li>
					</ol>
					<p id="note" style="font-size: 12px;">
						We recommend that you consult your servicing agent to perform a review of your insurance needs so that you can make an informed decision.
						<br/>
						Should we not receive a reply from you within 2 months from the date of this letter and you continue to pay the premiums, we will take this to be your agreement to the higher protection benefits.
					</p>
					
					<p id="contact" style="font-size: 12px;">
						Please do not hesitate to call your servicing agent, <span id="agent-name"><c:out value="${ log.taxChange.get(0).agntName }"/></span> <span id="agent-phone"><c:out value="${ log.taxChange.get(0).agntPhone }"/></span> or our Contact Centre if you have any questions.
					</p>
		
						<br/><br/><span id="sign">With kind regards</span>
		
						<br/><br/>David Nutman
						<br/><span id="pos">Chief Executive Officer</span>
<%-- 					<c:if test="${ empty log || (log.approved == false && log.rejected == false)}"> --%>
<!-- 						<div class="row" style="position: fixed; bottom: 0px; left: 0px; background: white; width: 100%; padding-bottom: 40px; margin: 0px;"> -->
<!-- 							<div class="col-lg-6"> -->
<!-- 								<span><span class="text-danger"><input type="checkbox" name="agreed" id="agreed" /></span> <span id="tandc2">I have read and agreed to the</span> <a id="tandc" style="cursor: pointer;">terms and conditions</a>.</span> -->
<!-- 							</div> -->
<!-- 							<div class="col-lg-6"> -->
<%-- 								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  /> --%>
<!-- 								<button class="btn btn-danger pull-right" id="btnNext"> -->
<!-- 									Next -->
<!-- 								</button> -->
<!-- 							</div> -->
<!-- 						</div>		 -->
<%-- 					</c:if> --%>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  />
				</form>
				
			</div>
		</div>
	</div>
	<div class="row" style="margin: 0px; height: 100px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	var subject = '';
	var dear = '';
	var greeting = '';
	var choose_option = '';
	var option_1_intro = '';
	var option_1_intro_2 = '';
	var ul_1 = '';
	var ul_2 = '';
	var ul_3 = '';
	var ul_4 = '';
	var option_2_intro = '';
	var ol_1 = '';
	var ol_2 = '';
	var note = '';
	var sign = '';
	var pos = '';
	var tandc = '';
	var tandc2 = '';
	var data = [];
	var sa_before = '';
	var sa_after = '';
	var tandc_en = [];
	var tandc_kh = [];
	var contact = '';
	var agent_name = '';
	var agent_phone = '';
	var current_p = '';
	var new_p = '';
	var th_desc = '';
	var th_current = '';
	var th_extra = '';
	var desc_basic_life1 = '';
	var desc_pcb_life1 = '';
	var desc_fib_life1 = '';
	var desc_basic_life2 = '';
	var desc_pcb_life2 = '';
	var desc_fib_life2 = '';
	var p_installment = '';
	var freq = '';
	var tandc_title = '';
	
	$(document).ready(function(){
		sa_before = $('#sa-before').text();
		sa_after = $('#sa-after').text();
		agent_name = $('#agent-name').text();
		agent_phone = $('#agent-phone').text();
		current_p = $('#current-p').text();
		new_p = $('#new-p').text();
		freq = $('#freq').text();
		
		$('[data-toggle="popover"]').popover();
		
		$('#toggle-lang').bootstrapToggle({
		      on: 'En',
		      off: 'Kh',
		      onstyle: 'danger',
		      size: 'small'
	    });
		
		$('#toggle-lang').change(function() {
		      if($(this).is(':checked')){
		    	  $('#subject').html(subject);
		    	  $('#dear').html(dear);
		    	  $('#greeting').html(greeting);
		    	  $('#choose-option').html(choose_option);
		    	  $('#option-1-intro').html(option_1_intro);
		    	  $('#option-1-intro-2').html(option_1_intro_2);
		    	  $('#ul-1').html(ul_1);
		    	  $('#ul-2').html(ul_2);
		    	  $('#ul-3').html(ul_3);
		    	  $('#ul-4').html(ul_4);
		    	  $('#option-2-intro').html(option_2_intro);
		    	  $('#ol-1').html(ol_1);
		    	  $('#ol-2').html(ol_2);
		    	  $('#note').html(note);
		    	  $('#sign').html(sign);
		    	  $('#pos').html(pos);
		    	  $('#tandc').html(tandc);
		    	  $('#tandc2').html(tandc2);
		    	  $('#sa-before').html(sa_before);
		    	  $('#sa-after').html(sa_after);
		    	  $('#contact').html(contact);
		    	  $('#agent-name').html(agent_name);
		    	  $('#agent-phone').html(agent_phone);
		    	  $('#current-p').html(current_p);
		    	  $('#new-p').html(new_p);
		    	  $('#th-desc').html(th_desc);
		    	  $('#th-current').html(th_current);
		    	  $('#th-extra').html(th_extra);
		    	  $('#desc-basic-life1').html(desc_basic_life1);
		    	  $('#desc-pcb-life1').html(desc_pcb_life1);
		    	  $('#desc-fib-life1').html(desc_fib_life1);
		    	  $('#desc-basic-life2').html(desc_basic_life2);
		    	  $('#desc-pcb-life2').html(desc_pcb_life2);
		    	  $('#desc-fib-life2').html(desc_fib_life2);
		    	  $('#p-installment').html(p_installment);
		    	  $('#freq').html(freq);
		    	  $('#tandc-title').html(tandc_title);
		    	  
		    	  $('#tandc-list').html('');
		    	  for(var i=0; i<tandc_en.length; i++){
			  			$('#tandc-list').append('<li>' + tandc_en[i].desc + '</li>');
				  }
		      }else{
		    	  $.ajax({
		    		  type: 'post',
		    		  url: '<c:url value="/" />taxchange/lang/kh/taxchangeagreement',
		    		  data: {
						"${_csrf.parameterName}": "${_csrf.token}"
					  },
		    		  success: function(result){
		    		  	if(result){
		    		  		data = JSON.parse(result);
		    		  		
		    		  		subject = $('#subject').html();
		    		  		dear = $('#dear').html();
		    		  		greeting = $('#greeting').html();
		    		  		choose_option = $('#choose-option').html();
		    		  		option_1_intro = $('#option-1-intro').html();
		    		  		option_1_intro_2 = $('#option-1-intro-2').html();
		    		  		ul_1 = $('#ul-1').html();
		    		  		ul_2 = $('#ul-2').html();
		    		  		ul_3 = $('#ul-3').html();
		    		  		ul_4 = $('#ul-4').html();
		    		  		option_2_intro = $('#option-2-intro').html();
		    		  		ol_1 = $('#ol-1').html();
		    		  		ol_2 = $('#ol-2').html();
		    		  		note = $('#note').html();
		    		  		sign = $('#sign').html();
		    		  		pos = $('#pos').html();
		    		  		tandc = $('#tandc').html();
		    		  		tandc2 = $('#tandc2').html();
		    		  		contact = $('#contact').html();
		    		  		th_desc = $('#th-desc').html();
		    		  		th_current = $('#th-current').html();
		    		  		th_extra = $('#th-extra').html();
		    		  		desc_basic_life1 = $('#desc-basic-life1').html();
		    		  		desc_pcb_life1 = $('#desc-pcb-life1').html();
		    		  		desc_fib_life1 = $('#desc-fib-life1').html();
		    		  		desc_basic_life2 = $('#desc-basic-life2').html();
		    		  		desc_pcb_life2 = $('#desc-pcb-life2').html();
		    		  		desc_fib_life2 = $('#desc-fib-life2').html();
		    		  		p_installment = $('#p-installment').html();
		    		  		tandc_title = $('#tandc-title').html();
		    		  		replaceFreqKh();
		    		  		
		    		  		for(var i=0;i<data.length;i++){
		    		  			$('#' + data[i].tagId).html(data[i].desc);
		    		  		}
		    		  		
		    		  		$('#sa-before').html(sa_before);
		  		    	  	$('#sa-after').html(sa_after);
							$('#agent-name').html(agent_name);
							$('#agent-phone').html(agent_phone);
							$('#current-p').html(current_p);
							$('#new-p').html(new_p);
							
							$('#tandc-list').html('');
				    	    for(var i=0; i<tandc_kh.length; i++){
					  			$('#tandc-list').append('<li>' + tandc_kh[i].desc + '</li>');
						    }
		    		  	}
		    		  },
		    		  error: function(){
		    			  console.log('error get lang');
		    		  }
		    	  })
		      }
	    })
		
		$('#btnNext').on('click', function(e){
			e.preventDefault();
			
			if($('#toggle-lang').is(':checked')){
				showTandC.show('Terms and Conditions', 'en');	
			}else{
				showTandC.show('ខ និង លក្ខខណ្ឌ', 'kh');
			}
		});
		
		$('#btnAgreed').on('click', function(e){
			e.preventDefault();
			
			$('form')[0].submit();
		})
		
		$('#agreed').on('change', function(){
			if($(this).is(':checked')){
				$('#btnSubmit').removeAttr('disabled');
			}else{
				$('#btnSubmit').attr('disabled', 'disabled');
			}
		});
		
		$.ajax({
			type: 'post',
			url: '<c:url value="/" />taxchange/lang/en/taxchangeagreement/tandc-en',
			data: {
				"${_csrf.parameterName}": "${_csrf.token}"
		  	},
			success: function(result){
				tandc_en = JSON.parse(result);
			},
			error: function(){
				console.log('error retrieving terms and conditions list');
			}
		})
		
		$.ajax({
			type: 'post',
			url: '<c:url value="/" />taxchange/lang/kh/taxchangeagreement/tandc-kh',
			data: {
				"${_csrf.parameterName}": "${_csrf.token}"
		  	},
			success: function(result){
				tandc_kh = JSON.parse(result);
			},
			error: function(){
				console.log('error retrieving terms and conditions list');
			}
		})
		
// 		$('#toggle-lang').bootstrapToggle('off')
	});
	
	function replaceFreqKh(){
		if(freq === 'year'){
			$('#freq').html('ឆ្នាំ');
		}else if(freq === 'half-year'){
			$('#freq').html('ឆមាស');
		}else{
			$('#freq').html('ខែ');
		}
	}
</script>
</body>
</html>