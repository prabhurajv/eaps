<%@ include file="shared/header.jspf" %>
<style>	
	#login-style{
		background-image: -webkit-linear-gradient(90deg, #8C0000, #DE1818);
		box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.2) inset, 0px 0px 2px rgba(0, 0, 0, 0.5);
	}
	
	/* Tooltip */
  .test + .tooltip > .tooltip-inner {
      background-color: #C9302C;
      color: #FFFFFF;
      border: 1px solid #C9302C;
      padding: 5px;
      font-size: 12px;
  }
  /* Tooltip on top */
  .test + .tooltip.top > .tooltip-arrow {
      border-top: 5px solid green;
  }
  /* Tooltip on bottom */
  .test + .tooltip.bottom > .tooltip-arrow {
      border-bottom: 5px solid #C9302C;
  }
  /* Tooltip on left */
  .test + .tooltip.left > .tooltip-arrow {
      border-left: 5px solid red;
  }
  /* Tooltip on right */
  .test + .tooltip.right > .tooltip-arrow {
      border-right: 5px solid black;
  }
  
  .popover {
      background-color: #C9302C;
      color: #FFFFFF;
      border: 1px solid #C9302C;
      padding: 5px;
      font-size: 12px;
  }

  /* Tooltip on bottom */
  .popover.bottom > .arrow:after {
      border-bottom-color: #C9302C;
  }
</style>
<body>
	<%@ include file="shared/navbar.jspf" %>
	<div id="login-page">
	  	<div class="container" style="margin-top:60px;">
	  		  <fmt:setBundle basename="version" />  
		      <fmt:message var="version" key="version" /> 
<%-- 			  <center><img src="<c:url value="/resources/images/logo_prudential_notagline.jpg"/>" width=300 height=130/></center> --%>
			  <form:form class="form-login" name='changepwd' id="changepwd"  action="changepwd" method='POST' autocomplete="off" commandName="pwd">
			        <h2 class="form-login-heading" style="background:#fff2f3;">
<!-- 			        	Update Credential <i class="fa fa-info-circle test" data-toggle="tooltip" data-placement="bottom" title="Password must be at least 8 characters. Consist of at least one lowercase character, one uppercase character and one digit."></i> -->
						Update Credential <i class="fa fa-info-circle" data-toggle="popover" data-trigger="focus" tabindex="0" data-placement="bottom" data-content="Password must be at least 8 characters. Has never been used and consist of at least one lower case character, one upper case character, one digit and one of these signs !, @, #, $, %, ^, &, +, ="></i>
		        	</h2>
			      
			        <div class="login-wrap">
			        	<label class="checkbox" style="height: 20px">
			            	<span class="pull-left">
			            		<div class="text-danger">
			            			Password must be at least 8 characters. Has never been used and consist of at least one lower case character, one upper case character, one digit and one of these signs !, @, #, $, %, ^, &, +, =
			            		</div>
			            		<br />
			            	</span>
			            </label>
			            <br />
			        	<div class="row" style="margin: 0px;">
			        		<div class="col-lg-12">
			        			<form:password path='oldPwd' id='oldPwd' class="form-control" placeholder="Current Password"/>
			        		</div>
			        	</div>
			            
			            <br />
			            <div class="row" style="margin: 0px;">
			        		<div class="col-lg-12">
			        			<form:password path='newPwd' id='newPwd' class="form-control" placeholder="New Password" />
			        		</div>
			        	</div>
			            
			            <br />
			            <div class="row" style="margin: 0px;">
			        		<div class="col-lg-12">
			        			<form:password path='confirmPwd' class="form-control" placeholder="Confirm Password" />
			        		</div>
			        	</div>
			        	
			        	<div class="row" style="margin: 0px">
			        		<c:if test="${not empty error}">
				        		<br />
				        		<span class="text-danger">
				        			<c:out value="${error }" />
				        		</span>
			        		</c:if>
			        	</div>
			        	
			        	<br />
			            <button class="btn btn-danger btn-block" type="submit"><i class="fa fa-lock"></i> SUBMIT</button>
			            <br/>
			             
			            <p class="text-muted">� 2016 Prudential Cambodia All Rights Reserved. </p>
			         </div>
					 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		      </form:form>
	  	</div>
	  	 
	  </div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
</body>
</html>
<script type="text/javascript">
	$.validator.addMethod("pwcheck", function(value) {
	   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
	   	   && /[A-Z]/.test(value) // has a uppercase letter
	       && /[a-z]/.test(value) // has a lowercase letter
	       && /\d/.test(value) // has a digit
	});
	
	$.validator.addMethod("notEqualTo", function(value, element, param) {
		 return this.optional(element) || value != $(param).val();
		});
	
	$(document).ready(function(){
		$('#changepwd').validate({
			rules: {
				oldPwd: {
					required: true
				},
				newPwd: {
					required: true,
					minlength: 8,
					//pwcheck: true,
					notEqualTo: '#oldPwd',
					/*remote: {
						url: "<c:url value='/agent/checkUsedPwd' />",
						type: "post",
						data: {
							"${_csrf.parameterName}": "${_csrf.token}",
							newPwd: function() {
								return $('#newPwd').val();
							}
						}
					}*/
				},
				confirmPwd: {
					required: true,
					equalTo: '#newPwd'
				}
			},
			messages: {
				newPwd: {
					//pwcheck: 'New password is not strong enough',
					notEqualTo: 'Cannot be the same as current password',
					//remote: 'New password has already been used'
				},
				confirmPwd: {
					equalTo: 'Must be the same as new password'
				}
			},
			highlight: function(element) {
				$(element).parent().removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).parent().removeClass('has-error').addClass('has-success');
			}
		});
		
		$('input[name=oldPwd]').focus();
		
// 		$('[data-toggle="tooltip"]').tooltip();
		
		$('[data-toggle="popover"]').popover();
		
		$('.test').on('show.bs.tooltip', function() {
		    // Only one tooltip should ever be open at a time
		    $('.tooltip').not(this).hide();
		});
		
		StartWarningTimer();
	});
	
	$('#btnStayLoggedIn').click(function(){
		ResetTimeOutTimer();
	})
</script>