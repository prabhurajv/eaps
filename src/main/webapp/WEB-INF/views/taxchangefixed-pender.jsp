﻿<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
	
	@media(max-width: 768px){
		#title{
			font-size: 16px;
		}
	}
</style>
<body style="height: 100%;">
	<div class="panel panel-danger" style="margin: 0 20px; margin-top: 10px;">
		<div class="panel-heading" style="background-color: #EE1B2E; border-color: #d43f3a;">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px;">
				<div class="col-lg-12" style="padding: 0px;">
					<h3 class="pull-left" id="title" style="font-family: Kh-Metal-Chrieng; color: white; line-height: 0; margin-top: 15px;">
						Prudential Cambodia Life Assurance
					</h3>
					<input type="checkbox" checked data-toggle="toggle" data-size="small"
					 data-on="En" data-off="Kh" data-onstyle="danger" id="toggle-lang" />
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px; font-family: Kh-Metal-Chrieng;">
				<form method="post">
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.getSendDateFormated(\"dd/MM/yyyy\") }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;"><c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">Policy Number: <c:out value="${ log.chdrNum }" /></span><br/>
<%-- 					<span class="text-danger" style="font-size: 12px;">Date of Birth: <fmt:formatDate type="date" pattern="dd MMMM yyyy" value="${ log.taxChange.get(0).poDob }" /></span><br/> --%>
					<span class="text-danger" style="font-size: 12px;">FC/LIC: <c:out value="${ log.taxChange.get(0).agntName }" /></span><br/>
					<p style="font-size: 12px;">
						<br/><b id="subject">Subject: Change in Cambodia's Tax Law for Life Insurance Company</b>
						<br/><br/><span id="dear">Dear</span> <c:out value="${ log.taxChange.get(0).salutl.trim() }. " /> <c:out value="${ log.taxChange.get(0).poName }" />,<br/>
						<br/><span id="greeting">
							We would like to thank you for trusting and choosing Prudential Cambodia as your life insurance provider.
							<br/><br/>
							It gives us great pleasure to inform you that the tax on life insurance based on 5% of premium has been revised.  As a result of this change, your premium has been reduced from US$<span id="current-p"><c:out value="${ log.taxChange.get(0).installmentBeforeTaxchange }" /></span> to US$<span id="new-p"><c:out value="${ log.getTotalInstallmentAfterTaxChange() }" /></span> with effected from <span id="effective-date"><c:out value="${ log.effectiveDate }" /></span> with no change in your benefits.
						</span>
						<br/>
<!-- 						<div class="row" style="font-size: 12px;"> -->
<!-- 							<div class="col-lg-12"> -->
<!-- 								<a href="#" id="link-refund" data-toggle="collapse" data-target="#block-refund">Retain the protection coverage under your policy with a reduction in total policy premiums.</a> -->
<!-- 								<div id="block-refund" class="collapse in"> -->
<!-- 									Your current policy benefits are status quo while your renewal premium is adjusted to lower renewal premium by 5%. In this case, the benefits under your policy would not change even though you pay less renewal premium.  -->
<!-- 									In order for us do so, you will need to contact your bank to change the standing order for payment of premium on your policy to reflect the adjusted renewal premium amount (as shown in the table below). We request that you confirm our implementation and that you have your standing order changed with the adjusted amount at your earliest convenience. The reduced renewal premium will take effect only after we receive the revised standing order from you. -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
					</p>
		
<!-- 					<fieldset class="scheduler-border"> -->
<!-- 						<legend class="scheduler-border"> -->
<!-- 							<span id="choose-option" class="btn btn-sm btn-danger"> -->
<!-- 								The table below shows the adjusted renewal premium for your reference. -->
<!-- 							</span> -->
<!-- 						</legend> -->
<!-- 						<div class="row table-responsive"> -->
<!-- 							<table class="table"> -->
<!-- 								<thead> -->
<!-- 									<tr> -->
<!-- 										<th>Description</th> -->
<!-- 										<th style="word-wrap: break-word;"> -->
<!-- 											Current benefits and premium  -->
<!-- 											installment/year -->
<!-- 										</th> -->
<!-- 										<th style="word-wrap: break-word;"> -->
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${ !log.approved && !log.rejected }"> --%>
<!-- 													<input type="radio" name="rdoOption" id="rdoRefund" value="refund" checked /> -->
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<%-- 													<c:choose> --%>
<%-- 														<c:when test="${ not empty log.rejectedDate }"> --%>
<!-- 															<input type="radio" name="rdoOption" id="rdoRefund" value="refund"  -->
<!-- 																checked disabled /> -->
<%-- 														</c:when> --%>
<%-- 													</c:choose> --%>
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
<!-- 											Same benefits and premium  -->
<!-- 											installment/year -->
<!-- 										</th> -->
<!-- 									</tr> -->
<!-- 								</thead> -->
<!-- 								<tbody> -->
<!-- 									<tr> -->
<!-- 										<td colspan="3" style="border: none;"> -->
<%-- 											<c:out value="${ log.lifePFs.get(0).laName }" /> --%>
<!-- 										</td> -->
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<td style="padding-left: 20px; min-width: 250px;"> -->
<!-- 											Basic sum assured -->
<!-- 										</td> -->
<!-- 										<td style="max-width: 200px"> -->
<%-- 											<c:out value="${ log.lifePFs.get(0).basicSA }" /> --%>
<!-- 										</td> -->
<!-- 										<td style="max-width: 200px"> -->
<%-- 											<c:out value="${ log.lifePFs.get(0).basicSA }" /> --%>
<!-- 										</td> -->
<!-- 									</tr> -->
<%-- 									<c:if test="${ log.lifePFs.get(0).hasRtr1.trim().toLowerCase().equals('yes') }"> --%>
<!-- 										<tr> -->
<!-- 											<td style="padding-left: 20px; min-width: 250px;"> -->
<!-- 												Parent coverage benefit -->
<!-- 											</td> -->
<!-- 											<td style="max-width: 200px"> -->
<%-- 												<c:out value="${ log.lifePFs.get(0).pcb }" /> --%>
<!-- 											</td> -->
<!-- 											<td style="max-width: 200px"> -->
<%-- 												<c:out value="${ log.lifePFs.get(0).pcb }" /> --%>
<!-- 											</td> -->
<!-- 										</tr> -->
<%-- 									</c:if> --%>
<%-- 									<c:if test="${ log.lifePFs.get(0).hasRsr1.trim().toLowerCase().equals('yes') }"> --%>
<!-- 										<tr> -->
<!-- 											<td style="padding-left: 20px; min-width: 250px;"> -->
<!-- 												Family income benefit -->
<!-- 											</td> -->
<!-- 											<td style="max-width: 200px"> -->
<%-- 												<c:out value="${ log.lifePFs.get(0).fib }" /> --%>
<!-- 											</td> -->
<!-- 											<td style="max-width: 200px"> -->
<%-- 												<c:out value="${ log.lifePFs.get(0).fib }" /> --%>
<!-- 											</td> -->
<!-- 										</tr> -->
<%-- 									</c:if> --%>
<!-- 									<tr style="border: none;"> -->
<!-- 										<td colspan="3" style="border: none;"></td> -->
<!-- 									</tr> -->
<%-- 									<c:if test="${ log.lifePFs.size() > 1 }"> --%>
<!-- 										<tr> -->
<!-- 											<td colspan="3" style="border: none;"> -->
<%-- 												<c:out value="${ log.lifePFs.get(1).laName }" /> --%>
<!-- 											</td> -->
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<td style="padding-left: 20px; min-width: 250px;"> -->
<!-- 												Basic sum assured -->
<!-- 											</td> -->
<!-- 											<td style="max-width: 200px"> -->
<%-- 												<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 											</td> -->
<!-- 											<td style="max-width: 200px"> -->
<%-- 												<c:out value="${ log.lifePFs.get(1).basicSA }" /> --%>
<!-- 											</td> -->
<!-- 										</tr> -->
<%-- 										<c:if test="${ log.lifePFs.get(1).hasRtr1.trim().toLowerCase().equals('yes') }"> --%>
<!-- 											<tr> -->
<!-- 												<td style="padding-left: 20px; min-width: 250px;"> -->
<!-- 													Parent coverage benefit -->
<!-- 												</td> -->
<!-- 												<td style="max-width: 200px"> -->
<%-- 													<c:out value="${ log.lifePFs.get(1).pcb }" /> --%>
<!-- 												</td> -->
<!-- 												<td style="max-width: 200px"> -->
<%-- 													<c:out value="${ log.lifePFs.get(1).pcb }" /> --%>
<!-- 												</td> -->
<!-- 											</tr> -->
<%-- 										</c:if> --%>
<%-- 										<c:if test="${ log.lifePFs.get(1).hasRsr1.trim().toLowerCase().equals('yes') }"> --%>
<!-- 											<tr> -->
<!-- 												<td style="padding-left: 20px; min-width: 250px;"> -->
<!-- 													Family income benefit -->
<!-- 												</td> -->
<!-- 												<td style="max-width: 200px"> -->
<%-- 													<c:out value="${ log.lifePFs.get(1).fib }" /> --%>
<!-- 												</td> -->
<!-- 												<td style="max-width: 200px"> -->
<%-- 													<c:out value="${ log.lifePFs.get(1).fib }" /> --%>
<!-- 												</td> -->
<!-- 											</tr> -->
<%-- 										</c:if> --%>
										
<!-- 										<tr style="border: none;"> -->
<!-- 											<td colspan="3" style="border: none;"></td> -->
<!-- 										</tr> -->
<%-- 									</c:if> --%>
<!-- 									<tr> -->
<!-- 										<td style="border: none; min-width: 250px;"> -->
<!-- 											Premium Installment/year -->
<!-- 										</td> -->
<!-- 										<td style="border: none;"> -->
<%-- 											<c:out value="${ log.taxChange.get(0).apBeforeTaxChange }" /> --%>
<!-- 										</td> -->
<!-- 										<td style="border: none; color: red;"> -->
<%-- 											<c:out value="${ log.taxChange.get(0).apBeforeTax }" /> --%>
<!-- 										</td> -->
<!-- 									</tr> -->
<!-- 								</tbody> -->
<!-- 							</table> -->
<!-- 						</div> -->
<!-- 					</fieldset> -->
						
					<p id="detail" style="font-size: 12px">
						Please contact your bank at your earliest convenience to amend your current standing instruction to reflect the lower premium amount as shown above. This may involve completing and processing a new set of forms. Please then send a copy of the authorized standing instruction to us at Prudential (Cambodia) Life Assurance plc, Phnom Penh Tower, 20F, #445, Preah Monivong Blvd, Boeung Prolit, 7 Makara, Phnom Penh, Cambodia, P.O.Box: 417.
					</p>
					<br/>
					<p style="font-size: 12px">
						<span id="note" style="font-size: 12px;">
							Any over payments of premium will be refunded to this bank account after we receive the revised authorised standing instruction from you.
						</span>
						<br/>
						<span id="contact" style="font-size: 12px;">
							Please do not hesitate to call our Contact Centre or your servicing agent, <span id="agent-name"><c:out value="${ log.taxChange.get(0).agntName }"/></span> <span id="agent-phone"><c:out value="${ log.taxChange.get(0).agntPhone }"/></span> if you have any questions.
						</span>
		
						<br/><br/><span id="sign">With kind regards</span>
		
						<br/><br/>David Nutman
						<br/><span id="pos">Chief Executive Officer</span>
					</p>
<%-- 					<c:if test="${ empty log || (log.approved == false && log.rejected == false)}"> --%>
<!-- 						<div class="row" style="position: fixed; bottom: 0px; left: 0px; background: white; width: 100%; padding-bottom: 40px; margin: 0px;"> -->
<!--  							<div class="col-lg-6"> -->
<!--  								<span><span class="text-danger"><input type="checkbox" name="agreed" /></span> <span id="tandc2">I have read and agreed to the</span> <a id="tandc" style="cursor: pointer;">terms and conditions</a>.</span> -->
<!--  							</div> -->
<!-- 							<div class="col-lg-6"> -->
<%-- 								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  /> --%>
<!-- 								<button class="btn btn-danger pull-right" id="btnNext"> -->
<!-- 									Next -->
<!-- 								</button> -->
<!-- 							</div> -->
<!-- 						</div>		 -->
<%-- 					</c:if> --%>
				</form>
				
			</div>
		</div>
	</div>
	<div class="row" style="margin: 0px; height: 100px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	var subject = '';
	var dear = '';
	var greeting = '';
	var choose_option = '';
	var note = '';
	var sign = '';
	var pos = '';
	var tandc = '';
	var tandc2 = '';
	var data = [];
	var tandc_en = [];
	var tandc_kh = [];
	var contact = '';
	var agent_name = '';
	var agent_phone = '';
	var current_p = '';
	var new_p = '';
	var detail = '';
	var effective_date = '';
	
	$(document).ready(function(){
		agent_name = $('#agent-name').html();
		agent_phone = $('#agent-phone').html();
		current_p = $('#current-p').html();
		new_p = $('#new-p').html();
		effective_date = $('#effective-date').html();
		
		$('[data-toggle="popover"]').popover();
		
		$('#toggle-lang').bootstrapToggle({
		      on: 'En',
		      off: 'Kh',
		      onstyle: 'danger',
		      size: 'small'
	    });
		
		$('#toggle-lang').change(function() {
		      if($(this).is(':checked')){
		    	  $('#subject').html(subject);
		    	  $('#dear').html(dear);
		    	  $('#greeting').html(greeting);
		    	  $('#choose-option').html(choose_option);
				  $('#note').html(note);
		    	  $('#sign').html(sign);
		    	  $('#pos').html(pos);
		    	  $('#tandc2').html(tandc2);
		    	  $('#tandc').html(tandc);
		    	  $('#contact').html(contact);
		    	  $('#agent-name').html(agent_name);
  		  		  $('#agent-phone').html(agent_phone);
  		  		  $('#current-p').html(current_p);
  		  		  $('#new-p').html(new_p);
  		  		  $('#detail').html(detail);
  		  		  $('#effective-date').html(effective_date);
		      }else{
		    	  $.ajax({
		    		  type: 'post',
		    		  url: '<c:url value="/" />taxchange/lang/kh/taxchangefixed',
		    		  data: {
						"${_csrf.parameterName}": "${_csrf.token}"
					  },
		    		  success: function(result){
		    		  	if(result){
		    		  		data = JSON.parse(result);
		    		  		
		    		  		subject = $('#subject').html();
		    		  		dear = $('#dear').html();
		    		  		greeting = $('#greeting').html();
		    		  		choose_option = $('#choose-option').html();
		    		  		note = $('#note').html();
		    		  		sign = $('#sign').html();
		    		  		pos = $('#pos').html();
		    		  		tandc = $('#tandc').html();
		    		  		tandc2 = $('#tandc2').html();
		    		  		contact = $('#contact').html();
		    		  		detail = $('#detail').html();
		    		  		
		    		  		for(var i=0;i<data.length;i++){
		    		  			$('#' + data[i].tagId).html(data[i].desc);
		    		  		}
		    		  		
		    		  		$('#agent-name').html(agent_name);
		    		  		$('#agent-phone').html(agent_phone);
		    		  		$('#current-p').html(current_p);
		    		  		$('#new-p').html(new_p);
		    		  		$('#effective-date').html(effective_date);
		    		  	}
		    		  },
		    		  error: function(){
		    			  console.log('error get lang');
		    		  }
		    	  })
		      }
	    })
		
		$('#btnNext').on('click', function(e){
			e.preventDefault();
			
			if($('#toggle-lang').is(':checked')){
				showTandC.show('Terms and Conditions', 'en');	
			}else{
				showTandC.show('ខ និង លក្ខខណ្ឌ', 'kh');
			}
		});
		
		$('#agreed').on('change', function(){
			if($(this).is(':checked')){
				$('#btnSubmit').removeAttr('disabled');
			}else{
				$('#btnSubmit').attr('disabled', 'disabled');
			}
		});
		
		$.ajax({
			type: 'post',
			url: '<c:url value="/" />taxchange/lang/en/taxchangeagreement/tandc-en',
			data: {
				"${_csrf.parameterName}": "${_csrf.token}"
		  	},
			success: function(result){
				tandc_en = JSON.parse(result);
			},
			error: function(){
				console.log('error retrieving terms and conditions list');
			}
		})
		
		$.ajax({
			type: 'post',
			url: '<c:url value="/" />taxchange/lang/kh/taxchangeagreement/tandc-kh',
			data: {
				"${_csrf.parameterName}": "${_csrf.token}"
		  	},
			success: function(result){
				tandc_kh = JSON.parse(result);
			},
			error: function(){
				console.log('error retrieving terms and conditions list');
			}
		})
		
// 		$('#toggle-lang').bootstrapToggle('off');
	});
</script>
</body>
</html>