<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
	
	@media(max-width: 768px){
		#title{
			font-size: 16px;
		}
	}
</style>
<body style="height: 100%">
	<div class="panel panel-danger" style="margin: 0 20px; margin-top: 10px;">
		<div class="panel-heading" style="background-color: #EE1B2E; border-color: #d43f3a;">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px;">
				<div class="col-lg-12" style="padding: 0px;">
					<h3 class="pull-left" id="title" style="font-family: Kh-Metal-Chrieng; color: white; line-height: 0; margin-top: 15px;">
						Prudential Cambodia Life Assurance
					</h3>
					<input type="checkbox" checked data-toggle="toggle" data-size="small"
					 data-on="En" data-off="Kh" data-onstyle="danger" id="toggle-lang" />
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row" style="margin: 0px; padding: 0px; margin-left: 20px; margin-right: 20px; font-family: Kh-Metal-Chrieng;">
				<form method="post">
					<span class="text-danger" style="font-size: 12px;">DDMMYYYY</span><br/>
					<span class="text-danger" style="font-size: 12px;">Dear <c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">Policy Number: <c:out value="${ log.chdrNum }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">Date of Birth: <fmt:formatDate type="date" value="${ log.taxChange.get(0).poDob }" /></span><br/>
					<span class="text-danger" style="font-size: 12px;">LIS/LIC: <c:out value="${ log.taxChange.get(0).agntName }" /></span><br/>
					<p style="font-size: 12px;">
						<br/><b id="subject">Subject: Prudential Cambodia offers extra protection benefit to your policy in view of the recent change in Cambodia's Tax Law</b>
						<br/><br/><span id="dear">Dear</span> <c:out value="${ log.taxChange.get(0).salutl.trim() }. " /><c:out value="${ log.taxChange.get(0).poName }" />,
						<br/><br/><span id="greeting">We would like to thank you for trusting and choosing Prudential Cambodia as your life insurance provider.
						It is with pleasure we write to inform you that in observance of the Royal Government Cambodia's Prakas on 2017 Budget, the 5% tax that is based on your premium is removed from your next payment onward this year. In view of this, we are excited to inform you that we are able to offer you with one of the following two options:</span>
						<br/>
						<ul style="font-size: 12px;">
							<li>
								<a href="#" id="link-increase" data-toggle="collapse" data-target="#block-increase">Increased protection benefit coverage.</a>
								<div id="block-increase" class="collapse">
									With effect from your next payment of renewal premium on <fmt:formatDate type="date" value="${ log.taxChange.get(0).nextDueDate }" />, Prudential Cambodia offers you extra parental coverage benefits for your policy with the same gross amount of renewal premium and exempted tax amount. This means that while you pay the same amount as you had done in the past, you automatically obtain additional benefits (as shown in the table below) for your family without further proof of insurability. You will just need to confirm your acceptance by emailing to cs@prudential.com.kh, calling our Contact Center at 023 964 222 or 1800-212-223, print this copy and tick the box above with signed date and return it to us through your servicing agent (LIS/LIC), or clicking the buttons in the table below. Please note that Prudential Cambodia reserves the rights to discontinue our offer should you not confirm acceptance in two months’ time from the above date.
								</div>
							</li>
							<li>
								<a href="#" id="link-refund" data-toggle="collapse" data-target="#block-refund">Same protection benefit coverage with reduced renewal premium.</a>
								<div id="block-refund" class="collapse">
									You may choose your current policy benefit while your renewal premium is adjusted to lower renewal premium by 5%. In other words, your current benefits do not change even though you pay less renewal premium. This reduced renewal premium will take effect only after you have your current standing order changed with the adjusted renewal premium amount (as shown in the table below). In this case we request that you have your standing order changed with the adjusted amount at your earliest convenience.
								</div>
							</li>
						</ul>
					</p>
		
					<fieldset class="scheduler-border">
						<legend class="scheduler-border">
							<span id="choose-option" class="btn btn-sm btn-danger">
								The table below shows the summary of the two options of our offer.
							</span>
						</legend>
						<div class="row table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Description</th>
										<th style="word-wrap: break-word;">
											Current benefits and premium 
											installment/year
										</th>
										<th style="word-wrap: break-word;">
											<c:choose>
												<c:when test="${ !log.approved && !log.rejected }">
													<input type="radio" name="rdoOption" id="rdoIncrease" value="increase" checked />
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${ not empty log.approvedDate }">
															<input type="radio" name="rdoOption" id="rdoIncrease" value="increase" 
																checked disabled />
														</c:when>
														<c:otherwise>
															<input type="radio" name="rdoOption" id="rdoIncrease" value="increase"
															 	disabled />
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
											Extra benefits and premium 
											installment/year
										</th>
										<th style="word-wrap: break-word;">
											<c:choose>
												<c:when test="${ !log.approved && !log.rejected }">
													<input type="radio" name="rdoOption" id="rdoRefund" value="refund" />
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${ not empty log.rejectedDate }">
															<input type="radio" name="rdoOption" id="rdoRefund" value="refund" 
																checked disabled />
														</c:when>
														<c:otherwise>
															<input type="radio" name="rdoOption" id="rdoRefund" value="refund"
															 	disabled />
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
											Same benefits and premium 
											installment/year
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="3" style="border: none;">
											<c:out value="${ log.lifePFs.get(0).laName }" />
										</td>
									</tr>
									<tr>
										<td style="padding-left: 20px; min-width: 250px;">
											Basic sum assured
										</td>
										<td style="max-width: 200px">
											<c:out value="${ log.lifePFs.get(0).basicSA }" />
										</td>
										<td style="max-width: 200px">
											<c:out value="${ log.lifePFs.get(0).basicSA }" />
										</td>
										<td style="max-width: 200px">
											<c:out value="${ log.lifePFs.get(0).basicSA }" />
										</td>
									</tr>
									<c:if test="${ log.lifePFs.get(0).hasRtr1.trim().toLowerCase().equals('yes') }">
										<tr>
											<td style="padding-left: 20px; min-width: 250px;">
												Parent coverage benefit
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(0).pcb }" />
											</td>
											<td style="color: red; max-width: 200px;">
												<c:out value="${ log.taxChange.get(0).extraSA }" />
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(0).pcb }" />
											</td>
										</tr>
									</c:if>
									<c:if test="${ log.lifePFs.get(0).hasRsr1.trim().toLowerCase().equals('yes') }">
										<tr>
											<td style="padding-left: 20px; min-width: 250px;">
												Family income benefit
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(0).fib }" />
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(0).fib }" />
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(0).fib }" />
											</td>
										</tr>
									</c:if>
									<tr style="border: none;">
										<td colspan="3" style="border: none;"></td>
									</tr>
									<c:if test="${ log.lifePFs.size() > 1 }">
										<tr>
											<td colspan="3" style="border: none;">
												<c:out value="${ log.lifePFs.get(1).laName }" />
											</td>
										</tr>
										<tr>
											<td style="padding-left: 20px; min-width: 250px;">
												Basic sum assured
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(1).basicSA }" />
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(1).basicSA }" />
											</td>
											<td style="max-width: 200px">
												<c:out value="${ log.lifePFs.get(1).basicSA }" />
											</td>
										</tr>
										<c:if test="${ log.lifePFs.get(1).hasRtr1.trim().toLowerCase().equals('yes') }">
											<tr>
												<td style="padding-left: 20px; min-width: 250px;">
													Parent coverage benefit
												</td>
												<td style="max-width: 200px">
													<c:out value="${ log.lifePFs.get(1).pcb }" />
												</td>
												<td style="color: red; max-width: 200px;">
													<c:out value="${ log.taxChange.get(1).extraSA }" />
												</td>
												<td style="max-width: 200px">
													<c:out value="${ log.lifePFs.get(1).pcb }" />
												</td>
											</tr>
										</c:if>
										<c:if test="${ log.lifePFs.get(1).hasRsr1.trim().toLowerCase().equals('yes') }">
											<tr>
												<td style="padding-left: 20px; min-width: 250px;">
													Family income benefit
												</td>
												<td style="max-width: 200px">
													<c:out value="${ log.lifePFs.get(1).fib }" />
												</td>
												<td style="max-width: 200px">
													<c:out value="${ log.lifePFs.get(1).fib }" />
												</td>
												<td style="max-width: 200px">
													<c:out value="${ log.lifePFs.get(1).fib }" />
												</td>
											</tr>
										</c:if>
										
										<tr style="border: none;">
											<td colspan="3" style="border: none;"></td>
										</tr>
									</c:if>
									<tr>
										<td style="border: none; min-width: 250px;">
											Premium Installment/year
										</td>
										<td style="border: none;">
											<c:out value="${ log.taxChange.get(0).apBeforeTaxChange }" />
										</td>
										<td style="border: none;">
											<c:out value="${ log.taxChange.get(0).apBeforeTaxChange }" />
										</td>
										<td style="border: none; color: red;">
											<c:out value="${ log.taxChange.get(0).apBeforeTax }" />
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</fieldset>
					<p style="font-size: 12px">
						<span id="detail">Detailed and updated information of your policy will also be reflected in the next electronic Annual Policy Statement provided that you confirm your choice and that your standing order is changed by the next due date of your renewal premium payment if you choose reduced premium option. If you have changed your email address or phone number, please notify us so that you are kept informed of your policy.</span>
						<br/><br/><span id="contact">Should you need any clarification, please do not hesitate to consult our LIS/LIC, write to cs@prudential.com.kh or call our Contact Center at 023 964 222 or 1800 21 22 23 during working hours.</span>
		
						<br/><br/><span id="sign">Signed and sealed</span>
		
						<br/><br/>David Nutman
						<br/><br/><span id="pos">Chief Executive Officer</span>
					</p>
					<c:if test="${ empty log || (log.approved == false && log.rejected == false)}">
						<div class="row" style="position: fixed; bottom: 0px; left: 0px; background: white; width: 100%; padding-bottom: 40px; margin: 0px;">
<!-- 							<div class="col-lg-6"> -->
<!-- 								<span><span class="text-danger"><input type="checkbox" name="agreed" id="agreed" /></span> <span id="tandc2">I have read and agreed to the</span> <a id="tandc" style="cursor: pointer;">terms and conditions</a>.</span> -->
<!-- 							</div> -->
							<div class="col-lg-6">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  />
								<button class="btn btn-danger pull-right" id="btnNext">
									Next
								</button>
							</div>
						</div>		
					</c:if>
				</form>
				
			</div>
		</div>
	</div>
	<div class="row" style="margin: 0px; height: 100px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	var subject = '';
	var dear = '';
	var greeting = '';
	var link_increase = '';
	var block_increase = '';
	var link_refund = '';
	var block_refund = '';
	var choose_option = '';
	var detail = '';
	var contact = '';
	var sign = '';
	var pos = '';
	var tandc = '';
	var tandc2 = '';
	var data = [];
	
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
		
		$('#toggle-lang').bootstrapToggle({
		      on: 'En',
		      off: 'Kh',
		      onstyle: 'danger',
		      size: 'small'
	    });
		
		$('#toggle-lang').change(function() {
		      if($(this).is(':checked')){
		    	  $('#subject').html(subject);
		    	  $('#dear').html(dear);
		    	  $('#greeting').html(greeting);
		    	  $('#link-increase').html(link_increase);
		    	  $('#block-increase').html(block_increase);
		    	  $('#link-refund').html(link_refund);
		    	  $('#block-refund').html(block_refund);
		    	  $('#choose-option').html(choose_option);
		    	  $('#detail').html(detail);
		    	  $('#contact').html(contact);
		    	  $('#sign').html(sign);
		    	  $('#pos').html(pos);
		    	  $('#tandc').html(tandc);
		    	  $('#tandc2').html(tandc2);
		      }else{
		    	  $.ajax({
		    		  type: 'post',
		    		  url: '<c:url value="/" />enhancement/lang/kh/taxchangeagreement',
		    		  data: {
						"${_csrf.parameterName}": "${_csrf.token}"
					  },
		    		  success: function(result){
		    		  	if(result){
		    		  		data = JSON.parse(result);
		    		  		
		    		  		subject = $('#subject').html();
		    		  		dear = $('#dear').html();
		    		  		greeting = $('#greeting').html();
		    		  		link_increase = $('#link-increase').html();
		    		  		block_increase = $('#block-increase').html();
		    		  		link_refund = $('#link-refund').html();
		    		  		block_refund = $('#block-refund').html();
		    		  		choose_option = $('#choose-option').html();
		    		  		detail = $('#detail').html();
		    		  		contact = $('#contact').html();
		    		  		sign = $('#sign').html();
		    		  		pos = $('#pos').html();
		    		  		tandc = $('#tandc').html();
		    		  		tandc2 = $('#tandc2').html();
		    		  		
		    		  		for(var i=0;i<data.length;i++){
		    		  			$('#' + data[i].tagId).html(data[i].desc);
		    		  		}
		    		  	}
		    		  },
		    		  error: function(){
		    			  console.log('error get lang');
		    		  }
		    	  })
		      }
	    })
		
		$('#btnNext').on('click', function(e){
			e.preventDefault();
			
			if($('#toggle-lang').is(':checked')){
				showTandC.show('Terms and Conditions');	
			}else{
				alert('This will show Khmer T&C');
			}
		});
		
		$('#agreed').on('change', function(){
			if($(this).is(':checked')){
				$('#btnSubmit').removeAttr('disabled');
			}else{
				$('#btnSubmit').attr('disabled', 'disabled');
			}
		});
		
		$('#toggle-lang').bootstrapToggle('off')
	});
</script>
</body>
</html>