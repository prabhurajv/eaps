﻿<%@ include file="shared/header.jspf" %>
<body>
<div class="container" style="margin-top:10px;">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<img src="resources/images/eapsfront_ads.png" style="width: 100%" />
		</div>
		<div class="col-lg-1"></div>
	</div>
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row" style="margin: 0px; margin-top: 5px; background-color: #ED1B2E;">
				<div class="row" style="margin: 0px;">
<!-- 					<div class="col-lg-1"></div> -->
					<div class="col-lg-12" style="margin-top: 5px; background-color: #ED1B2E; color: white; font-size: 12px;">
						<form:form method="post" autocomplete="off" commandName="data" id="getpolicy">
							<div class="col-lg-4">
								<br />
								<label class="form-label" style="font-family: Kh-Metal-Chrieng">សូមបញ្ចូលថ្ងៃខែឆ្នាំកំណើត (ថ្ងៃខែឆ្នាំ) <br/> Date of Birth (ddmmyyyy)</label>
							</div>
							<div class="col-lg-5">
								<br />
								<form:input type="text" name="dob" path="dob" class="form-control" placeholder="Eg: 28072016" />
								<form:input type="hidden" name="arg1" path="encryptString" value="${arg1 }" />
								<label class="form-label text-danger" style="color: white">${error}</label>
							</div>
							<div class="col-lg-3">
								<br />
								<input type="submit" value="ទាញយក / Download" id="btnSubmit" class="btn btn-success pull-right" style="font-family: Kh-Metal-Chrieng" />
							</div>
						</form:form></div>
<!-- 					<div class="col-lg-1"></div> -->
				</div>
				<div class="row" style="margin: 0px; margin-top: 5px; margin-bottom: 5px;">
<!-- 					<div class="col-lg-1"></div> -->
					<div class="col-lg-12" style="margin-top:0px; background-color: #ED1B2E; color: white; font-size: 10px; padding-bottom: 10px">
						<form:form method="post" autocomplete="off" commandName="data">
							<div class="col-lg-12" style="font-family: Kh-Metal-Chrieng">
								<li>សូមវាយថ្ងៃខែឆ្នាំកំណើតរបស់លោកអ្នកតាមទម្រង់ DDMMYYYY ចូលក្នុងប្រអប់ខាងលើ រួចចុចលើពាក្យ ទាញយក។ លេខសម្ងាត់សម្រាប់បើកលិខិតជូនព័ត៌មានអំពីខួបនៃបណ្ណសន្យារ៉ាប់រងគឺ ថ្ងៃខែឆ្នាំកំណើតដដែល។</li>
								<br />
								<li>Please type your date of birth in the format of DDMMYYYY into the above given box and click Download. The password for openning the PDF file (annual policy statement) is the same date of birth.</li>
							</div>
						</form:form>
					</div>
<!-- 					<div class="col-lg-1"></div> -->
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
	
	
</div>

<%@ include file="shared/footer.jspf" %>
</body>
</html>

<script>
	$(document).ready(function(){
		$('#getpolicy').validate({
			rules: {
				dob: {
					required: true,
					number: true,
					minlength: 8
				}
			},
			messages: {
				dob: {
					number: 'Number only',
					minlength: 'At least 8 digits'
				}
			},
			highlight: function(element) {
				$(element).parent().removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).parent().removeClass('has-error').addClass('has-success');
			}
		});
	});
</script>