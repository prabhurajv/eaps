<%@ include file="shared/header.jspf" %>
<style>
	html{
		height: 100%;
	}
	
	.popover {
	    background-color: #C9302C;
	    color: #FFFFFF;
	    border: 1px solid #C9302C;
	    padding: 5px;
	    font-size: 12px;
	    max-width: 50%;
	}

	/* popover on bottom */
	.popover.bottom > .arrow:after {
	    border-bottom-color: #C9302C;
	}
</style>
<body style="height: 100%">
	<%@ include file="shared/navbar.jspf" %>
	<div class="row" style="margin: 0px; height: 60px"></div>
	<div class="container">
		<div class="row" style="margin: 0px;border:1px solid #ccc; margin-top: 10px;padding:10px;border-top-right-radius:10px;border-top-left-radius:10px;background:#EEEEEE;">
			<form method="post">
				<!-- <div class="col-lg-3" style="padding: 0px">
					<label class="form-label" for="criteria">Policy Num / Application Num</label>
				</div> -->
				<div class="col-lg-11">
					<input type="text" name="keyword" id="criteria" class="form-control" value="${e:forHtmlAttribute(keyword) }" placeholder="Enter keyword here" />
					<i class="fa fa-question-circle" style="float: right; margin-top: -23px; margin-right: 5px; font-size: 15px; color: #C9302C;" data-toggle="popover" data-trigger="focus" tabindex="0" data-placement="bottom" data-content="Keyword: Policy Num, Agent Num, Supervisor/UM/DBM Code, Client Num, App Num, Client Name, List today, List this day, List this month, List this year"></i>
				</div>
				<div class="col-lg-1">
					<input type="submit" value="Search" class="btn btn-danger" id="search" />
				</div>
				<div class="col-lg-4"></div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  />
			</form>
		</div>
		
		<div class="row" style="margin: 0px;margin-top:10px;border:1px solid #ccc; border-bottom-right-radius:10px;border-bottom-left-radius:10px;margin-bottom:10px;">
			<div style="width:100%;background:#EEEEEE;border-bottom:1px solid #ccc;">
				<div style="padding:10px">
					<span style="font-size: 16px; color: #D9534F; font-weight: 500;">Search Result <c:if test="${items.size() > 0}">(${items.size()})</c:if></span>
				</div>
			</div>
			<div style="padding:10px;">
			<table id="results" class="table table-striped table-bordered dt-responsive nowrap table-advance" cellspacing="0" style="width: 100%;font-size:12px;">
				<thead>
					<tr>
						<th>No</th>
						<th>Policy Num</th>
						<th>App Num</th>
						<th>Client Num</th>
						<th>Client Name</th>
						<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'SO'}">
							<th>UM/SUP Code</th>
							<th>UM/SUP Name</th>
							<th>Agent Num</th>
							<th>Agent Name</th>
						</c:if>
						<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'U0' }">
							<th>Agent Num</th>
							<th>Agent Name</th>
						</c:if>
						<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'SS'  }">
							<th>Agent Num</th>
							<th>Agent Name</th>
						</c:if>						
						<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'CS' }">
							<th>Client Dob</th>
						</c:if>
						<th>Year</th>
						<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'CS' }">
							<th>Short Url</th>
						</c:if>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${items}">
						<tr>
							<td><c:out value="${item[0]}" /></td>
							<td><c:out value="${item[10] }" /></td>
							<td><c:out value="${item[9] }" /></td>
							<td><c:out value="${item[7] }" /></td>
							<td><c:out value="${item[8] }" /></td>
							<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'SO'}">
								<td><c:out value="${item[3] }" /></td>
								<td><c:out value="${item[4] }" /></td>
								<td><c:out value="${item[5] }" /></td>
								<td><c:out value="${item[6] }" /></td>
							</c:if>
							<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'U0' }">
								<td><c:out value="${item[5] }" /></td>
								<td><c:out value="${item[6] }" /></td>
							</c:if>
							<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'SS' }">
								<td><c:out value="${item[5] }" /></td>
								<td><c:out value="${item[6] }" /></td>
							</c:if>
							<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'CS' }">
								<td>${fn:substring(item[16], 8, 10)}${fn:substring(item[16], 5, 7)}${fn:substring(item[16], 0, 4)}</td>
							</c:if>
							<td>${item[12] }</td>
							<c:if test="${fn:substring(pageContext.request.userPrincipal.name, 0, 2) == 'CS' }">
								<td><c:out value="${item[15] }" /></td>
							</c:if>
							<td style="text-align:center;">
								<form id="form${item[0]}" target="_blank">
									<p class="btn btn-danger btn-xs btn-outline btnDownload">&nbsp;&nbsp;PRINT&nbsp;&nbsp; </p>
									<input type="hidden" value="<c:out value="${item[14] }" />" name="hash" />
<%-- 									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"  /> --%>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
		</div>
	</div>
<div class="row" style="margin: 0px; height: 30px"></div>
<%@ include file="shared/footerinfo.jspf" %>
<%@ include file="shared/footer.jspf" %>
<script>
	$(document).ready(function(){
		$('#results').dataTable({
			"oLanguage": {
                "sSearch": "Filter : "
            },
			dom: "<'row'<'col-lg-12'B>>" + "<'row'<'col-lg-6'l><'col-lg-6'f>>" + "<'row'<'col-lg-12'tr>>" + "<'row'<'col-lg-6'i><'col-lg-6'p>>",
			buttons: [
				{
					extend: 'colvis',
					text: 'Show/Hide Columns'
				}
          	],
			"columnDefs": [
			    { 'targets': 0, 'width': '20px'}
			    ,{ 'targets': -1, 'width': '30px', 'className': 'btnDownload' }
			    ,{ 'targets': 1, 'width': '80px'}
             ],
             "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
		});
		
		$('#results').on('click', '.btnDownload', function(){
			var form = $(this).parent();
			var action_url = 'agent/get/' + $(this).next().val();
			form.attr('action', action_url);
			form.submit();
		});
		
		$('[data-toggle="popover"]').popover();
		
		StartWarningTimer();
	});
	
	$('#btnStayLoggedIn').click(function(){
		ResetTimeOutTimer();
	})
</script>
</body>
</html>