package eapsfront.handler;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import eapsfront.dao.ILogDao;
import eapsfront.dao.IUserDao;

public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private ILogDao logDao;
	
	@Autowired
	private IUserDao userDao;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		String username = auth.getName();
		String action = "login successfully";
		String ip = request.getRemoteHost();
		String pip = request.getRemoteHost();
		Date date = new Date();
		
		logDao.logUserAction(username, action, ip, pip, date);
		userDao.resetLoginAttemptAndLastLoginDate(username);
		
		getRedirectStrategy().sendRedirect(request, response, "/agent");
	}

}
