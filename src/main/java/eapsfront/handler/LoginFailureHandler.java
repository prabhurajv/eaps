package eapsfront.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import eapsfront.dao.IUserDao;
import eapsfront.model.User;

public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler implements AuthenticationFailureHandler {
	@Autowired
	private IUserDao userDao;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		saveException(request, exception);
		
		String username = request.getParameter("username");
		//setDefaultFailureUrl("/login");
		
		if(exception instanceof BadCredentialsException) {
			User user = userDao.getUser(username);
			if(user != null) {
				userDao.setLoginAttempt(user);
			}
		}
		
//		request.setAttribute("error", "Invalid username or password");
		getRedirectStrategy().sendRedirect(request, response, "/login?error=1");
	}

}
