package eapsfront.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.web.filter.CharacterEncodingFilter;

import eapsfront.handler.LoginFailureHandler;
import eapsfront.handler.LoginSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	private SessionRegistry sessionRegistry;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//add filter before csrf
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter,CsrfFilter.class);

		http.authorizeRequests()
			.antMatchers("/login**")
				.permitAll()
			.antMatchers("/agent**")
				.access("hasRole('ROLE_SaleSupport') or hasRole('ROLE_SYSTEMADMIN')")
			.antMatchers("/agent/**")
				.authenticated()
			.antMatchers("/agent/changepwd**")
				.authenticated()
			.antMatchers("/agent/changepwd/**")
				.authenticated()
			.antMatchers("/agent/get/**")
				.authenticated()
			.antMatchers("/forcelogout**")
				.authenticated()
			.antMatchers("/admin**")
				.access("hasRole('ROLE_SYSTEMADMIN')")
			.antMatchers("/admin/**")
				.access("hasRole('ROLE_SYSTEMADMIN')")
			.and()
			.exceptionHandling().accessDeniedPage("/403")
			.and()
			.formLogin()
				.loginPage("/login")
				.usernameParameter("username")
				.passwordParameter("pwd")
				.successHandler(loginSuccessHandler())
				.failureHandler(loginFailureHandler())
			.and()
			.csrf()
			.and()
			.exceptionHandling().accessDeniedPage("/403")
			.and()
			.logout()
				//.logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // use GET request
				.logoutSuccessUrl("/login?logout")
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.permitAll()
			.and()
			.sessionManagement().sessionFixation().migrateSession()
//				.invalidSessionUrl("/login?expired")
				.sessionAuthenticationErrorUrl("/login?expired")
				.maximumSessions(1)
//				.maxSessionsPreventsLogin(true)
				.sessionRegistry(sessionRegistry)
				.expiredUrl("/login?expired=1");
		
		http.headers()
			.httpStrictTransportSecurity()
			.and()
			.xssProtection()
			.xssProtectionEnabled(true)
			.and()
			.addHeaderWriter(new StaticHeadersWriter("HttpOnly", "true"));
	}
	
	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
	// Work around https://jira.spring.io/browse/SEC-2855
    @Bean(name = "sessionRegistry")
    public SessionRegistry sessionRegistry() {        
        return new SessionRegistryImpl();
    }
    
    @Bean
    public LoginFailureHandler loginFailureHandler() {
    	return new LoginFailureHandler();
    }
    
    @Bean
    public LoginSuccessHandler loginSuccessHandler() {
    	return new LoginSuccessHandler();
    }
	
	/* Equivalent to below xml code written in eapsfront-context.xml
	 *
	 	<http auto-config="true">
			<intercept-url pattern="/agent**" access="USER,ADMIN" />
		</http>

		<authentication-manager>
		  <authentication-provider>
		    <user-service>
				<user name="admin" password="12345" authorities="USER,ADMIN" />
				<user name="test" password="12345" authorities="USER" />
		    </user-service>
		  </authentication-provider>
		</authentication-manager>
	 *
	 */
}
