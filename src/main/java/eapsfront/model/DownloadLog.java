package eapsfront.model;

import java.util.Date;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "\"eaps\".download_logs")
public class DownloadLog {
	@Id
	@Column(name = "\"log_id\"")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auto_num_id") //Auto number primary key
	@SequenceGenerator(name="auto_num_id", sequenceName="\"eaps\".auto_num_id", allocationSize=1)
	private long Id;
	
	@Column(name = "\"chdr_num\"")
	private String chdrNum;
	
	@Column(name = "\"ann_year_th\"")
	private int annYearTh;
	
	@Column(name = "\"ip\"")
	private String ip;
	
	@Column(name = "\"username\"")
	private String userName;
	
	@Column(name = "\"download_date\"")
	private Date downloadDate;
	
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public int getAnnYearTh() {
		return annYearTh;
	}
	public void setAnnYearTh(int annYearTh) {
		this.annYearTh = annYearTh;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getDownloadDate() {
		return downloadDate;
	}
	public void setDownloadDate(Date downloadDate) {
		this.downloadDate = downloadDate;
	}
}
