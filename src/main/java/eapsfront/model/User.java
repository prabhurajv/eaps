package eapsfront.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "\"eaps\".sec_users")
public class User {
	@Id
	@Column(name = "\"username\"")
	private String username;
	
	@Column(name = "\"password\"")
	private String pwd;
	
	@Column(name = "\"user_id\"")
	private String userId;
	
	@Column(name = "\"full_name\"")
	private String fullName;
	
	@Column(name = "\"full_name_kh\"")
	private String fullNameKh;
	
	@Column(name = "\"gender\"")
	private String gender;
	
	@Column(name = "\"email\"")
	private String email;
	
	@Column(name = "\"account_expired_date\"")
	private Date accountExpiredDate;
	
	@Column(name = "\"pwd_expired_date\"")
	private Date pwdExpiredDate;
	
	@Column(name = "\"last_login_date\"")
	private Date lastLoginDate;
	
	@Column(name = "\"account_lock_date\"")
	private Date accountLockDate;
	
	@Column(name = "\"enabled\"")
	private boolean enabled;
	
	@Column(name = "\"first_login\"")
	private boolean isFirstLogin;
	
	@Column(name = "\"locked\"")
	private boolean isLocked;
	
	@Column(name = "\"attemp_no\"")
	private int attemptNo;
	
	@Column(name = "\"max_attemp_no\"")
	private int maxAttemptNo;
	
	@Column(name = "\"operator\"")
	private String operator;
	
	@Column(name = "\"syn_date\"")
	private Date synDate;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "\"eaps\".sec_user_roles",
				joinColumns = @JoinColumn(name = "username"),
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private List<Role> roles;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullNameKh() {
		return fullNameKh;
	}

	public void setFullNameKh(String fullNameKh) {
		this.fullNameKh = fullNameKh;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getAccountExpiredDate() {
		return accountExpiredDate;
	}

	public void setAccountExpiredDate(Date accountExpiredDate) {
		this.accountExpiredDate = accountExpiredDate;
	}

	public Date getPwdExpiredDate() {
		return pwdExpiredDate;
	}

	public void setPwdExpiredDate(Date pwdExpiredDate) {
		this.pwdExpiredDate = pwdExpiredDate;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Date getAccountLockDate() {
		return accountLockDate;
	}

	public void setAccountLockDate(Date accountLockDate) {
		this.accountLockDate = accountLockDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isFirstLogin() {
		return isFirstLogin;
	}

	public void setFirstLogin(boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public int getAttemptNo() {
		return attemptNo;
	}

	public void setAttemptNo(int attemptNo) {
		this.attemptNo = attemptNo;
	}

	public int getMaxAttemptNo() {
		return maxAttemptNo;
	}

	public void setMaxAttemptNo(int maxAttemptNo) {
		this.maxAttemptNo = maxAttemptNo;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getSynDate() {
		return synDate;
	}

	public void setSynDate(Date synDate) {
		this.synDate = synDate;
	}

	public List<Role> getRoles() {
		return roles;
	}
}
