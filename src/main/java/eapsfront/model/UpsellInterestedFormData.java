package eapsfront.model;

public class UpsellInterestedFormData {
	private String is_upsell_interested;

	public String getIs_upsell_interested() {
		return is_upsell_interested;
	}

	public void setIs_upsell_interested(String is_upsell_interested) {
		this.is_upsell_interested = is_upsell_interested;
	}
}
