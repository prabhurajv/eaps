package eapsfront.model;

import java.util.List;


public class ContractLf {
	private String agntNum;
	private String agntRegionCode;
	private String poNum;
	private String policyNum;
	private String applicationNum;
	private String agntSaleUnitCode;
	private String agntSaleBranchCode;
	private String agntSupervisor;
	private String clntName;
	private String policyStatCode;
	
	public String getAgntNum() {
		return agntNum;
	}

	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}

	public String getAgntRegionCode() {
		return agntRegionCode;
	}

	public void setAgntRegionCode(String agntRegionCode) {
		this.agntRegionCode = agntRegionCode;
	}

	public String getPoNum() {
		return poNum;
	}

	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}

	public String getPolicyNum() {
		return policyNum;
	}

	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}

	public String getApplicationNum() {
		return applicationNum;
	}

	public void setApplicationNum(String applicationNum) {
		this.applicationNum = applicationNum;
	}

	public String getAgntSaleUnitCode() {
		return agntSaleUnitCode;
	}

	public void setAgntSaleUnitCode(String agntSaleUnitCode) {
		this.agntSaleUnitCode = agntSaleUnitCode;
	}

	public String getAgntSaleBranchCode() {
		return agntSaleBranchCode;
	}

	public void setAgntSaleBranchCode(String agntSaleBranchCode) {
		this.agntSaleBranchCode = agntSaleBranchCode;
	}

	public String getAgntSupervisor() {
		return agntSupervisor;
	}

	public void setAgntSupervisor(String agntSupervisor) {
		this.agntSupervisor = agntSupervisor;
	}

	public String getClntName() {
		return clntName;
	}

	public void setClntName(String clntName) {
		this.clntName = clntName;
	}

	public String getPolicyStatCode() {
		return policyStatCode;
	}

	public void setPolicyStatCode(String policyStatCode) {
		this.policyStatCode = policyStatCode;
	}

	public List<MasterAps> getMasterApsList() {
		return masterApsList;
	}

	public void setMasterApsList(List<MasterAps> masterApsList) {
		this.masterApsList = masterApsList;
	}

	private List<MasterAps> masterApsList;
}
