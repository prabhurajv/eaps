package eapsfront.model;

import java.util.Date;

import javax.persistence.Column;

public class RptTaxChange {
	@Column(name="no")
	private long no;
	
	@Column(name="app_num")
	private String appNum;
	
	@Column(name="chdr_num")
	private String chdrNum;
	
	@Column(name="po_name")
	private String poName;
	
	@Column(name="po_dob")
	private Date poDob;
	
	@Column(name="sa_before_taxchange")
	private double saBeforeTaxChange;
	
	@Column(name="sa_after_taxchange")
	private double saAfterTaxChange;
	
	@Column(name="prem_installment_before_taxchange")
	private double premInstallmentBeforeTaxChange;
	
	@Column(name="prem_installment_after_taxchange")
	private double premInstallmentAfterTaxChange;
	
	public long getNo() {
		return no;
	}
	public void setNo(long no) {
		this.no = no;
	}
	public String getAppNum() {
		return appNum;
	}
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getPoName() {
		return poName;
	}
	public void setPoName(String poName) {
		this.poName = poName;
	}
	public Date getPoDob() {
		return poDob;
	}
	public void setPoDob(Date poDob) {
		this.poDob = poDob;
	}
	public double getSaBeforeTaxChange() {
		return Math.round(saBeforeTaxChange * 100.0) / 100.0;
	}
	public void setSaBeforeTaxChange(double saBeforeTaxChange) {
		this.saBeforeTaxChange = saBeforeTaxChange;
	}
	public double getSaAfterTaxChange() {
		return Math.round(saAfterTaxChange * 100.0) / 100.0;
	}
	public void setSaAfterTaxChange(double saAfterTaxChange) {
		this.saAfterTaxChange = saAfterTaxChange;
	}
	public double getPremInstallmentBeforeTaxChange() {
		return Math.round(premInstallmentBeforeTaxChange * 100.0) / 100.0;
	}
	public void setPremInstallmentBeforeTaxChange(double premInstallmentBeforeTaxChange) {
		this.premInstallmentBeforeTaxChange = premInstallmentBeforeTaxChange;
	}
	public double getPremInstallmentAfterTaxChange() {
		return Math.round(premInstallmentAfterTaxChange * 100.0) / 100.0;
	}
	public void setPremInstallmentAfterTaxChange(double premInstallmentAfterTaxChange) {
		this.premInstallmentAfterTaxChange = premInstallmentAfterTaxChange;
	}
}
