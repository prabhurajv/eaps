package eapsfront.model;

public class PremiumTaxChangeCustomerDecisionFormData {
	private String customer_decision;

	public String getCustomer_decision() {
		return customer_decision;
	}

	public void setCustomer_decision(String customer_decision) {
		this.customer_decision = customer_decision;
	}
}