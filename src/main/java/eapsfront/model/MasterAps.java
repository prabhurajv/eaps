package eapsfront.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "\"eaps\".tab_master_aps")
public class MasterAps implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1274371582475802478L;

	@Id
	@Column(name = "\"chdr_num\"")
	private String chdrNum;
	
	@Id
	@Column(name = "\"chdr_appnum\"")
	private String chdrAppNum;
	
	@Id
	@Column(name = "\"aps_year\"")
	private int apsYear;
	
	@Column(name = "\"ann_date\"")
	private Date annDate;
	
	@Column(name = "\"aps_year_th\"")
	private int apsYearTh;
	
	@Column(name = "\"aps_url\"")
	private String apsUrl;
	
	@Column(name = "\"param_hash\"")
	private String paramHash;
	
	@Column(name = "\"download_attempt\"")
	private int downloadAttempt;
	
	@Column(name = "\"download_attempt_max\"")
	private int downloadAttemptMax;
	
	@Column(name = "\"last_attempt_date\"")
	private Date lastAttemptDate;
	
	@Column(name = "\"short_url\"")
	private String shortUrl;
	
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getChdrAppNum() {
		return chdrAppNum;
	}
	public void setChdrAppNum(String chdrAppNum) {
		this.chdrAppNum = chdrAppNum;
	}
	public int getApsYear() {
		return apsYear;
	}
	public void setApsYear(int apsYear) {
		this.apsYear = apsYear;
	}
	public Date getAnnDate() {
		return annDate;
	}
	public void setAnnDate(Date annDate) {
		this.annDate = annDate;
	}
	public int getApsYearTh() {
		return apsYearTh;
	}
	public void setApsYearTh(int apsYearTh) {
		this.apsYearTh = apsYearTh;
	}
	public String getApsUrl() {
		return apsUrl;
	}
	public void setApsUrl(String apsUrl) {
		this.apsUrl = apsUrl;
	}
	public String getParamHash() {
		return paramHash;
	}
	public void setParamHash(String paramHash) {
		this.paramHash = paramHash;
	}
	public int getDownloadAttempt() {
		return downloadAttempt;
	}
	public void setDownloadAttempt(int downloadAttempt) {
		this.downloadAttempt = downloadAttempt;
	}
	public int getDownloadAttemptMax() {
		return downloadAttemptMax;
	}
	public void setDownloadAttemptMax(int downloadAttemptMax) {
		this.downloadAttemptMax = downloadAttemptMax;
	}
	public Date getLastAttemptDate() {
		return lastAttemptDate;
	}
	public void setLastAttemptDate(Date lastAttemptDate) {
		this.lastAttemptDate = lastAttemptDate;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
}
