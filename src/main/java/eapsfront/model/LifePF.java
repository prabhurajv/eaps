package eapsfront.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="\"eaps\".tab_taxchange_lifepf")
public class LifePF {

	@Id
	@Column(name="\"log_id\"")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auto_num_id") //Auto number primary key
	@SequenceGenerator(name="auto_num_id", sequenceName="\"eaps\".auto_num_id", allocationSize=1)
	private long logId;
	
	@Column(name="\"chdr_num\"")
	private String chdrNum;
	
	@Column(name="\"la_name\"")
	private String laName;
	
	@Column(name="\"clnt_num\"")
	private String clntNum;
	
	@Column(name="\"basic_sa\"")
	private double basicSA;
	
	@Column(name="\"pcb\"")
	private double pcb;
	
	@Column(name="\"fib\"")
	private double fib;
	
	@Column(name="\"has_rtr1\"")
	private String hasRtr1;
	
	@Column(name="\"has_rsr1\"")
	private String hasRsr1;
	
	@Column(name="\"chdr_life\"")
	private int chdrLife;

	public long getLogId() {
		return this.logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public String getChdrNum() {
		return this.chdrNum;
	}

	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	public String getLaName() {
		return this.laName;
	}

	public void setLaName(String laName) {
		this.laName = laName;
	}

	public String getClntNum() {
		return this.clntNum;
	}

	public void setClntNum(String clntNum) {
		this.clntNum = clntNum;
	}

	public double getBasicSA() {
		return this.basicSA;
	}

	public void setBasicSA(double basicSA) {
		this.basicSA = basicSA;
	}

	public double getPcb() {
		return this.pcb;
	}

	public void setPcb(double pcb) {
		this.pcb = pcb;
	}

	public double getFib() {
		return this.fib;
	}

	public void setFib(double fib) {
		this.fib = fib;
	}

	public String getHasRtr1() {
		return hasRtr1;
	}

	public void setHasRtr1(String hasRtr1) {
		this.hasRtr1 = hasRtr1;
	}

	public String getHasRsr1() {
		return hasRsr1;
	}

	public void setHasRsr1(String hasRsr1) {
		this.hasRsr1 = hasRsr1;
	}

	public int getChdrLife() {
		return chdrLife;
	}

	public void setChdrLife(int chdrLife) {
		this.chdrLife = chdrLife;
	}
}