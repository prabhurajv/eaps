package eapsfront.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"eaps\".tab_lang")
public class ViewLang {
	@Id
	@Column(name="\"id\"")
	private long id;
	
	@Column(name="\"lang\"")
	private String lang;
	
	@Column(name="\"tag_id\"")
	private String tagId;
	
	@Column(name="\"desc\"")
	private String desc;
	
	@Column(name="\"page\"")
	private String page;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLang() {
		return lang.trim();
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getTagId() {
		return tagId.trim();
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getDesc() {
		return desc.trim();
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPage() {
		return page.trim();
	}

	public void setPage(String page) {
		this.page = page;
	}
}
