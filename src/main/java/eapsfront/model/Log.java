package eapsfront.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "\"eaps\".sec_logs")
public class Log {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_logs_log_id_seq1") //Auto number primary key
	@SequenceGenerator(name="sec_logs_log_id_seq1", sequenceName="\"eaps\".sec_logs_log_id_seq1", allocationSize=1)
	@Column(name = "\"log_id\"")
	private int id;
	
	@Column(name = "\"username\"")
	private String username;
	
	@Column(name = "\"action\"")
	private String action;
	
	@Column(name = "\"local_ip\"")
	private String ip;
	
	@Column(name = "\"public_ip\"")
	private String pip;
	
	@Column(name = "\"log_date\"")
	private Date logDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPip() {
		return pip;
	}

	public void setPip(String pip) {
		this.pip = pip;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
}
