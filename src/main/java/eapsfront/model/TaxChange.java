package eapsfront.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="\"eaps\".prem_tax_chg_policy")
public class TaxChange implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"chdr_num\"")
	private String chdrNum;
	
	@Column(name="\"po_name\"")
	private String poName;
	
	@Column(name="\"po_dob\"")
	private Date poDob;
	
	@Column(name="\"salutl\"")
	private String salutl;
	
	@Column(name="\"agnt_num\"")
	private String agntNum;
	
	@Column(name="\"agnt_name\"")
	private String agntName;
	
	@Column(name="\"next_due_date\"")
	private Date nextDueDate;
	
	@Column(name="\"ap_before_tax\"")
	private double apBeforeTax;
	
	@Column(name="\"sa\"")
	private double sa;
	
	@Column(name="\"freq\"")
	private String freq;
	
	@Column(name="\"extra_sa\"")
	private double extraSA;
	
	@Column(name="\"option\"")
	private String option;
	
	@Column(name="\"reduction_ap\"")
	private String reductionAP;
	
	@Column(name="\"reduction_installment\"")
	private double reductionInstallment;
	
	@Column(name="\"ap_before_taxchange\"")
	private double apBeforeTaxChange;
	
	@Id
	@Column(name="\"chdr_life\"")
	private String chdrLife;
	
	@Id
	@Column(name="\"life_cnum\"")
	private String lifeCNum;
	
	@Column(name="\"agnt_phone\"")
	private String agntPhone;
	
	@Column(name="\"installment_before_taxchange\"")
	private double installmentBeforeTaxchange;
	
	@Column(name="\"case_type\"")
	private String caseType;
	
	@Column(name="\"pro_rate_refund\"")
	private double proRateRefund;

	public String getChdrNum() {
		return this.chdrNum;
	}

	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	public String getPoName() {
		return this.poName;
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	public Date getPoDob() {
		return this.poDob;
	}

	public void setPoDob(Date poDob) {
		this.poDob = poDob;
	}

	public String getAgntNum() {
		return this.agntNum;
	}

	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}

	public String getAgntName() {
		return this.agntName;
	}

	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}

	public Date getNextDueDate() {
		return this.nextDueDate;
	}
	public String getNextDueDateFormated(String format){
		String formatedNextDueDate = new SimpleDateFormat(format).format(nextDueDate);
		
		return formatedNextDueDate;
	}

	public void setNextDueDate(Date nextDueDate) {
		this.nextDueDate = nextDueDate;
	}

	public double getApBeforeTax() {
		return this.apBeforeTax;
	}

	public void setApBeforeTax(double apBeforeTax) {
		this.apBeforeTax = apBeforeTax;
	}

	public double getSa() {
		return this.sa;
	}

	public void setSa(double sa) {
		this.sa = sa;
	}

	public String getFreq() {
		if(this.freq != null && this.freq != ""){
			return Integer.parseInt(this.freq.trim()) + "";
		}
		
		return "";
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public double getExtraSA() {
		return this.extraSA;
	}

	public void setExtraSA(double extraSA) {
		this.extraSA = extraSA;
	}

	public String getOption() {
		return this.option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getReductionAP() {
		return this.reductionAP;
	}

	public void setReductionAP(String reductionAP) {
		this.reductionAP = reductionAP;
	}

	public double getReductionInstallment() {
		return this.reductionInstallment;
	}

	public void setReductionInstallment(double reductionInstallment) {
		this.reductionInstallment = reductionInstallment;
	}

	public double getApBeforeTaxChange() {
		return apBeforeTaxChange;
	}

	public void setApBeforeTaxChange(double apBeforeTaxChange) {
		this.apBeforeTaxChange = apBeforeTaxChange;
	}

	public String getSalutl() {
		return salutl;
	}

	public void setSalutl(String salutl) {
		this.salutl = salutl;
	}

	public String getChdrLife() {
		return chdrLife;
	}

	public void setChdrLife(String chdrLife) {
		this.chdrLife = chdrLife;
	}

	public String getLifeCNum() {
		return lifeCNum;
	}

	public void setLifeCNum(String lifeCNum) {
		this.lifeCNum = lifeCNum;
	}

	public String getAgntPhone() {
		return agntPhone;
	}

	public void setAgntPhone(String agntPhone) {
		this.agntPhone = agntPhone;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public double getInstallmentBeforeTaxchange() {
		return installmentBeforeTaxchange;
	}

	public void setInstallmentBeforeTaxchange(double installmentBeforeTaxchange) {
		this.installmentBeforeTaxchange = installmentBeforeTaxchange;
	}

	public double getProRateRefund() {
		return proRateRefund;
	}

	public void setProRateRefund(double proRateRefund) {
		this.proRateRefund = proRateRefund;
	}
}