package eapsfront.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

public class PremiumTaxChangePolicy {
	private String policy_num;
	private String payment_method;
	private String protection_upgr_eligible;
	private String customer_decision;
	private boolean isAnonymous;
	private Date po_dob;
	private int id;

	public String getPolicy_num() {
		return policy_num;
	}

	public void setPolicy_num(String policy_num) {
		this.policy_num = policy_num;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public String getProtection_upgr_eligible() {
		return protection_upgr_eligible;
	}

	public void setProtection_upgr_eligible(String protection_upgr_eligible) {
		this.protection_upgr_eligible = protection_upgr_eligible;
	}

	public String getCustomer_decision() {
		return customer_decision;
	}

	public void setCustomer_decision(String customer_decision) {
		this.customer_decision = customer_decision;
	}

	public boolean isAnonymous() {
		return isAnonymous;
	}

	public void setAnonymous(boolean isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public Date getPo_dob() {
		return po_dob;
	}

	public void setPo_dob(Date po_dob) {
		this.po_dob = po_dob;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}