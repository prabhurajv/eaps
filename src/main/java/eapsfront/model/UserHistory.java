package eapsfront.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "\"eaps\".sec_user_history")
public class UserHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_user_history_user_history_id_seq") //Auto number primary key
	@SequenceGenerator(name="sec_user_history_user_history_id_seq", sequenceName="\"eaps\".sec_user_history_user_history_id_seq", allocationSize=1)
	@Column(name = "\"user_history_id\"")
	private int id;
	
	@Column(name = "\"username\"")
	private String username;
	
	@Column(name = "\"password\"")
	private String pwd;
	
	@Column(name = "\"description\"")
	private String action;
	
	@Column(name = "\"operator\"")
	private String operator;
	
	@Column(name = "\"syn_date\"")
	private Date date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
