package eapsfront.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "\"eaps\".tab_taxchange_visit_logs")
public class TaxChangeVisitLog {
	@Id
	@Column(name = "\"log_id\"")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auto_num_id") //Auto number primary key
	@SequenceGenerator(name="auto_num_id", sequenceName="\"eaps\".auto_num_id", allocationSize=1)
	private long logId;
	
	@Column(name = "\"browser\"")
	private String browser;
	
	@Column(name = "\"ip\"")
	private String ip;
	
	@Column(name = "\"link\"")
	private String link;
	
	@Column(name = "\"visit_date\"")
	private Date visitDate;

	public long getLogId() {
		return logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Date getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
}
