package eapsfront.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "\"eaps\".tab_taxchange_logs")
public class TaxChangeLog {
	@Id
	@Column(name = "\"chdr_num\"")
	private String chdrNum;
	
	@Column(name = "\"send_date\"")
	private Date sendDate;
	
	@Column(name = "\"approved\"")
	private boolean approved;
	
	@Column(name = "\"approved_date\"")
	private Date approvedDate;
	
	@Column(name = "\"rejected\"")
	private boolean rejected;
	
	@Column(name = "\"rejected_date\"")
	private Date rejectedDate;
	
	@Column(name = "\"link\"")
	private String link;
	
	@Column(name = "\"ip\"")
	private String ip;
	
	@Column(name = "\"trn_date\"")
	private Date trnDate;
	
	@Column(name = "\"last_trn_date\"")
	private Date lastTrnDate;
	
	@Transient
	private String effectiveDate;
	
	@Transient
	private ArrayList<TaxChange> taxChange;
	
	@Transient
	private ArrayList<LifePF> lifePFs;
	
	@Transient
	private boolean isAnonymous;
	
	@Transient
	private String isSubmitted;

	public String getChdrNum() {
		return chdrNum;
	}

	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	public Date getSendDate() {
		return sendDate;
	}
	public String getSendDateFormated(String format){
		String formatedSendDate = new SimpleDateFormat(format).format(sendDate);
		
		return formatedSendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public Date getRejectedDate() {
		return rejectedDate;
	}

	public void setRejectedDate(Date rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getTrnDate() {
		return trnDate;
	}

	public void setTrnDate(Date trnDate) {
		this.trnDate = trnDate;
	}

	public Date getLastTrnDate() {
		return lastTrnDate;
	}

	public void setLastTrnDate(Date lastTrnDate) {
		this.lastTrnDate = lastTrnDate;
	}

	public ArrayList<TaxChange> getTaxChange() {
		return taxChange;
	}
	
	public void setTaxChange(ArrayList<TaxChange> taxChange){
		this.taxChange = taxChange;
	}

	public ArrayList<LifePF> getLifePFs() {
		return lifePFs;
	}

	public void setLifePFs(ArrayList<LifePF> lifePFs) {
		this.lifePFs = lifePFs;
	}
	
	public double getTotalSABeforeTaxChange(){
		double totalSA = 0;
		
		for(TaxChange taxchange : taxChange){
			totalSA = totalSA + taxchange.getSa();
		}
		
//		for(LifePF lifepf : lifePFs){
//			totalSA = totalSA + lifepf.getPcb() + lifepf.getFib();
//		}
		
		return totalSA;
	}
	
	public double getTotalSAAfterTaxChange(){
		double totalSA = 0;
		
		for(TaxChange taxchange : taxChange){
			totalSA = totalSA + taxchange.getSa() + taxchange.getExtraSA();
		}
		
//		for(LifePF lifepf : lifePFs){
//			totalSA = totalSA + lifepf.getPcb() + lifepf.getFib();
//		}
		
		return totalSA;
	}
	
	public double getTotalReductionInstallment(){
		double totalReductionInstallment = 0;
		
		for(TaxChange taxchange : taxChange){
			totalReductionInstallment = totalReductionInstallment + taxchange.getReductionInstallment();
		}
		
		return totalReductionInstallment;
	}
	
	public double getTotalInstallmentAfterTaxChange(){
		double totalInstallment = 0;
		
		totalInstallment = taxChange.get(0).getInstallmentBeforeTaxchange() - getTotalReductionInstallment();
		
		return Math.round(totalInstallment * 100.0) / 100.0;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isAnonymous() {
		return isAnonymous;
	}

	public void setAnonymous(boolean isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public String getIsSubmitted() {
		return isSubmitted;
	}

	public void setIsSubmitted(String isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
}
