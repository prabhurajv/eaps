package eapsfront.model;

import javax.persistence.Column;
import java.util.Date;

public class RptMasterAps {
	@Column(name = "\"no\"")
	private int no;
	
	@Column(name = "\"dbm_code\"")
	private String dbmCode;
	
	@Column(name = "\"dbm_name\"")
	private String dbmName;
	
	@Column(name = "\"um_sup_code\"")
	private String umSupCode;
	
	@Column(name = "\"um_sup_name\"")
	private String umSupName;
	
	@Column(name = "\"agnt_num\"")
	private String agntNum;
	
	@Column(name = "\"agnt_name\"")
	private String agntName;
	
	@Column(name = "\"po_num\"")
	private String poNum;
	
	@Column(name = "\"clnt_name\"")
	private String clntName;
	
	@Column(name = "\"chdr_appnum\"")
	private String appNum;
	
	@Column(name = "\"chdr_num")
	private String chdrNum;
	
	@Column(name = "\"aps_date\"")
	private Date apsDate;
	
	@Column(name = "\"aps_year\"")
	private String apsYear;
	
	@Column(name = "\"aps_url\"")
	private String apsUrl;
	
	@Column(name = "\"param_hash\"")
	private String paramHash;
	
	@Column(name = "\"short_url\"")
	private String shortUrl;
	
	@Column(name = "\"clnt_dob\"")
	private String clntDob;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getDbmCode() {
		return dbmCode;
	}
	public void setDbmCode(String dbmCode) {
		this.dbmCode = dbmCode;
	}
	public String getDbmName() {
		return dbmName;
	}
	public void setDbmName(String dbmName) {
		this.dbmName = dbmName;
	}
	public String getUmSupCode() {
		return umSupCode;
	}
	public void setUmSupCode(String umSupCode) {
		this.umSupCode = umSupCode;
	}
	public String getUmSupName() {
		return umSupName;
	}
	public void setUmSupName(String umSupName) {
		this.umSupName = umSupName;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	public String getPoNum() {
		return poNum;
	}
	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}
	public String getClntName() {
		return clntName;
	}
	public void setClntName(String clntName) {
		this.clntName = clntName;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getAppNum() {
		return appNum;
	}
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
	public Date getApsDate() {
		return apsDate;
	}
	public void setApsDate(Date apsDate) {
		this.apsDate = apsDate;
	}
	public String getApsYear() {
		return apsYear;
	}
	public void setApsYear(String apsYear) {
		this.apsYear = apsYear;
	}
	public String getApsUrl() {
		return apsUrl;
	}
	public void setApsUrl(String apsUrl) {
		this.apsUrl = apsUrl;
	}
	public String getParamHash() {
		return paramHash;
	}
	public void setParamHash(String paramHash) {
		this.paramHash = paramHash;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	
	public String getClntDob() {
		return clntDob;
	}
	public void setClntDob(String clntDob) {
		this.clntDob = clntDob;
	}
}
