package eapsfront.gluster;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class GlusterFs {
	private static final String AUTH_PWD_HEADER = "X-Storage-Pass";
	private static final String AUTH_STORAGE_URL = "X-Storage-Url";
	private static final String AUTH_TOKEN_HEADER = "X-Auth-Token";
	//private static final String AUTH_URL = "/auth/v1.0";
	private static final String AUTH_URL = System.getProperty("auth_url");
	private static final String AUTH_USERNAME_HEADER = "X-Storage-User";
	private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
	
	private static final int FILENAME_LENGTH = 26;
	
	static final Log log = LogFactory.getLog(GlusterFs.class);

	private String authToken = null;
	//private String glusterHost = "vkhlifeluvnode.pru.intranet.asia";
	private String glusterHost = System.getProperty("auth_host");
	private String scheme = "https";
	private String storageURL = null;
	

	public void authenticate(String username, String pwd)
			throws GlusterAuthnFailedException {

		HttpResponse<String> response;
		try {
			String url = scheme + "://" + glusterHost + AUTH_URL;
			System.out.println(url);
			response = Unirest.get(scheme + "://" + glusterHost + AUTH_URL)
					.header(AUTH_USERNAME_HEADER, username)
					.header(AUTH_PWD_HEADER, pwd)
					.header("cache-control", "no-cache").asString();
			int status = response.getStatus();
			if (status == 200) {
				authToken = response.getHeaders().get(AUTH_TOKEN_HEADER).get(0);
				storageURL = response.getHeaders().get(AUTH_STORAGE_URL).get(0);
				log.debug("Authentication Successfull to storage URL["
						+ storageURL + "]");
			} else {
				throw new GlusterAuthnFailedException(
						"Authentication Failed with Status Code" + status);
			}

		} catch (UnirestException e) {
			e.printStackTrace();
			throw new GlusterAuthnFailedException(e);
		}

	}

	private void checkAuthStatus() throws IllegalStateException {
		if (!isAuthenticated())
			throw new IllegalStateException("Not Authenticated");
	}

	public void createContainer(String container)
			throws GlusterRequestFailedException {
		checkAuthStatus();

		HttpResponse<String> response;
		try {
			response = Unirest.put(storageURL + "/" + container + "/")
					.header(AUTH_TOKEN_HEADER, authToken)
					.header("cache-control", "no-cache").asString();
			int status = response.getStatus();
			if (isResponseSuccess(status)) {

				log.debug("Container created Successfull in storage URL["
						+ storageURL + "]");

			} else {
				if (status == 404) {
					throw new GlusterRequestFailedException(
							"Parent Container Not found create parent container first?");
				}
				throw new GlusterRequestFailedException(
						"Request Failed with Status Code " + status);
			}

		} catch (UnirestException e) {
			throw new GlusterRequestFailedException(e);
		}

	}

	public void createObjectWithSpecificName(String container, String targetFileName,
			String localFilePath, String mimeType)
			throws GlusterRequestFailedException {
		checkAuthStatus();

		HttpResponse<String> response;
		try {

			
			File file = new File(localFilePath);
			
			byte[] bFile = new byte[(int) file.length()];

			// convert file into array of bytes
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

			response = Unirest
					.put(storageURL + "/" + container + "/" + targetFileName)
					.header(AUTH_TOKEN_HEADER, authToken)
					.header("cache-control", "no-cache")
					.header(HTTP_HEADER_CONTENT_TYPE, mimeType).body(bFile)

					.asString();
			int status = response.getStatus();
			if (isResponseSuccess(status)) {

				log.debug("File Upload Successfull in storage URL["
						+ storageURL + "]");

			} else {
				if (status == 404) {
					throw new GlusterRequestFailedException("Container "
							+ container + " Not found in  create first?");
				}
				throw new GlusterRequestFailedException(
						"Request Failed with Status Code " + status);
			}

		} catch (UnirestException e) {
			throw new GlusterRequestFailedException(e);
		} catch (FileNotFoundException e) {
			throw new GlusterRequestFailedException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new GlusterRequestFailedException(e);
		}

	}
	
	
	public String createObject(String container,
			String localFilePath, String mimeType)
			throws GlusterRequestFailedException {
		checkAuthStatus();

		HttpResponse<String> response;
		String targetFileName=null;
		try {

			File file = new File(localFilePath);
			
			byte[] bFile = new byte[(int) file.length()];

			// convert file into array of bytes
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();
			targetFileName=generateRandomFileName();
			response = Unirest
					.put(storageURL + "/" + container + "/" + targetFileName)
					.header(AUTH_TOKEN_HEADER, authToken)
					.header("cache-control", "no-cache")
					.header(HTTP_HEADER_CONTENT_TYPE, mimeType).body(bFile)

					.asString();
			int status = response.getStatus();
			if (isResponseSuccess(status)) {

				log.debug("File Upload Successfull in storage URL["
						+ storageURL + "]");

			} else {
				if (status == 404) {
					throw new GlusterRequestFailedException("Container "
							+ container + " Not found in  create first?");
				}
				throw new GlusterRequestFailedException(
						"Request Failed with Status Code " + status);
			}

		} catch (UnirestException e) {
			throw new GlusterRequestFailedException(e);
		} catch (FileNotFoundException e) {
			throw new GlusterRequestFailedException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new GlusterRequestFailedException(e);
		}
		return storageURL + "/" + container + "/" + targetFileName;

	}

	public void getFileToLocal(String container, String targetFileName,
			String localFilePath) throws GlusterRequestFailedException {
		checkAuthStatus();

		@SuppressWarnings("rawtypes")
		HttpResponse response;
		try {
			response = Unirest
					.get(storageURL + "/" + container + "/" + targetFileName)
					.header(AUTH_TOKEN_HEADER, authToken)
					.header("cache-control", "no-cache").asBinary();

			FileOutputStream outputStream = null;
			try {
				outputStream = new FileOutputStream(localFilePath);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			InputStream inputStream = response.getRawBody();

			int read = 0;
			byte[] bytes = new byte[1024];

			try {
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
			} catch (IOException e) {
				throw new GlusterRequestFailedException(e);
			}

			int status = response.getStatus();
			if (isResponseSuccess(status)) {
				log.debug("File Get Successfull in storage URL["
						+ localFilePath + "]");

			} else {
				if (status == 404) {
					throw new GlusterRequestFailedException("File Not found");
				}
				throw new GlusterRequestFailedException(
						"Request Failed with Status Code " + status);
			}

		} catch (UnirestException e) {
			throw new GlusterRequestFailedException(e);
		}

	}
	
	public void getFileToLocal(String url, HttpServletResponse mainStream, boolean isForPrint) {
		String localFilePath = "C:/Users/326625/Desktop";

		@SuppressWarnings("rawtypes")
		HttpResponse response;
		try {
			String tempurl = storageURL + "/" + url;
			System.out.println(tempurl);
			response = Unirest
					.get(storageURL + "/" + url)
					.header(AUTH_TOKEN_HEADER, authToken)
					.header("cache-control", "no-cache").asBinary();
			
			OutputStream outputStream = mainStream.getOutputStream();			

			InputStream inputStream = response.getRawBody();
			
			mainStream.setContentType("application/pdf");
			
			if(isForPrint == false) {
				mainStream.setHeader("Content-Disposition", "attachment");
			}
			
			mainStream.setContentLength(inputStream.available());

			int read = 0;
			byte[] bytes = new byte[1024];

			try {
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new GlusterRequestFailedException(e);
			}

			int status = response.getStatus();
			if (isResponseSuccess(status)) {
				log.debug("File Get Successfull in storage URL["
						+ localFilePath + "]");
			} else {
				if (status == 404) {
					throw new GlusterRequestFailedException("File Not found");
				}
				throw new GlusterRequestFailedException(
						"Request Failed with Status Code " + status);
			}
			
			outputStream.flush();
			outputStream.close();
			inputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
//			throw new GlusterRequestFailedException(e);
		}
	}
	
	public String getStorageURL(){
		return storageURL;
	}
	
	public String getAuthToken(){
		return authToken;
	}

	public String getGlusterHost() {
		return glusterHost;
	}

	public String getScheme() {
		return scheme;
	}

	private boolean isAuthenticated() {
		return authToken != null && storageURL != null;
	}

	private boolean isResponseSuccess(int status) {
		return status == 200 || status == 201 || status == 202 || status == 204;
	}

	public void setGlusterHost(String glusterHost) {
		this.glusterHost = glusterHost;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	
	protected String generateRandomFileName()
	{
		String alphabet= "abcdefghijklmnopqrstuvwxyz1234567890";
        String s = "";
        Random random = new Random();
        
        for (int i = 0; i < FILENAME_LENGTH; i++) {
            char c = alphabet.charAt(random.nextInt(36));
            s+=c;
        }
         
        return s;
	}

	public static void main(String[] args) {
		GlusterFs fs = new GlusterFs();
/*
 * 
		fs.setGlusterHost("VKHLIFELUVNODE.pru.intranet.asia");
		try {
			fs.authenticate("aps:swiftTest", "testing");
			fs.createContainer("BALA123456");
			fs.createFile(
					"BALA123456",
					"poldoc5.jpg",
					"C:/Users/balasubramaniam/workspace-fuse-training/gluster-example/src/main/resources/Policy-Printing-Solution.jpg",
					"image/jpeg");

			fs.getFileToLocal(
					"BALA123456",
					"poldoc5.jpg",
					"C:/Users/balasubramaniam/workspace-fuse-training/gluster-example/src/main/resources/Policy-Printing-Solution"
							+ System.currentTimeMillis() + ".jpg");

		} catch (GlusterAuthnFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GlusterRequestFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	*/
		
		System.out.println("Filename:"+fs.generateRandomFileName());
	}	
}
