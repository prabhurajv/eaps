package eapsfront.dao;

import eapsfront.model.DownloadLog;

public interface IDownloadLogDao {
	public void insertLog(DownloadLog log);
}
