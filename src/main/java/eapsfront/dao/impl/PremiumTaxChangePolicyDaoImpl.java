package eapsfront.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.IPremiumTaxChangePolicyDao;
import eapsfront.model.PremiumTaxChangePolicy;
import eapsfront.model.RptTaxChange;
import eapsfront.model.TaxChangeVisitLog;

@Repository
@Transactional
public class PremiumTaxChangePolicyDaoImpl implements IPremiumTaxChangePolicyDao {
	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public int addVisitLog(String browser, String link, String ip, Date visitDate) {
		Session session = sessionFactory.openSession();

		int affected = 0;

		try {
			Transaction tran = session.beginTransaction();

			TaxChangeVisitLog log = new TaxChangeVisitLog();
			log.setBrowser(browser);
			log.setLink(link);
			log.setIp(ip);
			log.setVisitDate(visitDate);

			session.save(log);
			tran.commit();

			affected = 1;
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return affected;
	}

	@Override
	public Map<String, Object> GeneratePrintPremiumTaxChange(PremiumTaxChangePolicy premiumTaxChangePolicy) {
        Map<String,Object> 	mapResult = new HashMap<String, Object>();

		Session session = sessionFactory.openSession();
		try {

			Query querySetting = session.createSQLQuery(
					"SELECT query, templatelatin,templatekhmer FROM eaps.premtaxchtemplate WHERE code=:code");
			querySetting.setParameter("code", premiumTaxChangePolicy.getProtection_upgr_eligible());
			List<Object[]> querySettingResult = querySetting.list();

			Query txchQuery = session.createSQLQuery(querySettingResult.get(0)[0].toString()).setParameter("policy_num",
			premiumTaxChangePolicy.getPolicy_num());
			txchQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			List<Map<String, Object>> txchResult = txchQuery.list();
			
			ObjectMapper mapper = new ObjectMapper();
			mapResult.put("TaxChange", mapper.writeValueAsString(txchResult.get(0)));

			mapResult.put("TemplateEN", querySettingResult.get(0)[1].toString());
			mapResult.put("TemplateKH", querySettingResult.get(0)[2].toString());
			
		} catch (HibernateException ex) {
			ex.printStackTrace();
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return mapResult;
	}

	@Override
	public PremiumTaxChangePolicy GetPremiumTaxChange(String policyNum) {
		PremiumTaxChangePolicy premiumTaxChangePolicy = null;

		Session session = null;
		session = sessionFactory.openSession();

		Query query = session.createSQLQuery("SELECT  id, policy_num,payment_method,customer_decision,protection_upgr_eligible,po_dob FROM eaps.prem_tax_chg_policy where policy_num=:policy_num")
	            .setResultTransformer(Transformers.aliasToBean(PremiumTaxChangePolicy.class));
		query.setString("policy_num", policyNum);

		try {
			ArrayList<PremiumTaxChangePolicy> list = (ArrayList<PremiumTaxChangePolicy>) query.list();
			if(list != null && list.size()> 0)
			{
				premiumTaxChangePolicy=list.get(0);
			}
		} catch (HibernateException ex) {
			ex.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return premiumTaxChangePolicy;
	}

	@Override
	public ArrayList<RptTaxChange> getLstRptTaxChange(String userId, String keyword) {
		ArrayList<RptTaxChange> list = null;

		Session session = null;
		session = sessionFactory.openSession();

		Query query = session.createSQLQuery("SELECT *  FROM eaps.rpttaxchange(:userId, :keyword)");
		query.setString("userId", userId);
		query.setString("keyword", keyword);

		try {
			list = (ArrayList<RptTaxChange>) query.list();
		} catch (HibernateException ex) {
			ex.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return list;
	}

	@Override
	public int setCustomerDecision(String customer_decision, String policyNum) {
		Session session = sessionFactory.openSession();

		String sql = "UPDATE \"eaps\".prem_tax_chg_policy SET customer_decision=?,"
					+ "customer_decision_date=CAST(? AS timestamp without time zone) WHERE policy_num=?";
	
		Query query = session.createSQLQuery(sql);
		query.setParameter(0, customer_decision);
		query.setParameter(1, new Date());
		query.setParameter(2, policyNum);

		int affected = 0;

		try {
			affected = query.executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return affected;
	}

	@Override
	public int setUpsellInterested(String is_upsell_interested, String policyNum) {
		Session session = sessionFactory.openSession();

		String sql = "UPDATE \"eaps\".prem_tax_chg_policy SET is_upsell_interested=?"
					+ " WHERE policy_num=?";
	
		Query query = session.createSQLQuery(sql);
		query.setParameter(0, is_upsell_interested);
		query.setParameter(1, policyNum);

		int affected = 0;

		try {
			affected = query.executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return affected;
	}
}