package eapsfront.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.IDownloadLogDao;
import eapsfront.model.DownloadLog;

@Repository
@Transactional
public class DownloadLogDaoImpl implements IDownloadLogDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insertLog(DownloadLog log) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try{
			session.save(log);
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}finally{
			session.flush();
			session.close();
		}
	}
}
