package eapsfront.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eapsfront.dao.ITaxChangeDao;
import eapsfront.model.LifePF;
import eapsfront.model.RptTaxChange;
import eapsfront.model.TaxChange;
import eapsfront.model.TaxChangeLog;
import eapsfront.model.TaxChangeVisitLog;
import eapsfront.model.ViewLang;

@Repository
@Transactional
public class TaxChangeDaoImpl implements ITaxChangeDao {
	@Autowired
	private SessionFactory sessionFactory;

	// @Override
	// public int setCustomerChoice(boolean choice, String choiceType, String link) {
	// 	Session session = sessionFactory.openSession();

	// 	String sql = "";

	// 	if (choiceType.compareTo("increase") == 0) {
	// 		sql = "UPDATE \"eaps\".tab_taxchange_logs SET approved=?,"
	// 				+ "approved_date=CAST(? AS timestamp without time zone),rejected=false,rejected_date=NULL WHERE link=?";
	// 	} else {
	// 		sql = "UPDATE \"eaps\".tab_taxchange_logs SET rejected=?,"
	// 				+ "rejected_date=CAST(? AS timestamp without time zone),approved=false,approved_date=NULL WHERE link=?";
	// 	}

	// 	Query query = session.createSQLQuery(sql);
	// 	query.setParameter(0, choice);
	// 	query.setParameter(1, new Date());
	// 	query.setParameter(2, link);

	// 	int affected = 0;

	// 	try {
	// 		affected = query.executeUpdate();
	// 	} catch (HibernateException e) {
	// 		e.printStackTrace();
	// 	} finally {
	// 		session.flush();
	// 		session.close();
	// 	}

	// 	return affected;
	// }

	// @Override
	// public int addVisitLog(String browser, String link, String ip, Date visitDate) {
	// 	Session session = sessionFactory.openSession();

	// 	int affected = 0;

	// 	try {
	// 		Transaction tran = session.beginTransaction();

	// 		TaxChangeVisitLog log = new TaxChangeVisitLog();
	// 		log.setBrowser(browser);
	// 		log.setLink(link);
	// 		log.setIp(ip);
	// 		log.setVisitDate(visitDate);

	// 		session.save(log);
	// 		tran.commit();

	// 		affected = 1;
	// 	} catch (HibernateException e) {
	// 		e.printStackTrace();
	// 	} finally {
	// 		session.flush();
	// 		session.close();
	// 	}

	// 	return affected;
	// }

	@Override
	public TaxChangeLog getTaxChangeLog(String chdrNum, String link) {
		Session session = null;
		session = sessionFactory.openSession();

		TaxChangeLog log = null;

		Criterion crit1 = Restrictions.eq("chdrNum", chdrNum);

		Criterion crit2 = Restrictions.eq("link", link);

		Criteria cr = session.createCriteria(TaxChangeLog.class);

		LogicalExpression andExp = Restrictions.and(crit1, crit2);

		cr.add(andExp);

		try {
			if (cr.list().size() > 0) {
				log = (TaxChangeLog) cr.list().get(0);

				if (log != null) {
					Hibernate.initialize(log.getTaxChange());
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return log;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<LifePF> getLifePFs(String chdrNum) {
		Session session = sessionFactory.openSession();
		ArrayList<LifePF> lifePFs = null;

		Criteria cr = session.createCriteria(LifePF.class);
		cr.add(Restrictions.eq("chdrNum", chdrNum)).addOrder(Order.asc("chdrLife")).addOrder(Order.asc("clntNum"));

		try {
			lifePFs = (ArrayList<LifePF>) cr.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return lifePFs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ViewLang> getLangs(String lang, String page) {
		Session session = sessionFactory.openSession();
		ArrayList<ViewLang> viewLangs = null;

		Criterion crit1 = Restrictions.eq("lang", lang);

		Criterion crit2 = Restrictions.eq("page", page);

		Criteria cr = session.createCriteria(ViewLang.class);

		LogicalExpression andExp = Restrictions.and(crit1, crit2);

		cr.add(andExp);

		try {
			viewLangs = (ArrayList<ViewLang>) cr.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return viewLangs;
	}

	@Override
	public TaxChangeLog getTaxChangeLog(String chdrNum) {
		Session session = sessionFactory.openSession();
		TaxChangeLog log = null;

		Criteria cr = session.createCriteria(TaxChangeLog.class);

		cr.add(Restrictions.eq("chdrNum", chdrNum));

		try {
			if (cr.list().size() > 0) {
				log = (TaxChangeLog) cr.list().get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return log;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<TaxChange> getTaxChange(TaxChangeLog log) {
		ArrayList<TaxChange> taxChangeList = null;

		Session session = null;
		session = sessionFactory.openSession();

		// Criteria cr = session.createCriteria(TaxChange.class);
		//
		// cr.add(Restrictions.eq("chdrNum", log.getChdrNum()));

		try {
			// if(cr.list().size() > 0){
			// taxChangeList = (ArrayList<TaxChange>)cr.list();
			// }
			taxChangeList = new ArrayList<TaxChange>();
			for (int i = 0; i < log.getLifePFs().size(); i++) {
				Criteria cr = session.createCriteria(TaxChange.class);

				cr.add(Restrictions.eq("chdrNum", log.getChdrNum()));
				cr.add(Restrictions.eq("lifeCNum", log.getLifePFs().get(i).getClntNum()));

				if (cr.uniqueResult() != null) {
					taxChangeList.add((TaxChange) cr.uniqueResult());
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return taxChangeList;
	}

	@Override
	public TaxChange getTaxChangeOne(TaxChangeLog log) {
		TaxChange taxChange = new TaxChange();

		Session session = null;
		session = sessionFactory.openSession();

		Criteria cr = session.createCriteria(TaxChange.class);

		cr.add(Restrictions.eq("chdrNum", log.getChdrNum()));

		try {
			if (cr.list().size() > 0) {
				taxChange = (TaxChange) cr.list().get(0);
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return taxChange;
	}
}
