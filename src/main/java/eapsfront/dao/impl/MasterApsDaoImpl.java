package eapsfront.dao.impl;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.IMasterApsDao;
import eapsfront.model.MasterAps;

@Repository
@Transactional
public class MasterApsDaoImpl implements IMasterApsDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public String getDownloadUrl(String criteria, MasterAps masterAps) {
		String policyNum = criteria.substring(0, 8);
		int year = Integer.parseInt(criteria.substring(criteria.length()-2, criteria.length()));
		
		Session session = sessionFactory.openSession();

		String  downloadUrl = "";
		
		if(masterAps.getChdrNum().trim().equals(policyNum) && masterAps.getApsYearTh() == year) {
			downloadUrl = masterAps.getApsUrl();
			
			Transaction tx = session.beginTransaction();
			
			try{
				if(masterAps.getDownloadAttempt() > 0 || masterAps.getLastAttemptDate() != null) {
					masterAps.setDownloadAttempt(0);
					masterAps.setLastAttemptDate(null);
					
					session.update(masterAps);
					tx.commit();
				}
			}catch(HibernateException e) {
				tx.rollback();
			}finally{
				session.flush();
				session.close();
			}
		}else {
			Transaction tx = session.beginTransaction();
			
			try{
				masterAps.setDownloadAttempt(masterAps.getDownloadAttempt() + 1);
				masterAps.setLastAttemptDate(new Date());
				
				session.update(masterAps);
				tx.commit();
			}catch(HibernateException e) {
				tx.rollback();
			}finally{
				session.flush();
				session.close();
			}
		}
		
		return downloadUrl;
	}

	@Override
	public MasterAps getItem(String policyNum, int year) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria cr = session.createCriteria(MasterAps.class);
		cr.add(Restrictions.eq("chdrNum", policyNum));
		cr.add(Restrictions.eq("apsYearTh", year));
		
		MasterAps item = null;
		
		try{
			item = (MasterAps) cr.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return item;
	}

	@Override
	public MasterAps getItem(String pathHash) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria cr = session.createCriteria(MasterAps.class);
		cr.add(Restrictions.eq("paramHash", pathHash));
		
		MasterAps item = null;
		
		try{
			item = (MasterAps) cr.uniqueResult();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return item;
	}

	@Override
	public void setDownloadAttempt(MasterAps masterAps, boolean reset) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try{
			if(reset) {
				masterAps.setDownloadAttempt(0);
			}else {
				masterAps.setDownloadAttempt(masterAps.getDownloadAttempt() + 1);
			}
			
			masterAps.setLastAttemptDate(new Date());
			session.update(masterAps);
			tx.commit();
		}catch (HibernateException e) {
			tx.rollback();
		}finally{
			session.flush();
			session.close();
		}
	}

	@Override
	public String getDownloadUrl(String criteria) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria crx = session.createCriteria(MasterAps.class);
		crx.add(Restrictions.eq("paramHash", criteria));
		
		MasterAps item = null;
		
		try{
			item = (MasterAps) crx.uniqueResult();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}

		String  downloadUrl = "";
		
		downloadUrl = item.getApsUrl();
		
		return downloadUrl;
	}

}
