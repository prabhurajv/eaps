package eapsfront.dao.impl;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.ILogDao;
import eapsfront.model.Log;
import eapsfront.model.UserHistory;

@Repository
@Transactional
public class LogDaoImpl implements ILogDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void logUserAction(String username, String action, String ip, String pip, Date date) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		
		Log log = new Log();
		log.setUsername(username);
		log.setAction(action);
		log.setIp(ip);
		log.setPip(pip);
		log.setLogDate(date);
		
		Transaction tx = session.beginTransaction();
		
		try{
			
			session.save(log);
			tx.commit();
			
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
	}

	@Override
	public void logChangePwd(String username, String action, String oldPwd, String operator, Date date) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		UserHistory history = new UserHistory();
		history.setUsername(username);
		history.setAction(action);
		history.setPwd(oldPwd);
		history.setOperator(operator);
		history.setDate(date);
		
		try{
			session.saveOrUpdate(history);
			tx.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
			tx.rollback();
		}finally{
			session.flush();
			session.close();
		}
	}

}
