package eapsfront.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.IRptMasterAps;
import eapsfront.model.RptMasterAps;

@Repository
@Transactional
public class RptMasterApsImpl implements IRptMasterAps {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RptMasterAps> getRptMasterApsList(String userId, String keyword) {
		// TODO Auto-generated method stub
		List<RptMasterAps> rptMasterApsList = null;
		
		Session session = null;
		session = sessionFactory.openSession();
		
		Query query = session.createSQLQuery("SELECT * FROM \"eaps\".rptapsmaster(?,?)");
		query.setParameter(0, userId);
		query.setParameter(1, keyword);
		
		try{
			rptMasterApsList = (List<RptMasterAps>) query.list();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return rptMasterApsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RptMasterAps> getDefaultRptMasterApsList(String userId) {
		// TODO Auto-generated method stub
		List<RptMasterAps> rptMasterApsList = null;
		
		Session session = null;
		session = sessionFactory.openSession();
		
		Query query = session.createSQLQuery("SELECT * FROM \"eaps\".rptapsmaster(?,?) LIMIT 10");
		query.setParameter(0, userId);
		query.setParameter(1, "");
		
		try{
			rptMasterApsList = (List<RptMasterAps>) query.list();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return rptMasterApsList;
	}

	@Override
	public String getPolicyOwnerVerifiedResult(String userId, String hash) {
		// TODO Auto-generated method stub
		String result = "";
		
		Session session = null;
		session = sessionFactory.openSession();
		
		Query query = session.createSQLQuery("SELECT policypassfail FROM \"eaps\".rptapsmasterverify(?,?)");
		query.setParameter(0, userId);
		query.setParameter(1, hash);
		
		try{
			result = query.list().get(0).toString();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return result;
	}
}
