package eapsfront.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.IUserDao;
import eapsfront.model.User;
import eapsfront.model.UserHistory;
import eapsfront.util.PwdEncoder;

@Repository
@Transactional
public class UserDaoImpl implements IUserDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User getUser(String username) {
		// TODO Auto-generated method stub
		User user = null;
		
		Session session = null;
		session = sessionFactory.openSession();
		Criteria crit = session.createCriteria(User.class);
		
		try{
			crit.add(Restrictions.eq("username", username));
			
			user = (User) crit.uniqueResult();
			
			// Prevent LazyInitializationException when Spring try to get roles while session is already closed.
			// This exception happens because of relationship between models.
			if(user != null) {
				Hibernate.initialize(user.getRoles());
			}
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return user;
	}

	@Override
	public void setLoginAttempt(User user) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		String sql = "";
		
		try{
			Query query = null;
			
			if(user.getAttemptNo() == 2) {
				sql = "UPDATE \"eaps\".sec_users SET attemp_no = ?, "
						+ "locked = '1', account_lock_date = CAST(? AS timestamp without time zone) WHERE username=?";
				
				query = session.createSQLQuery(sql);
				query.setParameter(0, user.getAttemptNo() + 1);
				query.setParameter(1, new Date());
				query.setParameter(2, user.getUsername());
				
				query.executeUpdate();
			}else if(user.getAttemptNo() < 2){
				sql = "UPDATE \"eaps\".sec_users SET attemp_no = ? "
						+ "WHERE username=?";
				
				query = session.createSQLQuery(sql);
				query.setParameter(0, user.getAttemptNo() + 1);
				query.setParameter(1, user.getUsername());
				
				query.executeUpdate();
			}
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
	}

	@Override
	public int updateUserPwd(User user) {
		// TODO Auto-generated method stub
		int affected = 0;
		
		Session session = null;
		
		session = sessionFactory.openSession();
		
		Query query = null;
		
		try{
			String sql = "UPDATE \"eaps\".sec_users SET password = ? WHERE username=?";
			query = session.createSQLQuery(sql);
			query.setParameter(0, user.getPwd());
			query.setParameter(1, user.getUsername());
			
			query.executeUpdate();

			affected = 1;
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return affected;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isPwdAlreadyUsed(String username, String newRawPwd) {
		// TODO Auto-generated method stub
		boolean isUsed = false;
		
		Session session = null;
		session = sessionFactory.openSession();
		
		Criteria crx = session.createCriteria(UserHistory.class);
		crx.add(Restrictions.eq("username", username));
		
		List<UserHistory> userHistories = null;
		
		try{
			userHistories = crx.list();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		if(userHistories != null && userHistories.size() > 0) {
			for(UserHistory userHistory : userHistories) {
				if(PwdEncoder.matchesPwd(newRawPwd, userHistory.getPwd())) {
					isUsed = true;
					
					break;
				}
			}
		}
		
		return isUsed;
	}

	@Override
	public void disableUser() {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery("select \"eaps\".spgen_disableuser()");
		
		try{
			query.executeUpdate();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			query = null;
			session.flush();
			session.close();
		}
	}

	@Override
	public void resetLoginAttemptAndLastLoginDate(String username) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery("update \"eaps\".sec_users set attemp_no = ?, last_login_date = ? where username = ?");
		query.setParameter(0, 0);
		query.setParameter(1, new Date());
		query.setParameter(2, username);
		
		try{
			query.executeUpdate();
		}catch(HibernateException e) {
			e.printStackTrace();
		}finally{
			query = null;
			session.flush();
			session.close();
		}
	}
}
