package eapsfront.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eapsfront.dao.ILangDao;
import eapsfront.model.Lang;

@Repository
@Transactional
public class LangDaoImpl implements ILangDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Lang> getLangList(String lang, String... tagId) {
		Session session = null;
		session = sessionFactory.openSession();
		
		List<String> tagIdList = Arrays.asList(tagId);
		
		Criterion condition1 = Restrictions.eq("lang", lang);
		Criterion condition2 = Restrictions.eq("tagId", tagIdList.get(0));
		
//		if(tagIdList.size() > 1){
//			tagIdList.remove(0);
//			
//			LogicalExpression orExp = null;
//			
//			for(String aTagId : tagIdList){
//				
//			}
//		}
		
		LogicalExpression andExp = Restrictions.and(condition1, condition2);
		
		Criteria crit = session.createCriteria(Lang.class);
		crit.add(andExp);
		crit.addOrder(Order.asc("id"));
		
		ArrayList<Lang> langs = new ArrayList<Lang>();
		
		try{
			langs = (ArrayList<Lang>) crit.list();
		}catch(HibernateException ex){
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return langs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getSuccessMsg(String lang, String tagId) {
		Session session = null;
		session = sessionFactory.openSession();
		
		Criterion condition1 = Restrictions.eq("lang", lang);
		Criterion condition2 = Restrictions.eq("tagId", tagId);
		
		LogicalExpression andExp = Restrictions.and(condition1, condition2);
		
		Criteria crit = session.createCriteria(Lang.class);
		crit.add(andExp);
		crit.addOrder(Order.asc("id"));
		
		String successMsg = "";
		
		try{
			if(crit.list().size() > 0){
				successMsg = ((Lang)crit.list().get(0)).getDesc();
			}
		}catch(HibernateException ex){
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return successMsg.trim();
	}

	@Override
	public String GetLangs() {
        String json = null;

		Session session = sessionFactory.openSession();
		try {
			Query query = session.createSQLQuery("select trim(lang) as lang, trim(tag_id) as tag_id, trim(\"desc\") as description from eaps.tab_lang");
			
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			List<Map<String, Object>> result = query.list();
			
			ObjectMapper mapper = new ObjectMapper();
			json =  mapper.writeValueAsString(result);
			
		} catch (HibernateException ex) {
			ex.printStackTrace();
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return json;
	}
}
