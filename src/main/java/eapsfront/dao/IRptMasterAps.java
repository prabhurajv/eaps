package eapsfront.dao;

import java.util.List;

import eapsfront.model.RptMasterAps;

public interface IRptMasterAps {
	public List<RptMasterAps> getRptMasterApsList(String userId, String keyword);
	public List<RptMasterAps> getDefaultRptMasterApsList(String userId);
	public String getPolicyOwnerVerifiedResult(String userId, String hash);
}
