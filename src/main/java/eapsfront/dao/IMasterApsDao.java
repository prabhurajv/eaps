package eapsfront.dao;

import eapsfront.model.MasterAps;

public interface IMasterApsDao {
	public String getDownloadUrl(String criteria, MasterAps masterAps);
	public String getDownloadUrl(String criteria);
	public MasterAps getItem(String policyNum, int year);
	public MasterAps getItem(String pathHash);
	public void setDownloadAttempt(MasterAps masterAps, boolean reset);
}
