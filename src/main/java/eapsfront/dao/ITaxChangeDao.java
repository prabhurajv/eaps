package eapsfront.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import eapsfront.model.LifePF;
import eapsfront.model.RptTaxChange;
import eapsfront.model.TaxChange;
import eapsfront.model.TaxChangeLog;
import eapsfront.model.ViewLang;

public interface ITaxChangeDao {
	// public int setCustomerChoice(boolean choice, String choiceType, String link);
	// public int addVisitLog(String browser, String link, String ip, Date visitDate);
	public TaxChangeLog getTaxChangeLog(String chdrNum, String link);
	public TaxChangeLog getTaxChangeLog(String chdrNum);
	public ArrayList<LifePF> getLifePFs(String chdrNum);
	public ArrayList<ViewLang> getLangs(String lang, String page);
	public ArrayList<TaxChange> getTaxChange(TaxChangeLog log);
	public TaxChange getTaxChangeOne(TaxChangeLog log);
}
