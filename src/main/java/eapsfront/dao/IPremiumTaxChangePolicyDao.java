package eapsfront.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import eapsfront.model.PremiumTaxChangePolicy;
import eapsfront.model.RptTaxChange;

public interface IPremiumTaxChangePolicyDao {
	public Map<String,Object> GeneratePrintPremiumTaxChange(PremiumTaxChangePolicy premiumTaxChangePolicy);
	public PremiumTaxChangePolicy GetPremiumTaxChange(String policyNum);
	public ArrayList<RptTaxChange> getLstRptTaxChange(String userId, String keyword);
	public int  setCustomerDecision(String customer_decision, String policyNum);
	public int  setUpsellInterested(String is_upsell_interested, String policyNum);
	public int addVisitLog(String browser, String link, String ip, Date visitDate);
}
