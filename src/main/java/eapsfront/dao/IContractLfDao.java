package eapsfront.dao;

import eapsfront.model.ContractLf;

public interface IContractLfDao {
	public ContractLf getContractLf(String agntNum, String policyNum, String appNum);
}
