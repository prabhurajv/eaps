package eapsfront.dao;

import eapsfront.model.User;

public interface IUserDao {
	public User getUser(String username);
	public void setLoginAttempt(User user);
	public int updateUserPwd(User user);
	public boolean isPwdAlreadyUsed(String username, String newRawPwd);
	public void disableUser();
	public void resetLoginAttemptAndLastLoginDate(String username);
}
