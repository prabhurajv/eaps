package eapsfront.dao;

import java.util.ArrayList;
import java.util.Map;

import eapsfront.model.Lang;

public interface ILangDao {
	public ArrayList<Lang> getLangList(String lang, String... tagId);
	public String GetLangs();
	public String getSuccessMsg(String lang, String tagId);
}
