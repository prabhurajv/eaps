package eapsfront.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;


public class EAPSSecurityHelper {
	 	private final String cipherTransformation = "AES/CBC/PKCS5Padding";
	 	private final String chEncoding = "UTF-8";
	    private final String aesEncryptionAlgorithm = "AES";
	 
	    private  byte[] decrypt(byte[] cipherText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
	    {
	        Cipher cipher = Cipher.getInstance(cipherTransformation);
	        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
	        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
	        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
	        cipherText = cipher.doFinal(cipherText);
	        return cipherText;
	    }
	 
	    private byte[] encrypt(byte[] plainText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
	    {
	        Cipher cipher = Cipher.getInstance(cipherTransformation);
	        SecretKeySpec secretKeySpec = new SecretKeySpec(key, aesEncryptionAlgorithm);
	        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
	        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
	        plainText = cipher.doFinal(plainText);
	        return plainText;
	    }
	 
	    private byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
	        byte[] keyBytes= new byte[16];
	        byte[] parameterKeyBytes= key.getBytes(chEncoding);
	        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
	        return keyBytes;
	    }

	    public String encrypt(String plainText, String key) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
	        byte[] plainTextbytes = plainText.getBytes(chEncoding);
	        byte[] keyBytes = getKeyBytes(key);
	        return Base64.getEncoder().encodeToString(encrypt(plainTextbytes,keyBytes,keyBytes));
	    }
	 

	    public String decrypt(String encryptedText, String key) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException{
	        byte[] cipheredBytes = Base64.getDecoder().decode(encryptedText);
	        byte[] keyBytes = getKeyBytes(key);
	        return new String(decrypt(cipheredBytes, keyBytes, keyBytes), chEncoding);
	    }
	    
	    public static void main(String args[]) throws KeyException, GeneralSecurityException, IOException{
	    	String text = "";
	    	String encrypted = "";
	    	String decrypted = "";
	    	
	    	text = "80035500";
	    	System.out.println(text);
	    	
	    	encrypted = new EAPSSecurityHelper().encrypt(text, "05011987");
	    	if (encrypted.contains("/")) {
	    		encrypted = encrypted.replace("/", "hSalS");
            }
	    	System.out.println(encrypted);
	    	
	    	if (encrypted.contains("hSalS")) {
	    		encrypted = encrypted.replace("hSalS", "/");
            }
	    	decrypted = new EAPSSecurityHelper().decrypt(encrypted, "05011987");
	    	System.out.println(decrypted);
	    }

}
