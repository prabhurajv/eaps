package eapsfront.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PwdEncoder {
	public static String encodePwd(String rawPwd) {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		
		return encoder.encode(rawPwd);
	}
	
	public static boolean matchesPwd(String newPwd, String encryptedOldPwd) {
		boolean isMatched = false;
		
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		isMatched = encoder.matches(newPwd, encryptedOldPwd);
		
		return isMatched;
	}
}
