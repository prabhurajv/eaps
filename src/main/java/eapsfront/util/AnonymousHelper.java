package eapsfront.util;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AnonymousHelper {
	public static String getClientIP(HttpServletRequest request) {
//		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//		String ipAddress = request.getHeader("X-FORWARDED-FOR");
//		if (ipAddress == null) {
//		    ipAddress = request.getRemoteAddr();
//		}
		
		String ipAddress = request.getRemoteHost();
		
		return ipAddress;
	}
	
	public static String getReverseString(String original) {
		return new StringBuilder(original).reverse().toString();
	}
	
	public static boolean isPwdValid(String pwd){
		boolean isValid = false;
		
		String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{8,}$";
		
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(pwd);
		
		if(m.find()){
			isValid = true;
		}
		
		return isValid;
	}
	
	public static boolean isKeywordAllowed(String keyword){
		boolean isValid = false;
		
		String pattern = "^[a-zA-Z0-9]*$";
		
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(keyword);
		
		if(m.find()){
			isValid = true;
		}
		
		return isValid;
	}
	
	public static void main(String[] args){
		System.out.println(isPwdValid("Abc@123456"));
	}
}
