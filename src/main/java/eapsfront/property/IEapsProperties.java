package eapsfront.property;

public interface IEapsProperties {
	public String getUser();
	public String getPwd();
	public String getTaxChangeExpiredDay();
	public String getEffectiveDate();
}
