package eapsfront.property;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@Component
@Configuration
//@PropertySource("classpath:eaps.properties")
public class EapsProperties implements IEapsProperties {
//	@Value("${user}")
//	private String user;
//
//	@Value("${pwd}")
//	private String pwd;
	
	//To resolve ${} in @Value
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

//	@Override
//	public String getUser() {
//		return "KMpj6cE2JPaQUEGYXRq+B7Sm35NoDCdgCHA42icQOfQ=";
//	}
//
//	@Override
//	public String getPwd() {
//		return "2IdIdghbVYBtutCAMZHJLX3PXVjW1Yg0Dn+QMr3MDnY=";
//	}
	
	public String getUser(){
		return System.getProperty("user");
	}
	public String getPwd(){
		return System.getProperty("pwd");
	}
	
	public String getTaxChangeExpiredDay(){
		return System.getProperty("taxchangeLinkExpiredDay");
	}
	
	public String getEffectiveDate(){
		return System.getProperty("effectiveDate");
	}
}
