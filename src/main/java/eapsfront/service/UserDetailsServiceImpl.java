package eapsfront.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eapsfront.dao.IUserDao;
import eapsfront.model.Role;
import eapsfront.model.User;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private IUserDao userDao;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		userDao.disableUser();
		
		User user = userDao.getUser(username);
		
		if(user != null) {
			String pwd = user.getPwd();
			boolean enabled = user.isEnabled();
			boolean accountNonExpired = (user.getAccountExpiredDate() == null || (user.getAccountExpiredDate().getTime() - new Date().getTime()) >= 0);
			boolean credentialsNonExpired = (user.getPwdExpiredDate() == null || (user.getPwdExpiredDate().getTime() - new Date().getTime()) >= 0);
			boolean accountNonLocked = (user.isLocked() == false);
			
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			
			for (Role role : user.getRoles()) {
				// SimpleGrantedAuthority replaces GrantedAuthorityImpl
				authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
			}
			
			org.springframework.security.core.userdetails.User securityUser = 
					new org.springframework.security.core.userdetails.User(username, pwd, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
			
			return securityUser;
		}else {
			throw new UsernameNotFoundException("Username or password is invalid!");
		}
	}

}
