package eapsfront.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageAccessController {
	@RequestMapping(value = "/403")
	public String showAccessDeniedPage() {
		return "500-old";
	}
	
	@ExceptionHandler(Throwable.class)
	  public String handleAnyException(Throwable ex, HttpServletRequest request) {
	    return ClassUtils.getShortName(ex.getClass());
	  }
}
