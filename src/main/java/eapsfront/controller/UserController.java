package eapsfront.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import eapsfront.dao.ILogDao;
import eapsfront.dao.IUserDao;
import eapsfront.model.Pwd;
import eapsfront.model.User;
import eapsfront.util.AnonymousHelper;
import eapsfront.util.PwdEncoder;

@Controller
public class UserController {
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private ILogDao logDao;
	
	@RequestMapping(value = "/agent/changepwd" , method = RequestMethod.POST)
	public String changePwd(ModelMap model, HttpServletRequest request, HttpServletResponse response,
			@Valid Pwd pwd, BindingResult result) {
	    if(result == null) {
	    	model.put("error", "Something went wrong. Please try again.");
	    	
	    	return "agent";
	    }
	    
	    User user = userDao.getUser(SecurityContextHolder.getContext().getAuthentication().getName());
	    String oldPwd = user.getPwd();
	    
	    boolean isPwdValid = true;
	    
	    // Check if old pwd is correct
	    if(!PwdEncoder.matchesPwd(pwd.getOldPwd(), oldPwd)) {
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd is strong enough
	    if(isPwdValid && !AnonymousHelper.isPwdValid(pwd.getConfirmPwd())){
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd is the same as confirmed pwd
	    if(isPwdValid && (pwd.getNewPwd().compareTo(pwd.getConfirmPwd()) != 0)){
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd is the same as old pwd
	    if(isPwdValid && PwdEncoder.matchesPwd(pwd.getConfirmPwd(), oldPwd)){
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd has been used before
	    if(isPwdValid && isPwdHasBeenUsed(pwd.getConfirmPwd())){
	    	isPwdValid = false;
	    }
	    
	    if(!isPwdValid){
	    	model.put("pwd", new Pwd());
	    	model.put("pageTitle", "Update Credential");
	    	model.put("error", "Something went wrong. Please try again.");
	    	
	    	return "changepwd";
	    }
	    
	    String encodedPwd = PwdEncoder.encodePwd(pwd.getConfirmPwd());
	    user.setPwd(encodedPwd);
	    
	    int affected = userDao.updateUserPwd(user);
	    
	    if(affected > 0) {
	    	model.put("changepwd", "success");
	    	
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    	String username = auth.getName();
			String action = "change password successfully";
			String ip = request.getRemoteHost();
			String pip = request.getRemoteHost();
			Date date = new Date();
			
			logDao.logUserAction(username, action, ip, pip, date);
			logDao.logChangePwd(username, action, oldPwd, user.getOperator(), date);
			
			new SecurityContextLogoutHandler().logout(request, response, auth);
			
			return "redirect:/login?logout";
	    }else {
	    	model.put("error", "Something went wrong. Please try again.");
	    	
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    	String username = auth.getName();
			String action = "change password failed";
			String ip = request.getRemoteHost();
			String pip = request.getRemoteHost();
			Date date = new Date();
			
			logDao.logUserAction(username, action, ip, pip, date);
			
			model.put("pwd", new Pwd());
	    	model.put("pageTitle", "Update Credential");
	    	
	    	return "changepwd";
	    }
	}
	
	@RequestMapping(value = "/agent/changepwd", method = RequestMethod.GET)
	public String getChangePwdPage(ModelMap model, @Valid Pwd pwd, BindingResult result) {
		if(result == null) {
			model.put("pwd", new Pwd());
		}
		
		model.put("pageTitle", "Update Credential");
		
		return "changepwd";
	}
	
	@RequestMapping(value = "/agent/checkUsedPwd", method = RequestMethod.POST)
	@ResponseBody
	public String checkUsedPwd(HttpServletRequest request) {
		boolean isNotUsed = false;
		String newPwd = request.getParameter("newPwd");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		isNotUsed = !userDao.isPwdAlreadyUsed(auth.getName(), newPwd);
		
		return isNotUsed + "";
	}
	
	private boolean isPwdHasBeenUsed(String pwd){
		boolean isUsed = false;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		isUsed = userDao.isPwdAlreadyUsed(auth.getName(), pwd);
		
		return isUsed;
	}
	
	@RequestMapping(value="/forcelogout", method = RequestMethod.GET)
	public void forceLogout(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		new SecurityContextLogoutHandler().logout(request, response, auth);
	}
	
	@ExceptionHandler(HttpSessionRequiredException.class)
	public String handleSessionExpired() {
		return "redirect:/login";
	}
}
