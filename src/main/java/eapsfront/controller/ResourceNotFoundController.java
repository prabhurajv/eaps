package eapsfront.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import eapsfront.handler.ResourceNotFoundHandler;

public class ResourceNotFoundController {

	@ExceptionHandler(ResourceNotFoundHandler.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@RequestMapping(value = "/**")
	public String redirectToPageNotFound() {
		return "pagenotfound";
	}
}
