package eapsfront.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import eapsfront.dao.IDownloadLogDao;
import eapsfront.dao.IMasterApsDao;
import eapsfront.gluster.GlusterAuthnFailedException;
import eapsfront.gluster.GlusterFs;
import eapsfront.handler.ResourceNotFoundHandler;
import eapsfront.model.CustomerFormData;
import eapsfront.model.DownloadLog;
import eapsfront.model.MasterAps;
import eapsfront.property.IEapsProperties;
import eapsfront.util.AES;
import eapsfront.util.AnonymousHelper;
import eapsfront.util.EAPSSecurityHelper;

@Controller
@SessionAttributes("pageTitle")
public class MainController {
	
	@Autowired
	private IMasterApsDao masterApsDao;
	
	@Autowired
	private IDownloadLogDao downloadLogDao;
	
	@Autowired
	private IEapsProperties properties;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ExceptionHandler(ResourceNotFoundHandler.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String redirectToPageNotFound() {
		return "pagenotfound";
	}
	
	@RequestMapping(value = "/{arg1}", method = RequestMethod.GET)
	public String GetPolicy(@PathVariable String arg1, ModelMap model) {
		model.addAttribute("pageTitle", "Prudential Cambodia Life Assurance");
		model.addAttribute("arg1", arg1);
		model.addAttribute("data", new CustomerFormData());

		return "getpolicy";
	}
	
	@RequestMapping(value = "/{arg1}", method = RequestMethod.POST)
	public String PostPolicy(@PathVariable String arg1, ModelMap model, HttpServletRequest request, HttpServletResponse response, @Valid CustomerFormData data, BindingResult result) {
		if(result.hasErrors()){
			return "getpolicy";
		}
		
		String pageTitle = model.get("pageTitle").toString();
		model.addAttribute("arg1", arg1);
		model.addAttribute("data", new CustomerFormData());
		model.addAttribute("pageTitle", pageTitle);
		
//		String gResponse = request.getParameter("g-recaptcha-response");
//		
//		if(gResponse == "" || gResponse == null) {
//			model.addAttribute("error", "Please verify that you are human by checking in the box below!");
//			return "getpolicy";
//		}
		
		GlusterFs fs = new GlusterFs();
		String user = AES.decrypt(properties.getUser());
		String pwd = AES.decrypt(properties.getPwd());
		String contentToDownload = "";
		String downloadUrl = "";
		String policyNum = "";
		int year = 0;
		String ip = "";
		Date downloadDate = new Date();
		MasterAps masterAps = null;
		String paramHash = data.getEncryptString().replace(AnonymousHelper.getReverseString("SlaSh"), "/");
		
		try {
			
			fs.authenticate(user, pwd);
			
			masterAps = masterApsDao.getItem(data.getEncryptString());
			
			if(masterAps == null) {
				model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
				return "getpolicy";
			}else {
				if(masterAps.getDownloadAttempt() > 2) {
					Date date = masterAps.getLastAttemptDate();
					Date current = new Date();
					
					long seconds = (current.getTime() - date.getTime()) / 1000;
					
					if(seconds > 3600) {
						masterApsDao.setDownloadAttempt(masterAps, true);
					}else {
						model.addAttribute("error", "ការទាញយករបស់អ្នកត្រូវបានបិទជាបណ្តោះអាសន្ន។​ សូមព្យាយាមម្តងទៀតបន្ទាប់ពី <br/> Your download has been locked. Please try again in <u>" + getWaitTime(seconds) + "</u>");
						return "getpolicy";
					}
				}
			}
			
			contentToDownload = new EAPSSecurityHelper().decrypt(paramHash, data.getDob());
			
			policyNum = contentToDownload.substring(0, 8);
			year = Integer.parseInt(contentToDownload.substring(contentToDownload.length()-2, contentToDownload.length()));
						
			downloadUrl = masterApsDao.getDownloadUrl(contentToDownload, masterAps);
			
			if(downloadUrl == "") {
				throw new Exception();
			}else{
				masterApsDao.setDownloadAttempt(masterAps, true);
			}
			
			ip = AnonymousHelper.getClientIP(request);
			
			fs.getFileToLocal(downloadUrl, response, false);
			
			DownloadLog log = new DownloadLog();
			log.setChdrNum(policyNum);
			log.setAnnYearTh(year);
			log.setIp(ip);
			log.setDownloadDate(downloadDate);
			
			downloadLogDao.insertLog(log);
			
		} catch (GlusterAuthnFailedException e) {
			 // TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (KeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			masterApsDao.setDownloadAttempt(masterAps, false);
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
		}
		
		return "getpolicy";
	}
	
	private String getWaitTime(long seconds) {
		String waitTime = "";
		long lockedHours = 3600; // equal to 1 hours;
		long hour = 0;
		long minute = 0;
		long second = 0;
		
		seconds = lockedHours - seconds;
		second = seconds % 60;
		minute = seconds / 60;
		hour = minute / 60;
		minute = minute % 60;
		
		if(hour > 0) {
			waitTime += hour + "h";
		}
		
		if(minute > 0) {
			waitTime += " " + minute + "m";
		}
		
		if(second > 0) {
			waitTime += " " + second + "s";
		}
		
		return waitTime;
	}
	
	@ExceptionHandler(HttpSessionRequiredException.class)
	public String handleSessionExpired() {
		return "redirect:/getpolicy";
	}
}
