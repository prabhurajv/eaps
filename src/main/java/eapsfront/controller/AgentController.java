package eapsfront.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.HtmlUtils;

import eapsfront.dao.IDownloadLogDao;
import eapsfront.dao.IMasterApsDao;
import eapsfront.dao.IPremiumTaxChangePolicyDao;
import eapsfront.dao.IRptMasterAps;
import eapsfront.dao.ITaxChangeDao;
import eapsfront.gluster.GlusterAuthnFailedException;
import eapsfront.gluster.GlusterFs;
import eapsfront.model.CustomerFormData;
import eapsfront.model.DownloadLog;
import eapsfront.model.MasterAps;
import eapsfront.model.PremiumTaxChangePolicy;
import eapsfront.model.RptMasterAps;
import eapsfront.model.RptTaxChange;
import eapsfront.model.TaxChangeLog;
import eapsfront.property.EapsValidationProperties;
import eapsfront.property.IEapsProperties;
import eapsfront.util.AES;
import eapsfront.util.AnonymousHelper;

@Controller
@SessionAttributes("log")
public class AgentController {
	@Autowired
	IRptMasterAps rptMasterApsDao;

	@Autowired
	private IMasterApsDao masterApsDao;

	@Autowired
	private IDownloadLogDao downloadLogDao;

	// @Autowired
	// private ITaxChangeDao taxChangeDao;
	
	@Autowired
	private IPremiumTaxChangePolicyDao premiumTaxChangePolicyDao;

	@Autowired
	private IEapsProperties properties;

	@RequestMapping(value = "/agent", method = RequestMethod.GET)
	public String getAgentHome(ModelMap model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<RptMasterAps> list = rptMasterApsDao.getDefaultRptMasterApsList(auth.getName());

		if (list == null) {
			list = new ArrayList<RptMasterAps>();
		}

		model.put("items", list);
		model.put("pageTitle", "eAPS Online");
		model.put("keyword", "");

		return "agent";
	}

	@RequestMapping(value = "/agent", method = RequestMethod.POST)
	public String postSearch(HttpServletRequest request, ModelMap model) {
		String keyword = request.getParameter("keyword");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<RptMasterAps> list = rptMasterApsDao.getRptMasterApsList(auth.getName(), keyword);

		if (list == null)
			list = new ArrayList<RptMasterAps>();

		model.put("pageTitle", "eAPS Online");
		model.put("items", list);
		model.put("keyword", keyword);

		return "agent";
	}

	@RequestMapping(value = "/agent/get/{param}", method = RequestMethod.GET)
	public String downloadPolicy(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		String hash = request.getParameter("hash");
		// String paramHash = hash.replace(AnonymousHelper.getReverseString("SlaSh"),
		// "/");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		model.put("pageTitle", "eAPS Online");

		String verifiedResult = rptMasterApsDao.getPolicyOwnerVerifiedResult(auth.getName(), hash);

		if (verifiedResult.compareTo(EapsValidationProperties.STR_IS_VALID) != 0) {
			return "agent";
		}

		GlusterFs fs = new GlusterFs();
		String username = AES.decrypt(properties.getUser());
		String pwd = AES.decrypt(properties.getPwd());
		String ip = "";
		Date downloadDate = new Date();
		MasterAps masterAps = null;

		try {

			fs.authenticate(username, pwd);

			masterAps = masterApsDao.getItem(hash);

			if (masterAps == null) {
				throw new Exception();
			}

			ip = AnonymousHelper.getClientIP(request);

			fs.getFileToLocal(masterAps.getApsUrl(), response, true);

			DownloadLog log = new DownloadLog();
			log.setChdrNum(masterAps.getChdrNum());
			log.setAnnYearTh(masterAps.getApsYearTh());
			log.setIp(ip);
			log.setUserName(auth.getName());
			log.setDownloadDate(downloadDate);

			downloadLogDao.insertLog(log);

		} catch (GlusterAuthnFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (KeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			masterApsDao.setDownloadAttempt(masterAps, false);
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "Something went wrong. Please try again.");
		}

		return "agent";
	}

	@RequestMapping(value = "/agent/taxchange", method = RequestMethod.GET)
	public String agentTaxChangeHome(ModelMap model) {
		model.addAttribute("pageTitle", "Tax Change");
		return "agent-taxchange";
	}

	@RequestMapping(value = "/agent/taxchange", method = RequestMethod.POST)
	public String agentSearchTaxChange(HttpServletRequest request, ModelMap model) {
		List<RptTaxChange> lstRptTaxChange = new ArrayList<RptTaxChange>();

		String keyword = request.getParameter("keyword");

		if (!AnonymousHelper.isKeywordAllowed(keyword)) {
			model.addAttribute("pageTitle", "Tax Change");
			model.addAttribute("keyword", HtmlUtils.htmlEscape(keyword));
			model.addAttribute("error", "Your keyword is invalid!");

			return "agent-taxchange";
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		lstRptTaxChange = premiumTaxChangePolicyDao.getLstRptTaxChange(auth.getName(), keyword);

		model.addAttribute("pageTitle", "Tax Change");
		model.addAttribute("lstRptTaxChange", lstRptTaxChange);
		model.addAttribute("keyword", keyword);

		return "agent-taxchange";
	}

	// Remove it and using printTaxChange instead TAXCHANGE V.2 2019-03-09 By Malen SOK
//	@RequestMapping(value = "/agent/taxchange/print", method = RequestMethod.GET)
//	public String agentPrintTaxChange(HttpServletRequest request, ModelMap model) {
//		String chdrNum = request.getParameter("policynum");
//
//		taxChangeLog = taxChangeDao.getTaxChangeLog(chdrNum);
//
//		if (taxChangeLog == null) {
//			model.addAttribute("data", new CustomerFormData());
//			model.addAttribute("pageTitle", "Tax Change");
//			model.addAttribute("error",
//					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
//
//			return "taxchange";
//		}
//
//		taxChangeLog.setLifePFs(taxChangeDao.getLifePFs(chdrNum));
//		taxChangeLog.setTaxChange(taxChangeDao.getTaxChange(taxChangeLog));
//		taxChangeLog.setEffectiveDate(properties.getEffectiveDate());
//
//		model.addAttribute("log", taxChangeLog);
//
//		chdrNum = chdrNum.substring(0, 1) + "XXXXXX" + chdrNum.substring(chdrNum.length() - 1, chdrNum.length());
//
//		return "redirect:/taxchange/" + chdrNum + "/" + taxChangeLog.isApproved();
//	}
	
	//TAXCHANGE V.2 2019-03-09 By Malen SOK
	@RequestMapping(value = "/agent/premiumTaxChange/get", method = RequestMethod.GET)
	public String getPremiumTaxChangePolicy(HttpServletRequest request, ModelMap model) {
		String contractNumber = request.getParameter("policynum");

		PremiumTaxChangePolicy premiumTaxChangePolicy = premiumTaxChangePolicyDao.GetPremiumTaxChange(contractNumber);
		if(premiumTaxChangePolicy == null)
		{
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Tax Change");
			model.addAttribute("error",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");

			return "customerPremiumTaxChange";
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().removeAttribute("premiumTaxChangePolicy");
		attr.getRequest().getSession().setAttribute("premiumTaxChangePolicy",premiumTaxChangePolicy);
		
		contractNumber = contractNumber.substring(0, 1) + "XXXXXX" + contractNumber.substring(contractNumber.length() - 1, contractNumber.length());
		return "redirect:/showPremiumTaxChange/" + contractNumber;
	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String handleSessionExpired() {
		return "redirect:/login";
	}
}
