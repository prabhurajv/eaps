package eapsfront.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eapsfront.model.User;

@Controller
public class LoginController {
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLogin(@RequestParam(value = "error", required = false) String error
			, @RequestParam(value = "logout", required = false) String logout
			, @RequestParam(value = "expired", required = false) String expired
			, HttpServletRequest request, ModelMap model, @Valid User user, BindingResult result) {
		if(result == null) {
			return "login";
		}
		model.put("user", new User());
		model.put("pageTitle", "Login");
		
		if(!SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser"))
			return "redirect:/agent";
		
		if(error != null) {
			model.put("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}
		
		if (logout != null) {
			String changepwd = request.getParameter("changepwd");
			
			if(changepwd != null) {
				model.put("msg", "Your credential has been changed successfully.");
			} else {
				model.put("msg", "You've been logged out successfully.");
			}
		}
		
		if (expired != null) {
			if(expired.equals("1"))
				model.put("expire", "Account has been log in at other place!, please login again!");
			else
				model.put("expire", "Session expired, please login again!");
		}
		
		return "login";
	}
	
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else if (exception instanceof AccountExpiredException) {
			error = exception.getMessage();
		} else if (exception instanceof DisabledException) {
			error = exception.getMessage();
		} else {
			error = "Invalid username and password!!";
		}

		return error;
	}
	
//	@RequestMapping(value = "/logout", method = RequestMethod.GET)
//	public String toLogout(@RequestParam(value = "error", required = false) String error
//			, @RequestParam(value = "logout", required = false) String logout
//			, @RequestParam(value = "expired", required = false) String expired
//			, HttpServletRequest request, ModelMap model, @Valid User user, BindingResult result){
//		model.put("user", new User());
//		model.put("pageTitle", "Login");
//		
//		return "redirect:/login";
//	}
	
	@ExceptionHandler(HttpSessionRequiredException.class)
	public String handleSessionExpired() {
		return "redirect:/login";
	}
}
