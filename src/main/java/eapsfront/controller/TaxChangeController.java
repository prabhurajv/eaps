package eapsfront.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eapsfront.dao.ILangDao;
import eapsfront.dao.IPremiumTaxChangePolicyDao;
import eapsfront.dao.ITaxChangeDao;
import eapsfront.model.CustomerFormData;
import eapsfront.model.Lang;
import eapsfront.model.PremiumTaxChangeCustomerDecisionFormData;
import eapsfront.model.PremiumTaxChangePolicy;
import eapsfront.model.TaxChange;
import eapsfront.model.TaxChangeLog;
import eapsfront.model.UpsellInterestedFormData;
import eapsfront.model.ViewLang;
import eapsfront.property.IEapsProperties;
import eapsfront.util.AnonymousHelper;
import eapsfront.util.EAPSSecurityHelper;

@Controller
@SessionAttributes("log")
public class TaxChangeController {
	private static final String linkExpiredMsg = "Your link has already expired!<br/>តំណភ្ជាប់របស់អ្នកបានហួសពេលកំនត់ហើយ។";
	private static final String accessNotPermit = "Something is wrong with this policy!<br/>មានបញ្ហាកើតឡើង។";

	// @Autowired
	// private ITaxChangeDao taxChangeDao;

	@Autowired
	private IPremiumTaxChangePolicyDao premiumTaxChangePolicyDao;

	@Autowired
	private IEapsProperties properties;

	@Autowired
	private ILangDao langDao;

	// private TaxChangeLog taxChangeLog = null;

	// @RequestMapping(value = "/taxchange/{arg1}", method = RequestMethod.GET)
	// public String ShowPasscodePage(@PathVariable String arg1, ModelMap model,
	// HttpServletRequest request) {
	// //model.addAttribute("pageTitle", "Prudential Cambodia Life Assurance");
	// model.addAttribute("pageTitle", "Tax Change");
	// model.addAttribute("data", new CustomerFormData());
	// model.addAttribute("arg1", arg1);
	// model.addAttribute("log", new TaxChangeLog());

	// String browser = request.getHeader("User-Agent");
	// String ip = AnonymousHelper.getClientIP(request);
	// String link = request.getRequestURL().toString();

	// int affected = taxChangeDao.addVisitLog(browser, link, ip, new Date());

	// return "taxchange";
	// }

	// TAXCHANGE V.2 2019-03-09 By Malen SOK
	// customer link for making decision
	@RequestMapping(value = "/taxchange/{arg1}", method = RequestMethod.GET)
	public String ShowPasscodePage(@PathVariable String arg1, ModelMap model, HttpServletRequest request) {
		// model.addAttribute("pageTitle", "Prudential Cambodia Life Assurance");
		model.addAttribute("pageTitle", "Tax Change");
		model.addAttribute("data", new CustomerFormData());
		model.addAttribute("arg1", arg1);
		model.addAttribute("log", new TaxChangeLog());

		String browser = request.getHeader("User-Agent");
		String ip = AnonymousHelper.getClientIP(request);
		String link = request.getRequestURL().toString();

		int affected = premiumTaxChangePolicyDao.addVisitLog(browser, link, ip, new Date());

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().removeAttribute("premiumTaxChangePolicy"); // remove session

		return "customerPremiumTaxChange";
	}

	// @RequestMapping(value = "/taxchange/{arg1}", method = RequestMethod.POST)
	// public String PostPasscode(@PathVariable String arg1, ModelMap model,
	// HttpServletRequest request,
	// HttpServletResponse response, @Valid CustomerFormData data, BindingResult
	// result) {
	// if(result.hasErrors()){
	// model.addAttribute("data", new CustomerFormData());
	// model.addAttribute("pageTitle", "Tax Change");

	// return "taxchange";
	// }

	// String chdrNum = arg1;
	// try {
	// chdrNum = new
	// EAPSSecurityHelper().decrypt(chdrNum.replace(AnonymousHelper.getReverseString("SlaSh"),
	// "/"), data.getDob());
	// } catch (KeyException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (InvalidAlgorithmParameterException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IllegalBlockSizeException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (BadPaddingException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (GeneralSecurityException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }

	// String link = request.getRequestURL().toString();
	// TaxChangeLog log = taxChangeDao.getTaxChangeLog(chdrNum, link);

	// if(log == null) {
	// model.addAttribute("data", new CustomerFormData());
	// model.addAttribute("pageTitle", "Tax Change");
	// model.addAttribute("error", "មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​
	// សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");

	// return "taxchange";
	// }

	// log.setLifePFs(taxChangeDao.getLifePFs(chdrNum));
	// log.setTaxChange(taxChangeDao.getTaxChange(log));
	// log.setEffectiveDate(properties.getEffectiveDate());
	// log.setAnonymous(true);

	// // Calculate lapse days
	// // int lapseDays = getLapseDays(log.getSendDate().getTime());

	// taxChangeLog = log;

	// // Link is not expired when smaller than or equal to 60 days
	// // if(lapseDays <= Integer.parseInt(properties.getTaxChangeExpiredDay())){
	// model.addAttribute("log", log);

	// return "redirect:/taxchange/" + arg1 + "/" + log.isApproved();
	// // }else{
	// // model.addAttribute("error", linkExpiredMsg);
	// // model.addAttribute("data", new CustomerFormData());
	// // model.addAttribute("pageTitle", "Tax Change");
	// //
	// // return "taxchange";
	// // }
	// }
	// customer link for making decision
	@RequestMapping(value = "/taxchange/{arg1}", method = RequestMethod.POST)
	public String PostPasscode(@PathVariable String arg1, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, @Valid CustomerFormData data, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Tax Change");

			return "customerPremiumTaxChange";
		}

		String chdrNum = arg1;
		try {
			chdrNum = new EAPSSecurityHelper().decrypt(chdrNum.replace(AnonymousHelper.getReverseString("SlaSh"), "/"),
					data.getDob());
		} catch (KeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PremiumTaxChangePolicy premiumTaxChangePolicy = premiumTaxChangePolicyDao.GetPremiumTaxChange(chdrNum);

		if (premiumTaxChangePolicy == null) {
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Premium Tax Change");
			model.addAttribute("error",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");

			return "customerPremiumTaxChange";
		}

		premiumTaxChangePolicy.setAnonymous(true);

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("premiumTaxChangePolicy", premiumTaxChangePolicy);

		return "redirect:/showPremiumTaxChange/" + arg1;
	}

	// TAXCHANGE V.2 2019-03-09 By Malen SOK
	@RequestMapping(value = "/showPremiumTaxChange/{arg1}", method = RequestMethod.GET)
	public String showPremiumTaxChangePolicy(@PathVariable String arg1, ModelMap model, HttpServletRequest request,
			SessionStatus status) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		PremiumTaxChangePolicy premiumTaxChangePolicy = (PremiumTaxChangePolicy) attr.getRequest().getSession()
				.getAttribute("premiumTaxChangePolicy");

		String view = showPremiumTaxChangePolicyHelper(model, request);
		status.setComplete();

		return view;
	}

	public String showPremiumTaxChangePolicyHelper(ModelMap model, HttpServletRequest request) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		PremiumTaxChangePolicy premiumTaxChangePolicy = (PremiumTaxChangePolicy) attr.getRequest().getSession()
				.getAttribute("premiumTaxChangePolicy");

		if (premiumTaxChangePolicy == null) {
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Premium Tax Change");
			model.addAttribute("error",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");

			return "customerPremiumTaxChange";
		}

		Map<String, Object> result = premiumTaxChangePolicyDao.GeneratePrintPremiumTaxChange(premiumTaxChangePolicy);

		if (result == null || result.size() == 0) {
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Premium Tax Change");
			model.addAttribute("error",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");

			return "customerPremiumTaxChange";
		}

		for (String key : result.keySet()) {
			model.addAttribute(key, result.get(key));
		}

		model.addAttribute("IsAnonymous", premiumTaxChangePolicy.isAnonymous());
		model.addAttribute("effectiveDate", properties.getEffectiveDate());
		model.addAttribute("pageTitle", "Premium Tax Change Agreement");

		String messages = langDao.GetLangs();
		model.addAttribute("Messages", messages);

		return "printPremiumTaxChangePolicy";
	}

	// @RequestMapping(value = "/taxchange/{arg1}/{optional}", method =
	// RequestMethod.GET)
	// public String ShowAgreement(@PathVariable String arg1, @PathVariable String
	// optional, ModelMap model,
	// @ModelAttribute("log") TaxChangeLog log, HttpServletRequest request,
	// SessionStatus status){
	//
	// // Calculate lapse days
	//// int lapseDays = getLapseDays(log.getSendDate().getTime());
	//
	// // Link is expired when bigger than 60 days
	//// if(lapseDays > Integer.parseInt(properties.getTaxChangeExpiredDay())){
	//// model.addAttribute("data", new CustomerFormData());
	//// model.addAttribute("error", linkExpiredMsg);
	//// model.addAttribute("pageTitle", "Tax Change");
	////
	//// return "taxchange";
	//// }
	//
	// if(log != null){
	// if(log.isApproved()){
	// log.setIsSubmitted("submitted");
	// }
	//// else if(log.isRejected()){
	//// model.addAttribute("choice", false);
	//// }
	// }
	//
	// if(taxChangeLog == null || taxChangeLog.getChdrNum() != log.getChdrNum()){
	// taxChangeLog = log;
	// }
	//
	// model.addAttribute("log", log);
	// model.addAttribute("pageTitle", "Tax Change Agreement");
	// model.addAttribute("successEn", getSuccessMsg("en", "successMsg"));
	// model.addAttribute("successKh", getSuccessMsg("kh", "successMsg"));
	//
	//// if(log.getIsSubmitted() != null && log.getIsSubmitted() != ""){
	//// model.addAttribute("isSubmitted", "submitted");
	//// }
	//// taxChangeLog.setIsSubmitted(null);
	//
	// TaxChange taxChange = log.getTaxChange().get(0);
	//
	// status.setComplete();
	//
	// if(taxChange.getOption().trim().toLowerCase().equals("option 1")){
	// if(taxChange.getCaseType().trim().toLowerCase().equals("pe")){
	// return "taxchangeagreement-pender";
	// }else if(taxChange.getCaseType().trim().toLowerCase().equals("jfm")){
	// return "taxchangeagreement-jfm";
	// }else if(taxChange.getCaseType().trim().toLowerCase().equals("la")){
	// return "taxchangeagreement-la";
	// }
	// else{
	// return "taxchangeagreement";
	// }
	// }else{
	// if(taxChange.getCaseType().trim().toLowerCase().equals("pe")){
	// return "taxchangefixed-pender";
	// }else if(taxChange.getCaseType().trim().toLowerCase().equals("jfm")){
	// return "taxchangefixed";
	// }else if(taxChange.getCaseType().trim().toLowerCase().equals("la")){
	// return "taxchangefixed-la";
	// }
	// else{
	// return "taxchangefixed";
	// }
	// }
	// }

	@RequestMapping(value = "/upsellInterested", method = RequestMethod.POST)
	public String SetUpsellInterested(ModelMap model, HttpServletRequest request,
			@Valid UpsellInterestedFormData data) {

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		PremiumTaxChangePolicy premiumTaxChangePolicy = (PremiumTaxChangePolicy) attr.getRequest().getSession()
				.getAttribute("premiumTaxChangePolicy");

		if (premiumTaxChangePolicy == null) {
			model.addAttribute("pageTitle", "Premium Tax Change Agreement");
			model.addAttribute("submitError",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
			model.addAttribute("submit", true);
			return showPremiumTaxChangePolicyHelper(model, request);
		}

		boolean isAnonymous = SecurityContextHolder.getContext()
				.getAuthentication() instanceof AnonymousAuthenticationToken;

		PremiumTaxChangePolicy entity = premiumTaxChangePolicyDao
				.GetPremiumTaxChange(premiumTaxChangePolicy.getPolicy_num());

		if (data == null || data.getIs_upsell_interested() == null || !isAnonymous
				|| !premiumTaxChangePolicy.isAnonymous() || // customer login and browse from protal or sms bitlink
				entity.getCustomer_decision() == null ||
				entity.getCustomer_decision() == "N")// not upgrade protection will return
		{

			model.addAttribute("pageTitle", "Premium Tax Change Agreement");
			model.addAttribute("submitError",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
			model.addAttribute("submit", true);
			return showPremiumTaxChangePolicyHelper(model, request);
		}

		int affected = premiumTaxChangePolicyDao.setUpsellInterested(data.getIs_upsell_interested(),
				premiumTaxChangePolicy.getPolicy_num());

		return "redirect:/showPremiumTaxChange/true";
	}

	@RequestMapping(value = "/showPremiumTaxChange/{arg1}", method = RequestMethod.POST)
	public String SetCustomerDecision(@PathVariable String arg1, ModelMap model, HttpServletRequest request) {

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		PremiumTaxChangePolicy premiumTaxChangePolicy = (PremiumTaxChangePolicy) attr.getRequest().getSession()
				.getAttribute("premiumTaxChangePolicy");

		if (premiumTaxChangePolicy == null) {
			model.addAttribute("pageTitle", "Premium Tax Change Agreement");
			model.addAttribute("submitError",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
			model.addAttribute("submit", true);
			return showPremiumTaxChangePolicyHelper(model, request);
		}

		boolean isAnonymous = SecurityContextHolder.getContext()
				.getAuthentication() instanceof AnonymousAuthenticationToken;

		PremiumTaxChangePolicy entity = premiumTaxChangePolicyDao
				.GetPremiumTaxChange(premiumTaxChangePolicy.getPolicy_num());

		if (!isAnonymous || !premiumTaxChangePolicy.isAnonymous()  // customer login and browse from protal or sms bitlink
				|| entity.getProtection_upgr_eligible() == null
				|| entity.getProtection_upgr_eligible().equals("N"))//  not eligible
		{

			model.addAttribute("pageTitle", "Premium Tax Change Agreement");
			model.addAttribute("submitError",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");
			model.addAttribute("submit", true);
			return showPremiumTaxChangePolicyHelper(model, request);
		}

		int affected = premiumTaxChangePolicyDao.setCustomerDecision("Y", premiumTaxChangePolicy.getPolicy_num());

		model.addAttribute("firstLoadAfterSubmit", true); // firstLoadAfterSubmit
		return showPremiumTaxChangePolicyHelper(model, request);
	}

	// @RequestMapping(value = "/taxchange/lang/{lang}/{page}", method =
	// RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	// @ResponseBody
	// public String getLang(@PathVariable String lang, @PathVariable String page){
	// String result = "";

	// ArrayList<ViewLang> langs = taxChangeDao.getLangs(lang, page);

	// if(langs != null && langs.size() > 0){
	// Gson gson = new GsonBuilder().create();

	// result = gson.toJson(langs);
	// }

	// return result;
	// }

	// @RequestMapping(value = "/taxchange/lang/{lang}/{page}/{tagId}", method =
	// RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	// @ResponseBody
	// public String getLangList(@PathVariable String lang, @PathVariable String
	// page, @PathVariable String tagId){
	// String result = "";

	// ArrayList<Lang> langs = langDao.getLangList(lang, tagId);

	// if(langs != null && langs.size() > 0){
	// Gson gson = new GsonBuilder().create();

	// result = gson.toJson(langs);
	// }

	// return result;
	// }

	// // View Taxchange Portal for allowing customer or agent to input their policy
	// number and DOB
	// @RequestMapping(value = "/taxchangepolicy", method = RequestMethod.GET)
	// public String ShowRegularPage(ModelMap model, HttpServletRequest request) {
	// model.addAttribute("pageTitle", "Tax Change");
	// model.addAttribute("data", new CustomerFormData());
	// model.addAttribute("log", new TaxChangeLog());

	// String browser = request.getHeader("User-Agent");
	// String ip = AnonymousHelper.getClientIP(request);
	// String link = request.getRequestURL().toString();

	// int affected = taxChangeDao.addVisitLog(browser, link, ip, new Date());

	// return "taxchange2";
	// }

	// View Taxchange Portal for allowing customer or agent to input their policy
	// number and DOB
	@RequestMapping(value = "/premiumtaxchangeportal", method = RequestMethod.GET)
	public String PremiumTaxChangePortal(ModelMap model, HttpServletRequest request) {
		model.addAttribute("pageTitle", "Tax Change");
		model.addAttribute("data", new CustomerFormData());
		model.addAttribute("log", new TaxChangeLog());

		String browser = request.getHeader("User-Agent");
		String ip = AnonymousHelper.getClientIP(request);
		String link = request.getRequestURL().toString();

		int affected = premiumTaxChangePolicyDao.addVisitLog(browser, link, ip, new Date());

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().removeAttribute("premiumTaxChangePolicy"); // remove session

		return "premiumTaxchangePortal";
	}

	// Action Search of Taxchange Portal for allowing customer or agent to input
	// their policy number and DOB
	@RequestMapping(value = "/premiumtaxchangeportal", method = RequestMethod.POST)
	public String PostRegularPage(ModelMap model, HttpServletRequest request, HttpServletResponse response,
			@Valid CustomerFormData data, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Premium Tax Change");

			return "premiumTaxchangePortal";
		}

		String chdrNum = data.getPolicyNum();
		PremiumTaxChangePolicy premiumTaxChangePolicy = premiumTaxChangePolicyDao.GetPremiumTaxChange(chdrNum);

		if (premiumTaxChangePolicy == null) {
			model.addAttribute("data", new CustomerFormData());
			model.addAttribute("pageTitle", "Premium Tax Change");
			model.addAttribute("error",
					"មានបញ្ហាមិនប្រក្រតីបានកើតឡើង។​ សូមព្យាយាមម្តងទៀត។ <br/> Something went wrong. Please try again.");

			return "premiumTaxchangePortal";
		}

		// log.setTaxChange(taxChangeDao.getTaxChange(log));

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		// String poDob = df.format(log.getTaxChange().get(0).getPoDob());
		String poDob = df.format(premiumTaxChangePolicy.getPo_dob());

		String inputPoDob = formatDateString(data.getDob());

		if (poDob.compareTo(inputPoDob) == 0) {

			premiumTaxChangePolicy.setAnonymous(true);

			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			attr.getRequest().getSession().setAttribute("premiumTaxChangePolicy", premiumTaxChangePolicy);

			chdrNum = chdrNum.substring(0, 1) + "XXXXXX" + chdrNum.substring(chdrNum.length() - 1, chdrNum.length());
			return "redirect:/showPremiumTaxChange/" + chdrNum;

		}

		model.addAttribute("data", new CustomerFormData());
		model.addAttribute("pageTitle", "Premuim Tax Change");

		return "premiumTaxchangePortal";
	}

	// private int getLapseDays(long sendDateTime){
	// int days = 0;
	//
	// long current = new Date().getTime();
	//
	// days = (int)((((current - sendDateTime) / 1000)/3600)/24);
	//
	// return days;
	// }

	private String formatDateString(String unformatedDate) {
		String date = "";

		if (unformatedDate.length() == 8) {
			date = unformatedDate.substring(4) + "-" + unformatedDate.substring(2, 4) + "-"
					+ unformatedDate.substring(0, 2);
		}

		return date;
	}

	@RequestMapping(value = "/taxchange/getsuccessmsg/{lang}/{tagId}", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String getSuccessMsg(@PathVariable("lang") String lang, @PathVariable("tagId") String tagId) {
		String successMsg = "";

		successMsg = langDao.getSuccessMsg(lang, tagId);

		return successMsg;
	}
}
