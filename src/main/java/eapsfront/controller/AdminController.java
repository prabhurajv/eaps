package eapsfront.controller;

import java.io.IOException;
import java.lang.RuntimeException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import eapsfront.model.AdminFormData;

@Controller
public class AdminController {
	public static final String ROOT = "/WEB-INF/resources/images";
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String getAdminPage(ModelMap model) {
		model.put("pageTitle", "Admin Console");
		model.put("data", new AdminFormData());
		
		return "admin";
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	public String uploadImage(ModelMap model, HttpServletRequest request, @Valid AdminFormData formData, BindingResult result) {
		String path = request.getSession().getServletContext().getRealPath(ROOT);
		
		if(!formData.getImage().isEmpty()) {
			try{
				if(Files.exists(Paths.get(path, "ads.jpg"))) {
					Files.move(Paths.get(path, "ads.jpg"), Paths.get(path, "ads-" + new Date().toString().replace(":", "-") + ".jpg"));
				}
				
				Files.copy(formData.getImage().getInputStream(), Paths.get(path, "ads.jpg"));
				model.put("msg", "Image is uploaded successfully");
			}catch(IOException|RuntimeException e){
				e.printStackTrace();
				model.put("error", "No file has been uploaded");
			}
		}else {
			model.put("error", "No file has been uploaded");
		}
		
		model.put("pageTitle", "Admin Console");
		model.put("data", new AdminFormData());
		
		return "admin";
	}
}
